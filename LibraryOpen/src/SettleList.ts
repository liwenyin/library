/**
 * 结算页
 */
class SettleList extends MsgDisplay {

	/**
	 * 重写
	 */
	public async setData(data: ITFView): Promise<void> {
		var self = this;
		var utils = WXUtils;
		// 获取列表
		var infos = await utils.getFriendInfos();
		// 获取个人数据
		var myInfo = utils.getUserGameInfo(infos);
		// 按分数从大到小排序
		utils.sort(infos, 'score');
		// 开始显示
		var shows = utils.getNearInfos(infos, myInfo);
		var length = shows.length - 1;
		var startX = [313, 212, 111][length];
		var mission = data.mission;
		for (let i in shows) {
			let item = new SettleItem(shows[i], mission);
			item.x = startX + Number(i) * 202;
			self.addChild(item);
		}
	}
}