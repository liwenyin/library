/**
 * 入口类，提供
 */
class Main extends egret.DisplayObjectContainer {

    public constructor() {
        super();
        // 初始化代码如下
        var stage = this; // egret.sys.$TempStage;
        var sHeight = egret.sys.$TempStage.stageHeight;
        // 舞台是否可使用
        var stageEnable = function (bool: boolean) {
            egret.ticker[bool ? 'resume' : 'pause']();
        };

        // 消息接受
        var onMessage = function (msg: ITFWXMessage) {
            var action = msg.action;
            var data = msg.data;
            if (action) {
                console.log('onMessage', msg);
                switch (action) {
                    // 更新用户数据，需手动调用
                    case 'update':
                        updateUser(data);
                        break;
                    // 进入页面，主域WXBitmap触发
                    case 'enter':
                        initView(data);
                        break;
                    // 退出页面，主域WXBitmap触发
                    case 'exit':
                        removeView(data);
                        break;
                    // 停止渲染
                    case 'pause':
                        stageEnable(false);
                        break;
                    // 恢复渲染
                    case 'resume':
                        stageEnable(true);
                        break;
                }
            }
        };

        /**
         * 初始化界面
         * @param data 界面数据，继承ITFView
         */
        var initView = function (data: ITFView): void {
            var target = getView(getClass(data.view));
            if (target) {
                stage.scaleX = 750 / data.width;
                stage.scaleY = sHeight / data.height;
                target.setData(data);
                stageEnable(true);
            }
        };

        /**
         * 获取类控件，不存在则创建
         * @param clzz 类
         */
        var getView = function <T extends egret.DisplayObject>(clzz: { new (): T }): T {
            var view: T;
            if (clzz) {
                for (let i = 0, num = stage.numChildren; i < num; i++) {
                    let child = stage.getChildAt(i);
                    if (child instanceof clzz) {
                        view = <any>child;
                        break;
                    }
                }
            }
            if (!view) {
                view = new clzz;
                stage.addChild(view);
                // 下一层刷新
                let last = stage.$children[stage.numChildren - 2];
                last && (last.visible = false);
            }
            return view
        };

        /**
         * 移除界面
         * @param data 
         */
        var removeView = function (data: { view: string }): void {
            var clzz = getClass(data.view);
            if (clzz) {
                for (let i = stage.numChildren - 1; i >= 0; i--) {
                    let child = stage.getChildAt(i);
                    if (child instanceof clzz) {
                        stage.removeChildAt(i);
                        // 顶层刷新
                        let top = stage.$children[stage.numChildren - 1];
                        top && (top.visible = true);
                        break;
                    }
                }
            }
        };

        /**
         * 获取类名，需要自己重现
         * @param view 
         */
        var getClass = function (view: string): { new (): MsgDisplay } {
            var clzz;
            switch (view) {
                case 'settle':
                    clzz = SettleList;
                    break;
                case 'rank':
                    clzz = RankList;
                    break;
            }
            return clzz;
        };

        /**
         * 更新用户信息
         */
        var updateUser = function (newInfo?: ISend): void {
            var clzz = WXUtils;
            clzz.clearData();
            clzz.updateUserKVData(newInfo);
        };

        // 启动接收
        wx.onMessage(onMessage);                // 注册
        updateUser();
        WXUtils.getUserInfo();                  // 刷新，减少延迟
    }
}