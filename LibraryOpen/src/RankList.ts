/**
 * 排行榜
 */
class RankList extends MsgDisplay {

    private list: eui.List;
    private myItem: RankItem;

    public constructor() {
        super();
        var scroller = new eui.Scroller;
        var list = scroller.viewport = this.list = new eui.List;
        var myItem = this.myItem = new RankItem(true);
        list.itemRenderer = RankItem;
        (list.layout = new eui.VerticalLayout).gap = 28;
        myItem.y = 710;
        scroller.width = 588;
        scroller.height = 648;
        // 添加
        this.addChild(scroller);
        this.addChild(myItem);
        this.initUI();
    }

    public $onRemoveFromStage(): void {
        super.$onRemoveFromStage();
        var self = this;
        self.list.dataProvider = null;
        self.list = self.myItem = null;
    }

    /**
     * 初始化UI界面
     */
    private async initUI(): Promise<void> {
        var self = this;
        var utils = WXUtils;
        // 获取列表
        var infos = await utils.getFriendInfos();
        // 获取个人数据
        var myInfo = utils.getUserGameInfo(infos);
        // 按分数从大到小排序
        utils.sort(infos, 'score');
        // 显示
        self.list.dataProvider = new eui.ArrayCollection(infos);
        self.myItem.data = myInfo;
    }
}