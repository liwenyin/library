module lie {
	/**
	 * 事件分发工具类，异步操作用
	 */
	export class Dispatch {

		private static $targets: any = {};	// 存放所有回调函数

		/**
		 * 注册对象
		 * @param key 事件的唯一标志
		 * @param obj 注册事件
		 * @param replace 是否替换掉之前的回调
		 */
		private static $register(key: string, obj: [Function, any], replace?: boolean): boolean {
			var self = Dispatch;
			var target = self.$targets[key];
			var isReg = !target || replace;
			isReg && (self.$targets[key] = obj);
			return isReg;
		}

		/**
		 * 注册事件
		 * @param key 事件的唯一标志
		 * @param call 回调函数
		 * @param thisObj 回调所属对象
		 * @param replace 是否替换掉之前的回调
		 */
		public static register(key: string, call: Function, thisObj?: any, replace?: boolean): boolean {
			return Dispatch.$register(key, [call, thisObj], replace);
		}

		/**
		 * 通知，触发回调
		 * @param key 事件标志
		 * @param param 回调参数
		 */
		public static notice(key: string, ...params: any[]): void {
			var target = Dispatch.$targets[key];
			target && target[0].apply(target[1], params);
		}

		/**
		 * 移除事件
		 */
		public static remove(key: string): void {
			delete Dispatch.$targets[key];
		}

		/**
		 * 是否注册了某个事件
		 */
		public static hasRegister(key: string): boolean {
			return !!Dispatch.$targets[key];
		}

		/**
		 * 注册多个事件，统一回调，参数功能看register
		 */
		public static registers(keys: string[], call: Function, thisObj?: any, replace?: boolean): void {
			var obj = <any>[call, thisObj];
			for (let i in keys) {
				let key = keys[i];
				Dispatch.$register(key, obj, replace);
			}
		}

		/**
		 * 清除所有事件
		 */
		public static clear(): void {
			Dispatch.$targets = {};
		}
	}
}