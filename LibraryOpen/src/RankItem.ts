/**
 * 排行榜子项
 */
class RankItem extends eui.ItemRenderer {

	private rankImg: egret.Bitmap;
	private rankLbl: egret.TextField;
	private headImg: egret.Bitmap;
	private nameLbl: egret.TextField;
	private scoreLbl: egret.TextField;

	public constructor(special?: boolean) {
		super();
		var width = this.width = 588;
		var height = this.height = 84;
		// 排名
		var rankImg = this.rankImg = new egret.Bitmap;
		var rankLbl = this.rankLbl = new egret.TextField;
		rankImg.x = 20;
		rankImg.y = 14;
		rankLbl.y = 28;
		rankLbl.size = 28;
		rankLbl.textColor = 0xff8900;
		rankLbl.width = 90;
		rankLbl.textAlign = egret.HorizontalAlign.CENTER;
		// 头像+头像遮罩
		var head = this.headImg = new egret.Bitmap;
		// var mask = new egret.Bitmap;
		var mask = ResUtils.getCircleShape(84);
		head.width = head.height = 84;
		mask.x = head.x = 90;
		mask.y = head.y = 0;
		head.mask = mask;
		// ResUtils.setTexture(mask, 'resource/assets/rank/head_mask.png');
		// 名称
		var name = this.nameLbl = new egret.TextField;
		name.x = 204;
		name.y = 28;
		name.size = 28;
		name.textColor = special ? 0xffffff : 0x20323a;
		// 关卡
		var score = this.scoreLbl = new egret.TextField;
		score.x = 434;
		score.y = 28;
		score.size = 28;
		score.textColor = special ? 0xf1dacb : 0xdf4800;
		// 添加
		this.addChild(rankImg);
		this.addChild(rankLbl);
		this.addChild(head);
		this.addChild(mask);
		this.addChild(name);
		this.addChild(score);
	}

	public $onRemoveFromStage(): void {
		super.$onRemoveFromStage();
		var self = this;
		self.rankImg = self.rankLbl = self.headImg =
			self.nameLbl = self.scoreLbl = null;
	}

	protected dataChanged(): void {
		var self = this;
		var setTexture = ResUtils.setTexture;
		var data = <IUserGameData>self.data;
		// 排名
		var rank = data.rank, rankImg = self.rankImg, rankLbl = self.rankLbl;;
		var showImg = rank < 4;
		rankLbl.visible = !(rankImg.visible = showImg);
		if (showImg)
			setTexture(rankImg, 'resource/assets/rank/icon_rank' + rank + '_ranking.png');
		else
			rankLbl.text = rank + '';
		// 其他
		setTexture(self.headImg, data.avatarUrl);
		self.nameLbl.text = ResUtils.getLimitStr(data.nickname, 14);
		self.scoreLbl.text = '第 ' + data.score + ' 关';
	}
}