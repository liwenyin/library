/**
 * 资源工具类
 */
class ResUtils {

    private static m_pCache: { [key: string]: egret.Texture } = {};

    /**
     * 加载资源
     * @param url 资源地址
     */
    public static async loadUrl(url: string): Promise<any> {
        return new Promise<egret.Texture>(function (resolve) {
            let cache = ResUtils.m_pCache;
            let texture = cache[url];
            let endCall = function () {
                resolve(texture);
            };
            if (texture)
                endCall();
            else {
                let loader = new egret.ImageLoader();
                loader.addEventListener(egret.Event.COMPLETE, function (event: egret.Event) {
                    let imageLoader = <egret.ImageLoader>event.currentTarget;
                    texture = new egret.Texture();
                    texture._setBitmapData(imageLoader.data);
                    cache[url] = texture;
                    endCall();
                }, null);
                loader.load(url);
            }
        });
    }

    /**
     * 设置纹理
     */
    public static async setTexture(bitmap: egret.Bitmap, url: string): Promise<void> {
        var t = await ResUtils.loadUrl(url);
        bitmap.texture = t;
    }

    // /**
    //  * 添加圆形遮罩
    //  */
    // public static getCircleRect(size: number, color: number = 0): Laya.Sprite {
    //     var shape = new Laya.Sprite;
    //     var graphics = shape.graphics;
    //     graphics.drawCircle(size, size, size, ResUtils.num2StrColor(color));
    //     return shape;
    // }

    /**
     * 添加圆形遮罩
     */
    public static getCircleShape(size: number, color: number = 0): egret.Shape {
        var shape = new egret.Shape;
        var graphics = shape.graphics;
        var bsize = size / 2;
        graphics.beginFill(color);
        graphics.drawCircle(bsize, bsize, bsize);
        graphics.endFill();
        shape.width = shape.height = size;
        return shape;
    }

    /**
     * 获取编码所占字节数
     */
    public static getCodeLen(c: number): number {
        var len = 0;
        if (c < 0x007f) {
            len = 1;
        }/* else if ((0x0080 <= c) && (c <= 0x07ff)) {
            len = 2;
        } else if ((0x0800 <= c) && (c <= 0xffff)) {
            len = 3;
        }*/
        else
            len = 2;
        return len;
    }

    /**
     * 字符串接省略号
     * @param name 字符串
     * @param max 限制字节数
     */
    public static getLimitStr(name: string, max: number): string {
        var str = '';
        var total = 0;
        for (let i = 0, len = name.length; i < len; i++) {
            let c = name.charCodeAt(i);
            total += ResUtils.getCodeLen(c);
            if (total > max && i < len - 1) {
                str += '..';
                break;
            }
            str += name[i];
        }
        return str;
    }

    /**
     * 数值转字符串颜色
     * @param num 数字
     */
    public static num2StrColor(num: number): string {
        var str = num.toString(16), fix = '#';
        for (let i = str.length; i < 6; i++)
            fix += '0';
        return fix + str;
    }
}