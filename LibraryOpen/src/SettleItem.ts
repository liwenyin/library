/**
 * 结算列表子项
 */
class SettleItem extends egret.DisplayObjectContainer {

	public constructor(data: IUserGameData, mission: number) {
		super();
		var width = this.width = 124;
		var height = this.height = 272;
		var setTexture = ResUtils.setTexture;
		// 排名
		var rank = new egret.TextField();
		rank.text = data.rank + '';
		rank.textColor = 0;
		rank.size = 36;
		rank.alpha = .5;
		rank.x = (width - rank.width) / 2;
		// 头像
		var head = new egret.Bitmap;
		var mask = ResUtils.getCircleShape(width);
		mask.width = mask.height = head.width = head.height = width;
		mask.y = head.y = 48;
		head.mask = mask;
		setTexture(head, data.avatarUrl);
		// setTexture(mask, 'resource/assets/result/head_mask_rt' + mission + '.png');
		// 昵称
		var name = new egret.TextField;
		name.size = 26;
		name.text = data.nickname;
		name.textColor = 0;
		name.alpha = .5;
		name.x = (width - name.width) / 2;
		name.y = 180;
		// 关卡/分数
		var score = new egret.TextField;
		score.size = 42;
		score.text = data.score + '';
		score.textColor = 0;
		score.alpha = .9;
		score.x = (width - score.width) / 2;
		score.y = 236;
		// 添加
		this.addChild(rank);
		this.addChild(head);
		this.addChild(mask);
		this.addChild(name);
		this.addChild(score);
	}
}