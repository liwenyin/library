/**
 * 工具类
 */
class Utils {

    /**
     * 自定义格式化字符串
     * @param reg 正则
     * @param str 字符串
     * @param args 填充参数
     */
    public static formatStringReg(reg: RegExp, str: string, args: any[]): string {
        for (let i in args) {
            let arg = args[i];
            if (reg.test(str))
                str = str.replace(reg, args[i]);
            else
                break;
        }
        return str;
    }

    /**
     * 格式化字符串
     */
    public static formatString(str: string, ...args: any[]): string {
        str = str.replace(/%%/g, '%');  // 有些识别问题出现两个%号
        return Utils.formatStringReg(/%d|%s/i, str, args);  // 忽略大小写
    }
}