declare var wx: any;

/**
 * 推送数据
 */
interface ISend {
	score: number;
	time: number;
}

/**
 * 分数
 */
interface IScore {
	score: number;
	time: number;
}

/**
 * 存储数据类型
 */
interface IKVData {
	key: string;
	value: string;
}

/**
 * 玩家云数据
 */
interface IUserGameData extends IScore {
	avatarUrl: string;		// 微信头像
	nickname: string;   	// 昵称
	openid: string;     	// 玩家openid
	KVDataList: IKVData[];	// 玩家数据
	// 这个是自己加的字段
	rank: number;			// 排名
	isMe: boolean;			// 是否时自己的数据
}

/**
 * 界面需要带的参数
 */
interface ITFView {
	view: string;
	width: number;
	height: number;
	// 界面专属
	mission: number;
}

/**
 * 微信通讯格式
 */
interface ITFWXMessage {
	action: string;
	data: any;
	url?: string;   // 资源
}