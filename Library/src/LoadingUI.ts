/**
 * Test loading页
 */
class LoadingUI extends lie.LoadingView {

    private $message: string = 'onLoadeEnd';    // 注册事件

    public constructor(groupName: string, url?: string) {
        super(groupName, url);
        this.createView();
    }

    private textField: egret.TextField;

    private createView(): void {
        this.textField = new egret.TextField();
        this.addChild(this.textField);
        this.textField.y = 300;
        this.textField.width = 480;
        this.textField.height = 100;
        this.textField.textAlign = "center";
    }

    /**
     * 重写
     */
    protected onSuccess(): void {
        super.onSuccess();
        Dispatch.notice(this.$message);
    }

    /**
     * 重写
     */
    protected onProgress(current: number, total: number): void {
        super.onProgress(current, total);
        this.textField.text = `Loading...${current}/${total}`;
    }

    /**
     * 重写
     */
    protected onError(errorCode: number): void {
        super.onError(errorCode);
        this.textField.text = '资源加载失败';
    }

    /**
     * 显示开始加载的界面——外部调用方法
     */
    public showLoadView(): Promise<any> {
        var self = this;
        return new Promise<any>(function (resolve, reject) {
            let count = 0;  // 首页+资源
            // 开始加载资源
            self.onLoadStart();
            // 触发完成
            Dispatch.register(self.$message, function () {
                ++count >= 1 && resolve();
            });
        });
    }
}
