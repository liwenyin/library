// 工具类类对象
const AppViews = lie.AppViews;
const Dispatch = lie.Dispatch;
const LocalData = lie.LocalData;
const LockUtils = lie.LockUtils;
const Http = lie.Http;
const SortTools = lie.SortTools;
const Utils = lie.Utils;

// 微信工具类（变量名修改时请全局修改）
const pfUtils = lie.WXUtils.getInstance();

/**
 * 入口类，模板
 */
class Main extends egret.DisplayObjectContainer {

    public constructor() {
        super();
        this.addEventListener(egret.Event.ADDED_TO_STAGE,
            /*this.onLoading*/this.onTest, this);
    }

    /**
     * 加载示例
     */
    private async onLoading(): Promise<void> {
        var self = this;
        lie.LifeCycle.init();
        // 适配器
        var register = egret.registerImplementation;
        register("eui.IAssetAdapter", new AssetAdapter);
        register("eui.IThemeAdapter", new ThemeAdapter);
        // 预加载组名与网络资源配置
        var progress = new LoadingUI('preload'/*, Server.CDN + 'resource/default.res.json'*/);
        self.addChild(progress);
        // 资源
        var resDir = 'resource/';		// 资源根目录
        var resJson = 'default.res.json';	// 资源配置文件
        await progress.loadConfig(resJson, resDir); // 先获取url配置再加载本地配置
        await progress.showLoadView();              // 开始loading资源到缓存
        await self.loadTheme(resDir + 'default.thm.json');
        self.enterGame();
    }

    /**
     * 加载主题
     */
    private loadTheme(file: string): Promise<any> {
        return new Promise((resolve, reject) => {
            //加载皮肤主题配置文件,可以手动修改这个文件。替换默认皮肤。
            let theme = new eui.Theme("resource/default.thm.json", this.stage);
            theme.addEventListener(eui.UIEvent.COMPLETE, resolve, null);
        });
    }

    /**
     * 进入游戏
     */
    private enterGame(): void {
        // 进入首页
        AppViews.pushPanel(MenuPanel);
        // 执行初始化
        AppConfig.excuteCalls();
    }

    //// 工具类使用示例，正式时请删除 ////

    /**
     * 测试用例，想看哪个效果就打开哪个注释
     */
    private onTest(): void {
        var self = this;
        // self.testHttp();
        // self.testCache();
        // self.testDispatch();
        // self.testRepeat();
        // self.testSort();
        // self.testShade();
        // self.testFPromise();
        self.testViewTree();
    }

    /**
     * Http示例
     */
    private testHttp(): void {
        Http.get({
            // 端口号请修改
            url: 'http://127.0.0.1:5357/test/readme.txt',
            type: 1     // 显示文本
        }).then(function (resp) {
            console.log('resp:' + resp);
        });
    }

    /**
     * 缓存工具类测试
     */
    private testCache(): void {
        class AClass {
            public id: number;
        };
        // 初始化
        var cache = new lie.CacheUtils<AClass>('test');
        cache.initCreate(function () {
            var c = new AClass;
            c.id = 0;
            return c;
        });
        // 使用
        var aC = cache.getByCreate();
        console.log('id0:' + aC.id);
        egret.setTimeout(function () {
            aC.id = 1;
            cache.addCache(aC);
        }, null, 100);
        egret.setTimeout(function () {
            let aC = cache.getByCreate();
            console.log('id1:' + aC.id);
            cache.clear();  // 用完删除
        }, null, 200);
    }

    /**
     * 轻量级通讯测试
     */
    private testDispatch(): void {
        var message = 'updateView';
        // 1.一般，先注册才能接到触发
        Dispatch.register(message + 0, function () {
            console.log('message0', arguments);
            Dispatch.remove(message + 0); // 如果注册isOnce为true，则不需要手动移除
        });
        egret.setTimeout(function () {
            Dispatch.notice(message + 0, 1, 2, 3);
        }, null, 2000);

        // 2.延迟触发可晚注册
        Dispatch.noticeLater(message + 1, 4, 5, 6);
        egret.setTimeout(function () {
            Dispatch.register(message + 1, function () {
                console.log('message1', arguments);
                Dispatch.remove(message + 1);
            });
        }, null, 2000);

        // 3.正规用法，此处用闭包表示不同的类
        (function () {
            var label = new egret.TextField;
            label.textColor = 0xf4f4f4;
            label.size = 40;
            egret.sys.$TempStage.stage.addChild(label);
            Dispatch.register('update', function (text: string) {
                label.text = text;
            });
        })();
        (function () {
            Http.get({
                url: 'http://127.0.0.1:5357/test/readme.txt',
                type: 1
            }).then(function () {
                Dispatch.notice('update', 'success');
            }).catch(function () {
                Dispatch.notice('update', 'fail');
            });
        })();
    }

    /**
     * 测试锁
     */
    private testLock(): void {
        var key = LockUtils.create(1000, function () {
            console.log('执行：' + Date.now());
        });
        // 1.初始化时调用：成功
        console.log('调用1：' + Date.now());
        LockUtils.excute(key);
        LockUtils.lock(key);
        // 2.锁住状态在解锁时间未到时直接调用：失败
        egret.setTimeout(function () {
            console.log('调用2：' + Date.now());
            LockUtils.excute(key);
        }, null, 500);
        // 3.解锁后再调用：成功
        egret.setTimeout(function () {
            LockUtils.unlock(key);
            console.log('调用3：' + Date.now());
            LockUtils.excute(key);
            // 锁住为了测试4
            LockUtils.lock(key);

            // 4.在锁住时间到期后调用：成功
            egret.setTimeout(function () {
                console.log('调用4：' + Date.now());
                LockUtils.excute(key);
                // 用完删除
                LockUtils.clear(key);
            }, null, 1100); // 每次解锁会重置倒计时
        }, null, 600);
    }

    /**
     * 测试重复工具类
     */
    private testRepeat(): void {
        // 主要用于重复发起容易失败的请求
        var repeat = new lie.RepeatUtils(2000, 4);
        var getCall = function (index) {
            console.log('index', index);
            return new Promise<boolean>(function (resolve) {
                lie.Http.get({
                    url: 'http://127.0.0.1:5357/test/readme.txt',
                    type: 1,
                    param: { msg: '哈哈哈' }
                }).then(function (res) {
                    console.log('return:', Date.now());
                    resolve(index == 3);
                });
            });
        };
        repeat.setRepeatCall(getCall);
        repeat.setFinishCall(function (bool) {
            console.log('The Repeat Result is ' + (bool ? 'success' : 'fail'));
        });
    }

    /**
     * 测试排序工具
     */
    private testSort(): void {
        var array0 = [
            { x: 1, y: 6 },
            { x: 5, y: 3 },
            { x: 4, y: 1 },
            { x: 3, y: 2 }
        ];
        var array1 = [
            { x: 1, y: 6 },
            { x: 4, y: 3 },
            { x: 4, y: 1 },
            { x: 4, y: 2 },
            { x: 3, y: 2 }
        ];
        // 对数组的单个子属性进行排序
        SortTools.sortMap(array0, 'x', true);
        console.log(array0);
        // 对数组的多个子属性进行排序（子属性的顺序表示排序的优先级从高到低）
        SortTools.sortMaps(array1, ['x', 'y']);
        console.log(array1);
    }

    /**
     * 测试按钮点击颜色加深过度效果
     */
    private testShade(): void {
        Utils.loadResByUrl('test/btn_complete.png').then(function (t) {
            let bitmap = new egret.Bitmap(t);
            bitmap.touchEnabled = true;
            AppViews.stage.addChild(bitmap);
            // 监听
            lie.EventUtils.addShadeListener(bitmap);
        });
    }

    /**
     * 测试FPromise，更详细示例可以看WXUtils的showAds
     */
    private testFPromise(): void {
        var create = function () {
            return new lie.FPromise<void>(function (resolve) {
                egret.setTimeout(function () {
                    resolve();
                }, null, 1000);
            });
        };
        var count0 = 0, count1 = 0;

        // 示例2
        class Test {

            private single: lie.FPromise<void>; // 内部变量

            public constructor() {
                var self = this;
                Utils.loadResByUrl('test/btn_complete.png').then(function (t) {
                    let bitmap = new egret.Bitmap(t);
                    bitmap.touchEnabled = true;
                    AppViews.stage.addChild(bitmap);
                    // 内部变量
                    let single: lie.FPromise<any>;
                    // 监听
                    lie.EventUtils.addTouchTapListener(bitmap, self.onClick, self);
                });
            }

            public onClick(): void {
                var self = this;
                var single = self.single || (self.single = create())
                console.log(Utils.formatString('你点击了%d次 时间：%d',
                    ++count0, Date.now()));
                single.then(function () {
                    self.single = null;  // 用完删除
                    console.log(Utils.formatString('Promise触发了%d次 时间：%d',
                        ++count1, Date.now()));
                });
            }
        }
        new Test;
    }

    /**
     * 测试视图树
     */
    private testViewTree(): void {
        var tree = lie.ViewTree.instance;
        tree.width = 750;
        tree.height = 760;
        this.addChild(tree);

        var cont = new egret.DisplayObjectContainer;
        cont.name = '哈哈1';
        cont.addChild(new egret.Shape).name = '你1';
        cont.addChild(new egret.TextField).name = '好1';
        cont.addChild(new egret.Bitmap).name = '吗1';
        this.addChild(cont);

        var cont = new egret.DisplayObjectContainer;
        cont.name = '哈哈2';
        cont.addChild(new egret.Shape).name = '你2';
        cont.addChild(new egret.TextField).name = '好2';
        cont.addChild(new egret.Bitmap).name = '吗2';
        this.addChild(cont);

        tree.refresh();
    }
}