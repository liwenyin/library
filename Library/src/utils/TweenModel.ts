module lie {

	/**
	 * 缓间动画模板类
	 */
	export class TweenModel {

        /**
         * 上下移动动画，先向上移动再返回原处
		 * @param move 移动距离
		 * @param time 向上移动的时间
		 * @param wait 循环的间隔，默认0
         */
		public static quadUp(display: egret.DisplayObject, move: number, time: number, wait: number = 0): void {
			var clzz = egret.Ease;
			var oldX = display.x, oldY = display.y;
			egret.Tween.get(display, { loop: true }).
				to({ x: oldX - move, y: oldY - move }, time, clzz.quadIn).
				to({ x: oldX, y: oldY }, time, clzz.quadOut).
				wait(wait);
		}

        /**
         * 循环抖动动画（倍数变化）
		 * @param time 每一阶段的时间，默认200
         */
		public static shake(target: egret.DisplayObject, time: number = 200): void {
			var width = target.width, olds = target.scaleX, sub = olds - 1;
			var scale0 = (width + 40) / width + sub, scale1 = (width + 20) / width + sub;
			var olds = target.scaleX;
			egret.Tween.get(target, { loop: true }).to({
				scaleX: scale0,
				scaleY: scale0
			}, time).to({
				scaleX: scale1,
				scaleY: scale1
			}, time / 2).to({
				scaleX: scale0,
				scaleY: scale0
			}, time / 2).to({
				scaleX: olds,
				scaleY: olds
			}, time).wait(time * 4);
		}

        /**
         * 循环闪烁效果（透明度变化）
		 * @param time 每一阶段的时间，默认150
         */
		public static blink(target: egret.DisplayObject, time: number = 150): void {
			egret.Tween.get(target, { loop: true }).to({
				alpha: 0
			}, time).to({
				alpha: 1
			}, time);
		}

		/**
		 * 循环左右摇晃动画，注意控件必须锚点居中或者有水平垂直约束，否则会很怪异
		 * @param time 第一段动画时间
		 */
		public static swing(target: egret.DisplayObject, time: number = 100): void {
			egret.Tween.get(target, { loop: true }).to({
				rotation: 30
			}, time).to({
				rotation: -30
			}, time * 1.6).to({
				rotation: 15
			}, time * 1.2).to({
				rotation: -15
			}, time * .8).to({
				rotation: 0
			}, time * .6).wait(time * 8);
		}
	}
}