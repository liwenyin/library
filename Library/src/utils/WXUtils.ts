module lie {

	//// 注：以下带*的皆是需要重写 ////

	declare var wx, sharedCanvas: HTMLCanvasElement;

	var $canvas, ins;

	/**
	 * 分享配置，因游戏而定
	 */
	interface IShareConfig {
		title: string;	// 分享文案
		image: string;	// 分享图
	}

	/**
	 * 分享参数
	 */
	interface IShareInfo {
		title: string;
		imageUrl: string;
		query?: string;
		success?: (a) => void;
		fail?: (a) => void;
	}

	/**
	 * 子域参数
	 */
	interface KVData {
		key: string;
		value: string;
	}

	/**
	 * 系统信息
	 */
	interface ISystemInfo {
		SDKVersion: string;		// 微信SDK版本
		brand: string;			// 运行环境，品牌
		model: string;			// 系统型号
		platform: string;		// 客户端平台
		system: string;			// 操作系统版本
		version: string;		// 微信版本号
		windowWidth: number;    // 可使用窗口宽度
		windowHeight: number;	// 可使用窗口高度
		screenWidth: number;	// 屏幕宽度
		screenHeight: number;	// 屏幕高度
	}

	/**
	 * 音乐接口
	 */
	interface IMusic {
		play(): void;
		pause(): void;
		src: string;
		loop: boolean;
		volume: number;
		startTime: number;
		// 微信才有的
		onEnded?(call: Function): void;
		destroy?(): void;
		param?: any;
	}

	/**
	 * 微信通讯格式
	 */
	interface IWXMessage {
		action: string;
		data: any;
	}

	/**
	 * 客服配置
	 */
	interface ICustomer {
		content: string;// 弹窗内容
		card: boolean;  // 是否显示卡片
		title: string;  // 卡片标题
		path: string;   // 卡片路径参数
		imgUrl: string; // 卡片图
		imgUrl1: string;// 无法打开弹窗时弹出的图片路径
	}

	/**
	 * 游戏圈空对象
	 */
	class GameClub {
		show(): void { }
		hide(): void { }
	}

	/**
	 * 用户信息按钮
	 */
	export interface IUserInfoBtn {
		show: Function;		// 显示
		hide: Function;		// 隐藏
		destroy: Function;	// 销毁
		text: string;		// 按钮文字，类型为text修改才有效
		onTap: (call: (res) => void) => void;	// 点击事件绑定
	}

	/**
	 * 微信广告对象
	 */
	export interface IBanner {
		isDestroy: boolean;	// 标记属性
		style: any;
		show: () => void;
		hide: () => void;
		onLoad: (call: Function) => void;
		onError: (call: Function) => void;
		destroy: () => void;
		onResize: (e) => void;
	}

	/**
	 * 意见建议按钮，创建时会默认显示
	 */
	export interface IFeedbackButton {
		show(): void;					// 显示
		hide(): void;					// 隐藏
		destroy(): void;				// 销毁
		onTap(call: Function): void;	// 监听点击事件
		offTap(call: Function): void;	// 取消点击事件
	}

	/**
	 * 微信平台工具类——微信工具除登录和获取用户信息
	 * 注：一些函数是需要根据游戏而定，暴露在AppConfig，请自行完善这些方法
	 */
	export class WXUtils {

		// 模拟分享相关
		private $onImitate: string = '$onImitate';					// 模拟分享回调
		private $failCount: number = 0;								// 分享失败次数
		private $shareRatios: number[];								// 分享成功概率
		// 其他缓存
		protected shares: IShareConfig[];							// 分享配置
		protected shareGroup: string[];								// 分享标记
		protected bgMusic: IMusic;									// 背景音乐
		protected effects: { [key: string]: IMusic[] } = {};		// 存放音效
		protected bannerCache: IBanner[] = [];						// 广告缓存
		protected adsPromise: FPromise<number>;						// 视频对象
		protected isShowAds: boolean;								// 是否显示广告视频
		protected isShowLoading: boolean;							// 是否显示loading中
		protected isCheckLogin: boolean;							// 是否检测登录中
		protected isPreviewImg: boolean;							// 是否弹出图片中
		protected hasRechage: boolean;								// 是否加载过充值界面
		protected ipxY: number = 60;								// ipx的距离顶部的偏移值
		protected ipSwitch: number;									// ip开关

		public offsetY: number;										// Y值偏移，适配用

		protected constructor() {
		}

		/**
		 * 获取单例
		 */
		public static getInstance(): WXUtils {
			return ins || (ins = new WXUtils);
		}

		/**
		 * 游戏进入初始化——调用一次即可
		 */
		public init(): void {
			var self = this;
			// 分享携带ShareTicket
			wx.updateShareMenu({
				withShareTicket: true
			});
			wx.onShow(self.onShow.bind(self));
			wx.onHide(self.onHide.bind(self));
			wx.showShareMenu();	// 自动显示
			// 网络状态变化监听
			wx.onNetworkStatusChange(NetworkUtils.excuteStatusChange);
			// 屏幕适配
			self.offsetY = self.isIPhoneX() ? self.ipxY : 0;
			// xgSdk
			wx.xgSdkInit();
			// 转发分享
			wx.onShareAppMessage/*wx.xgOnShareAppMessage*/(function () {
				let obj = self.getGlobalShareInfo();
				// obj.success = function (t) {
				// 	// 用户点击了“转发”按钮
				// };
				return obj;
			});
		}

		/**
		 * 微信onShow
		 */
		protected onShow(res: any): void {
			console.log('onShow', res);
			var clzz = AppConfig;
			// 分享打开的链接
			var query = res.query;
			// 其他固定操作
			this.playBgMusic();
			clzz.setQuery(query, res.shareTicket);
			// 特殊渠道
			var scene = res.scene;
			if (scene == 1103 || scene == 1104) {
				clzz.startWithShortcut();
			}
			// 从小程序进入
			else if (scene == 1037) {
				// let mini = clzz.mini;
				// let appId = mini && mini.appId;
				// appId && this.toMiniProgram(appId, mini.path, mini.extraData);
			}
			// 模拟分享手机版分享结束，需要延迟，因为真的分享也是onShow之后再触发success的
			egret.callLater(Dispatch.notice, null, this.$onImitate);
		}

		/**
		 * 微信onHide
		 */
		protected onHide(res: any): void {
			this.pauseBgMusic();
		}

		/**
		 * 获取开放域
		 */
		public getShareCanvas(): egret.Texture {
			var texture = new egret.Texture();
			(texture.bitmapData = new egret.BitmapData(sharedCanvas)).
				$deleteSource = false;
			return texture;
		}

		/**
		 * 显示分享
		 * @param query 分享携带参数
		 * @param imgUrl 指定图片路径
		 */
		public showShare(query?: string, imgUrl?: string): Promise<string> {
			var self = this;
			return new Promise<string>(function (resolve, reject) {
				let obj = self.getGlobalShareInfo(imgUrl, query);
				// 现在都不给了
				obj.success = function (res) {
					let tickets = res && res.shareTickets;
					resolve(tickets ? tickets[0] : '1');
				};
				obj.fail = function (res) {
					resolve();
				};
				wx.shareAppMessage(obj);
				// 新分享，不得用 test
				// wx.xgShareAppMessage(obj);
			});
		}

		/**
		 * 需检测配置、分享回调的使用该方法
		 * 注：参数同showShare
		 * @param unRandom 为true时，不随机结果，则按默认的来
		 * @param auto 是否由调用者主动控制，否则默认打印
		 * @returns 2表示模拟分享的结果
		 */
		public showNewShare(unRandom?: boolean, query?: string, imgUrl?: string, auto?: boolean): Promise<string> {
			var self = this;
			return new Promise<string>(function (resolve) {
				let isExcute = false;	// 检测是否已执行
				// 执行
				let excute = function (ticket: string) {
					isExcute = true;
					// 非主动控制则失败下提示
					!ticket && !auto && pfUtils.showToast('别总骚扰这个群，换个群分享吧！');
					resolve(ticket);
				};
				// 兼容低版本
				self.showShare(query).then(function (ticket) {
					!isExcute && excute(ticket);
				});
				// 模拟分享，兼容高版本
				let info = self.getSystemInfo();
				if (Utils.compareVersion(info.SDKVersion, '2.3.0') >= 0) {
					let time = 3000;		// 延迟为多久时标识成功
					// 兼容开发者工具（分享onShow没回调），发布时可删
					if (info.platform == 'devtools')
						egret.setTimeout(function () {
							!isExcute && excute('2');
						}, null, time);
					else {
						let start = egret.getTimer();
						// 可查看该事件的使用地方
						Dispatch.register(self.$onImitate, function () {
							// 兼容低版本判断
							if (!isExcute) {
								let result = egret.getTimer() - start >= time &&
									(unRandom || self.getShareResult());
								let ticket = result ? '2' : '';
								excute(ticket);
							}
						}, null, true);
					}
				}
			});
		}

		/**
		 * 获取模拟分享的结果
		 */
		private getShareResult(): boolean {
			var self = this;
			var ratio = self.$shareRatios[self.$failCount++];
			if (ratio == void 0)
				ratio = 1;
			var result = Math.random() < ratio;
			if (result)
				self.$failCount = 0;
			return result;
		}

		/**
		 * 获取分享的数据
		 * @param ticket 分享票据
		 */
		public getShareInfo(ticket: string, sessionKey: string): Promise<boolean> {
			var self = this;
			return new Promise<boolean>(function (resolve, reject) {
				wx.getShareInfo({
					shareTicket: ticket,
					success: function (res) {
						let openGId = self.decryptData(res.encryptedData,
							sessionKey, res.iv).openGId;
						if (openGId) {
							let group = self.shareGroup || (self.shareGroup = []);
							if (group.indexOf(openGId) == -1) {
								group.push(openGId);
								resolve(true);
							}
							else
								resolve(false);
						}
						else
							resolve(true);
					}
				})
			});
		}

		/**
		 * 微信对称加密
		 * @param data 加密数据
		 * @param sessionKey 登录标志
		 * @param iv 加密初始向量
		 */
		public decryptData(data: string, sessionKey: string, iv: string): { openGId: string } {
			var util = wxCrypto.util;
			var dekey = util.base64ToBytes(sessionKey);
			var endata = util.base64ToBytes(data);
			var deiv = util.base64ToBytes(iv);
			var ret = null;
			try {
				let mode = new wxCrypto.mode.CBC(wxCrypto.pad.pkcs7);
				let bytes = wxCrypto.AES.decrypt(endata, dekey, {
					asBpytes: true,
					iv: deiv,
					mode: mode
				});
				if (bytes) {
					ret = JSON.parse(bytes);
				}
			} catch (err) {
				console.log(err);
			}
			return ret;
		}

		/**
		 * app文字提示
		 */
		public showToast(msg: string, bool?: boolean): void {
			var obj = {
				title: msg,
				icon: 'none'
			};
			bool && (obj.icon = 'success');
			wx.showToast(obj);
		}

		/**
		 * 显示模态对话框
		 * @param content 内容
		 * @param title 标题，默认“提示”
		 * @param hidecancel 是否隐藏取消按钮
		 */
		public showModal(content: string, title: string = '提示', hidecancel?: boolean): Promise<boolean> {
			return new Promise<boolean>(function (resolve) {
				wx.showModal({
					title: title,
					content: content,
					showCancel: !hidecancel,
					success: function (res) {
						resolve(res.confirm);
					}
				});
			});
		}

		/**
		 * 设置用户数据
		 */
		public async setUserData(map: { [key: string]: any }): Promise<boolean> {
			return new Promise<boolean>(function (resolve, reject) {
				let datas = <KVData[]>[];
				let setCS = wx.setUserCloudStorage;
				let isObj = function (v) {
					return typeof v === 'object';
				};
				for (let i in map) {
					let v = map[i];
					if (isObj(v))
						v = JSON.parse(v);
					datas.push({ key: i, value: v + '' });
				}
				// 低版本不支持
				setCS ? setCS({
					KVDataList: datas,
					success: function () {
						resolve(true);
					},
					fail: function () {
						resolve(false);
					}
				}) : reject();
			})
		}

		/**
		 * 由主域向开放域推送消息
		 * @param action 消息标志
		 * @param data 消息参数
		 */
		public postMessage(action: string, data?: any): void {
			let msg = <IWXMessage>{};
			msg.action = action;
			msg.data = data;
			wx.postMessage(msg);
		}

		/**
		 * 获取玩家信息
		 */
		public getUserInfo(): Promise<any> {
			var self = this;
			return new Promise<any>(function (resolve) {
				wx.getUserInfo({
					withCredentials: true,
					success: function (res) {
						resolve(res);
					}
				})
			});
		}

		/**
		 * 是否有我的小程序功能
		 */
		public hasShortcut(): boolean {
			var self = this;
			return self.isIPhone() && Utils.compareVersion(self.getSystemInfo().version, '6.6.7') >= 0;
		}

		// 音乐相关

		/**
		 * 创建音乐对象并播放，同音效
		 */
		public playEffect(resName: string): void {
			if (AppConfig.isOpenEffect()) {
				let self = this;
				let effects = self.effects;
				let effect = effects[resName] || (effects[resName] = []);
				let audio = effect.pop();
				if (!audio) {
					audio = <any>wx.createInnerAudioContext();
					audio.onEnded(function () {
						effect.push(audio);
					});
					audio.src = AppConfig.getMusicSrc(resName);
				}
				audio.volume = .4;
				audio.startTime = 0;
				audio.play();
			}
		}

		/**
		 * 清除音效
		 */
		public clearEffect(): void {
			var self = this;
			var effects = self.effects;
			for (let i in effects) {
				let audios = effects[i];
				for (let j in audios) {
					audios[j].destroy();
				}
			}
			self.effects = {};
		}

		/**
		 * 设置背景音乐，注意背景音乐是循环播放的
		 */
		public setBgMusic(resName: string): IMusic {
			var self = this;
			if (AppConfig.isOpenMusic()) {
				let audio = self.bgMusic;
				if (!audio) {
					audio = self.bgMusic = wx.createInnerAudioContext();
					// 重写播放
					let play = audio.play;
					audio.play = function () {
						AppConfig.isOpenMusic() &&
							play.call(audio);
					};
				}
				if (audio.param != resName) {
					audio.param = resName;
					audio.src = AppConfig.getMusicSrc(resName);
					audio.volume = .2;
					audio.loop = true;
					audio.play();
				}
				return audio;
			}
		}

		/**
		 * 播放背景音乐
		 */
		public playBgMusic(): void {
			var music = this.bgMusic;
			music && music.play();
		}

		/**
		 * 停止播放
		 */
		public pauseBgMusic(): void {
			var music = this.bgMusic;
			music && music.pause();
		}

		/**
		 * 振动
		 * @param isLong 是否长振动
		 */
		public vibrate(isLong?: boolean): void {
			if (isLong)
				wx.vibrateLong();
			else {
				wx.vibrateShort();
			}
		}

		/**
		 * 点击显示客服，显示不了则弹出图片
		 * @returns 返回是否点了连接
		 */
		public showCustomer(customer: ICustomer): Promise<boolean> {
			var self = this;
			if (!self.isPreviewImg) {
				let sdk = self.getSystemInfo().SDKVersion;
				self.isPreviewImg = true;
				return new Promise<boolean>(function (resolve) {
					let endc = function (bool) {
						self.isPreviewImg = false;
						resolve(bool);
					};
					// 打得开客服就打开，否则打开更多游戏
					if (Utils.compareVersion(sdk, '2.0.3') >= 0) {
						pfUtils.showModal(customer.content).then(function (bool) {
							if (bool) {
								wx.openCustomerServiceConversation({
									showMessageCard: customer.card,
									sendMessageTitle: customer.title,
									sendMessagePath: customer.path,
									sendMessageImg: customer.imgUrl,
									success: function (res) {
										endc(res.path == customer.path);
									},
									fail: function () {
										endc(false);
									}
								});
							}
							else
								endc(false);
						});
					}
					else {
						let url = customer.imgUrl1 || customer.imgUrl;
						self.previewImage(url).then(function () {
							endc(false);
						});
					}
				});
			}
		}

		/**
		 * 显示图片
		 */
		public previewImage(url: string): Promise<boolean> {
			return new Promise<boolean>(function (resolve) {
				wx.previewImage({
					urls: [url],
					success: function () {
						resolve(true);
					},
					fail: function () {
						resolve(false);
					}
				});
			})
		}

		/**
		 * 跳转到小程序
		 * @param appId 小程序的appId
		 * @param path 跳转路径
		 * @param extraData 扩展消息
		 */
		public toMiniProgram(appId: string, path?: string, extraData?: string): Promise<boolean> {
			var toMini = wx.navigateToMiniProgram;
			if (toMini) {
				return new Promise<boolean>(function (resolve) {
					toMini({
						appId: appId,
						path: path,
						envVersion: 'trial',
						extraData: extraData,
						success: function (res) {
							resolve(true);
						},
						fail: function (res) {
							resolve(false);
						}
					});
				});
			}
		}

		/**
		 * 显示loading，禁止点击
		 */
		public showLoading(): void {
			var self = this;
			if (!self.isShowLoading) {
				self.isShowLoading = true;
				wx.showLoading({ mask: true });
			}
		}

		/**
		 * 关闭loading
		 */
		public hideLoading(): void {
			var self = this;
			if (self.isShowLoading) {
				self.isShowLoading = false;
				wx.hideLoading();
			}
		}

		/**
		 * 返回游戏圈按钮
		 * @param x 屏幕坐标
		 * @param y
		 * @param size 大小
		 */
		public createGameClub(x: number, y: number, size: number): GameClub {
			var create = wx.createGameClubButton;
			if (create) {
				let round = Math.round;
				let info = wx.getSystemInfoSync();
				let scaleWidth = info.screenWidth / 750;
				// let disHeight = (info.screenHeight - scaleWidth * 1334) / 2;	// 高度间隔
				let style = {
					left: round(x * scaleWidth),
					top: round(y * scaleWidth),
					width: round(size * scaleWidth),
					height: round(size * scaleWidth)
				};
				return create({
					icon: 'white',
					style: style
				});
			}
			return new GameClub;
		}

		/**
		 * 创建用户信息按钮
		 */
		public createUserInfoBtn(x: number, y: number, width: number, height: number, url: string): IUserInfoBtn {
			var create = wx.createUserInfoButton;
			if (create) {
				let round = Math.round;
				let info = this.getSystemInfo();
				let scale = info.screenWidth / AppViews.stage.stageWidth;
				return create({
					type: 'image',
					text: '',
					image: url,
					style: {
						left: round(x * scale),
						top: round(y * scale),
						width: round(width * scale),
						height: round(height * scale),
						// lineHeight: round(height * scale),
						// backgroundColor: '#00ff0000',
						// color: '#ffffff',
						// textAlign: 'center',
						// fontSize: 16,
						// borderRadius: 20
					}
				})
			}
		}

		/**
		 * 显示全局性的投诉按钮
		 */
		public showGlobalFeed(): void {
			try {
				let height = this.getSystemInfo().screenHeight;
				wx.createFeedbackButton({
					type: 'text',
					text: '投诉',
					style: {
						left: 10,
						top: height * 0.4,
						width: 50,
						height: 50,
						lineHeight: 50,
						backgroundColor: '#766d6f70',
						color: '#ffffff',
						textAlign: 'center',
						fontSize: 16,
						borderRadius: 25
					}
				});
			} catch (e) { }
		}

		/**
		 * 显示视频
		 * @param adsId 视频ID
		 * @param unShow 是否不显示提示（默认文字提示，不需要时请设为true）
		 * @returns 0失败，1成功，2中断
		 */
		public showAds(adsId: string, unShow?: boolean): FPromise<number> {
			var self = this;
			return self.adsPromise || (
				self.adsPromise = new FPromise<number>(function (resolve, reject) {
					let sdk = self.getSystemInfo().SDKVersion;
					// 转换回调
					let turnCall = function (str, type = 0) {
						self.adsPromise = null;
						!unShow && self.showToast(str);
						resolve(type);
					};
					// 成功回调
					let success = function () {
						self.adsPromise = null;
						resolve(1);
					};
					// 字符串
					if (Utils.compareVersion(sdk, '2.0.4') >= 0) {
						let videoAd = wx.createRewardedVideoAd({
							adUnitId: adsId
						});
						videoAd.load().then(() => {
							// 监听回调
							let call = videoAd.onClose(function (res) {
								self.adsPromise = null;
								videoAd.offClose(call);
								if (sdk >= '2.1.0' && res) {
									if (res.isEnded) {
										success();
									} else {
										turnCall('广告未看完无法获得奖励', 2);
									}
								} else {
									success();
								}
							});
							videoAd.show();
						}).catch(err => {
							turnCall('已达到该时段内看视频次数上限，请稍后重试');
						});
						videoAd.onError(function (res) {
							turnCall('已达到该时段内看视频次数上限，请稍后重试');
						});
					}
					else {
						turnCall('该微信版本暂不支持广告播放，请升级！');
					}
				}));
		}

		/**
		 * 显示banner广告
		 * @param banner 广告ID
		 */
		public showBannerAds(bannerId: string): IBanner {
			var self = this;
			var cache = self.bannerCache;
			var banLen = cache.length;
			var bannerAd: IBanner;
			// 长度较少则创建，否则轮询
			if (banLen < 6) {
				let info = self.getSystemInfo();
				let sdk = info.SDKVersion;
				// 字符串
				if (Utils.compareVersion(sdk, '2.0.4') >= 0) {
					let stage = AppViews.stage;
					let sWidth = stage.stageWidth;
					let floor = Math.floor;
					let width = 750, height = 260;
					let x = (sWidth - width) / 2;
					let y = stage.stageHeight - height;
					let scaleWidth = info.screenWidth / sWidth;
					let style = {
						left: floor(x * scaleWidth),
						top: floor(y * scaleWidth),
						width: floor(width * scaleWidth),
						height: floor(height * scaleWidth)
					};
					bannerAd = <IBanner>wx.createBannerAd({
						adUnitId: bannerId,
						style: style
					});
					// 监听是否销毁
					bannerAd.onLoad(function () {
						if (bannerAd.isDestroy) {
							bannerAd.destroy();
						}
					});
					// 错误
					bannerAd.onError(function (res) {
						// self.destroyBannerAds(key);
						let index = cache.indexOf(bannerAd);
						index > -1 && cache.splice(index, 1);
						bannerAd.isDestroy = true;
						bannerAd.destroy();
						console.log('error', res);
					});
					// 重置对齐
					if (bannerAd.onResize) {
						bannerAd.onResize(function (e) {
							let bStyle = bannerAd.style;
							if (bStyle) {
								bStyle.top = info.windowHeight - e.height;
								bStyle.left = (info.windowWidth - e.width) / 2;
							}
						});
					}
					bannerAd.show();
					cache.push(bannerAd);
				}
			}
			else {
				bannerAd = cache.shift();
				bannerAd.show();
				cache.push(bannerAd);
			}
			return bannerAd;
		}

		/**
		 * 销毁banner广告
		 */
		private destroyBannerAds(): void {
			var cache = this.bannerCache;
			var bannerAd = cache.pop();
			if (bannerAd) {
				bannerAd.isDestroy = true;
				bannerAd.destroy();
			}
		}

		/**
		 * *发起支付
		 * @param money 单位元
		 * @returns 返回带一个布尔值，表示充值是否成功
		 */
		public async requestPaymemts(money: number): Promise<boolean> {
			var self = this;
			return new Promise<boolean>(function (resolve, reject) {
				let payment = wx.requestMidasPayment;
				if (payment && !self.isIPhone())
					// 说明：offerId应用ID，详情看公众号，buyQuantity这里指金币数，
					// buyQuantity的值实际金额必须符合微信文档要求
					payment({
						mode: 'game',
						offerId: 0,	// *需修改
						buyQuantity: 100 * money,
						zoneId: 1,
						env: 0,
						currentType: "CNY",
						platform: "android",
						success(res) {
							resolve(true);
						},
						fail() {
							resolve(false);
						}
					});
				else
					reject();
			});
		}

		/**
		 * 登录微信
		 */
		public login(): Promise<string> {
			return new Promise<string>(function (resolve) {
				wx.login({
					success: (res) => {
						resolve(res.code);
					}
				});
			});
		}

		/**
		 * 检测登录状态
		 * @returns 返回是否还处于登录状态，即sessionkey还未过期
		 */
		public checkLogin(): Promise<boolean> {
			var self = this;
			if (self.isCheckLogin) return;
			self.isCheckLogin = true;
			return new Promise<boolean>(function (resolve, reject) {
				let end = function (bool) {
					self.isCheckLogin = false;
					resolve(bool);
				};
				wx.checkSession({
					success: function () {
						end(true)
					},
					fail: function () {
						end(false)
					}
				});
			});
		}

		/**
		 * 系统信息
		 */
		public getSystemInfo(): ISystemInfo {
			return wx.getSystemInfoSync();
		}

		/**
		 * 获取系统描述
		 */
		public getSystem(): string {
			return this.getSystemInfo().system;
		}

		/**
		 * 检测是不是ipx
		 */
		public isIPhoneX(): boolean {
			var info = this.getSystemInfo();
			return info.model.indexOf('iPhone X') > -1 &&
				AppViews.stage.stageHeight == 1624;
		}

		/**
		 * 检测是否是ios
		 */
		public isIPhone(): boolean {
			return this.getSystem().indexOf('iOS') > -1;
		}

		/**
		 * 创建obj
		 */
		private $createObj(key: string, global: any): any {
			return global[key] || (global[key] = {});
		}

		/**
		 * 注册类型到window
		 * @param keys 注册的名称
		 * @param values 对应的值
		 */
		public register(keys: string[], values: any[]): void {
			var len0 = keys.length;
			var len1 = values.length;
			var index;
			var create = function (key: string, global: any) {
				return global[key] || (global[key] = {});
			};
			len1 < len0 && (len0 = len1);
			for (let i = 0; i < len0; i++) {
				let key = keys[i];
				let global = window;	// 默认存放位置
				while ((index = key.indexOf('.')) > -1) {
					let pKey = key.substr(0, index);
					key = key.substr(index + 1);
					global = create(pKey, global);
				}
				global[key] = values[i];
			}
		}

		// 不暴露出去的方法

		/**
		 * 获取全局分享数据
		 * @param imgUrl 指定图片路径
		 */
		protected getGlobalShareInfo(imgUrl?: string, query?: string): IShareInfo {
			var random = Math.random;
			var shares = this.shares;
			var config = shares && shares[random() * shares.length | 0];
			var title = '';
			if (config) {
				title = config.title;
				imgUrl = imgUrl || /*Server.CDN + */config.image;
			}
			return {
				title: title,
				imageUrl: imgUrl,
				query: query
			};
		}

		/**
		 * 获取canvas对象，该方法用于自定义分享图
		 */
		private getCanvas(): HTMLCanvasElement {
			return $canvas || ($canvas = wx.createCanvas());
		}
	}

	/**
	 * 网络状态类型
	 */
	export type NetworkType = '2g' | '3g' | '4g' | 'wifi' | 'none' | 'unknown';

	/**
	 * 网络状态工具——基于微信
	 */
	export class NetworkUtils {

		private static $netCall: [[(type: NetworkType) => void, any, boolean]] = <any>[];

		/**
		 * 获取当前网络状态
		 */
		public static getNetworkType(): Promise<NetworkType> {
			return new Promise<NetworkType>(function (resolve) {
				wx.getNetworkType({
					success: function (res) {
						resolve(res.networkTyp);
					}
				});
			});
		}

		/**
		 * 判断类型是否是最佳网络状态
		 */
		public static isBestNetworkType(type: NetworkType): boolean {
			return type == '4g' || type == 'wifi';
		}

		/**
		 * 判断网络状态是否提升
		 * @param oldType 旧类型
		 * @param newType 新类型
		 */
		public static networkUpgrade(oldType: NetworkType, newType: NetworkType): boolean {
			var types = ['none', 'unknown', '2g', '3g', '4g', 'wifi'];
			var index0 = types.indexOf(oldType);
			var index1 = types.indexOf(newType);
			return index1 > index0;
		}

		/**
		 * 添加网络变化回调
		 * @param call 回调，参数是当前的网络状态
		 * @param thisObj 回调对象
		 * @param isOnce 是否是一次性的回调，即回调结束下次网络变化不再接受
		 */
		public static addNetworkStatusChange(call: (type: NetworkType) => void, thisObj?: any, isOnce?: boolean): void {
			NetworkUtils.$netCall.push([call, thisObj, isOnce]);
		}

		/**
		 * 移除网络变化回调
		 * @param call 回调，参数是当前的网络状态
		 * @param thisObj 回调对象
		 * @returns 返回移除结果
		 */
		public static removeNetworkStatusChange(call: (type: NetworkType) => void, thisObj?: any): boolean {
			var netCall = NetworkUtils.$netCall;
			for (let i = 0, len = netCall.length; i < len; i++) {
				let item = netCall[i];
				if (item[0] == call && item[1] == thisObj) {
					netCall.splice(i, 1);
					return true;
				}
			}
			return false;
		}

		/**
		 * 执行网络变化回调——请勿手动调用
		 */
		public static excuteStatusChange(type: NetworkType): void {
			var netCall = NetworkUtils.$netCall;
			for (let i = 0, len = netCall.length; i < len; i++) {
				let item = netCall[i];
				item[0].call(item[1], type);
				// 删除一次性回调
				if (item[2]) {
					netCall.splice(i, 1);
					i--;
				}
			}
		}
	}
}