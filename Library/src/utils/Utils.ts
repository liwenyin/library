module lie {

    /**
     * 工具类，存放一些常用的且不好归类的方法
     */
    export class Utils {

        /**
         * 初始化一个长度为length，值全为value的数组
         * 注意，由于数组公用一个value，因此，value适用于基础类型，对于对象类的，请用memset2方法
         */
        public static memset<T>(length: number, value: T): T[] {
            return Array.apply(null, Array(length)).map(function () { return value; });
        }

        /**
         * 初始化一个长度为length，值通过getValue函数获取，注意getValue第一个参数是没用，第二个参数是当前数组的下标，即你返回的数据将存放在数组的该下标
         */
        public static memset2<T>(length: number, getValue: (value?: T, index?: number) => T): T[] {
            return Array.apply(null, Array(length)).map(getValue);
        }

        /**
         * 创建一个定时器
         * @param listener 定时回调
         * @param thisObject 回调所属对象
         * @param millisecond 回调间隔，单位毫秒，默认0，即帧回调
         * @param repeat 循环次数，默认一直重复
         * @param isStop 是否手动启动，即创建先不启动
         */
        public static createTimer(listener: Function, thisObject: any, millisecond?: number, repeat?: number, isStop?: boolean): egret.Timer {
            var timer = new egret.Timer(millisecond, repeat);
            timer.addEventListener(egret.TimerEvent.TIMER, listener, thisObject);
            !isStop && timer.start();
            return timer;
        }

        /**
         * 移除一个定时器
         * @param timer 被移除的定时器
         * @param listener 监听函数
         * @param thisObject 函数所属对象
         */
        public static removeTimer(timer: egret.Timer, listener: Function, thisObject?: any): void {
            timer.stop();
            timer.removeEventListener(egret.TimerEvent.TIMER, listener, thisObject);
        }

        /**
         * 自定义格式化字符串
         * @param reg 正则
         * @param str 字符串
         * @param args 填充参数
         */
        public static formatStringReg(reg: RegExp, str: string, args: any[]): string {
            for (let i in args) {
                let arg = args[i];
                if (reg.test(str))
                    str = str.replace(reg, args[i]);
                else
                    break;
            }
            return str;
        }

        /**
         * 格式化字符串
         */
        public static formatString(str: string, ...args: any[]): string {
            str = str.replace(/%%/g, '%');  // 有些识别问题出现两个%号
            return Utils.formatStringReg(/%d|%s/i, str, args);  // 忽略大小写
        }

        /**
         * 二次延迟
         */
        public static callLater(call: Function, thisObj?: any, ...params: any[]): void {
            var later = egret.callLater;
            later(function () {
                later(function () {
                    call.apply(thisObj, params);
                }, null);
            }, null);
        }

        /**
         * 数学sin cos
         */
        private static mathSinCos(attr, angle: number): number {
            return Math[attr](angle * Math.PI / 180);
        }

        /**
         * sin
         * @param angle 角度，0~360
         */
        public static sin(angle: number): number {
            return Utils.mathSinCos('sin', angle);
        }

        /**
         * cos
         * @param angle 角度，0~360
         */
        public static cos(angle: number): number {
            return angle % 180 == 90 ? 0 : Utils.mathSinCos('cos', angle);
        }

        /**
         * tan
         * @param angle 角度，0~360
         */
        public static tan(angle): number {
            return Utils.mathSinCos('tan', angle);
        }

        /**
         * 求一个点x,y逆时针旋转angle角度之后的坐标
         */
        public static getRotatPoint(x: number, y: number, angle: number): number[] {
            var self = Utils;
            var cos = self.cos, sin = self.sin;
            var x0 = x * cos(angle) - y * sin(angle);
            var y0 = x * sin(angle) + y * cos(angle);
            return [x0, y0];
        }

        /**
         * 加载资源
         * @returns 如果加载成功，返回纹理否则报错返回data
         */
        public static loadResByUrl(url: string, type: string = 'image'): Promise<any> {
            return new Promise(function (resolve, reject) {
                let loader = new egret.ImageLoader();
                loader.addEventListener(egret.Event.COMPLETE, function (event: egret.Event) {
                    let data = event.currentTarget.data;
                    if (data instanceof egret.BitmapData) {
                        let texture = new egret.Texture();
                        texture._setBitmapData(data);
                        resolve(texture);
                    }
                    else
                        resolve(data);
                }, null);
                loader.addEventListener(egret.IOErrorEvent.IO_ERROR, reject, null);
                loader.load(url);
            });
        }

        /**
         * 检测点是否在矩形上
         */
        public static pointInRect(x: number, y: number, rect: { x: number, y: number, width: number, height: number }): boolean {
            x -= rect.x;
            y -= rect.y;
            return x >= 0 && x <= rect.width && y >= 0 && y <= rect.height;
        }

        /**
         * 重写子属性方法
         * @param obj 对象
         * @param attr 属性
         * @param call 重写回调
         * @param thisObj 回调函数
         */
        public static rewriteFunc(obj: any, attr: string, call: Function, thisObj?: any): void {
            var func = obj[attr];
            obj[attr] = function () {
                func.apply(obj, arguments);
                call.apply(thisObj, arguments);
            };
        }

        /**
         * 画扇形
         * @param shape 绘画控件
         * @param radiaus 扇形半径
         * @param color 扇形颜色
         * @param startAngle 起始角度（角度转向为顺时针）
         * @param endAngle 结束角度
         * @param x 圆点坐标
         * @param y 圆点坐标
         */
        public static drawFanShaped(shape: egret.Shape, radiaus: number,
            color: number, startAngle: number, endAngle: number,
            x: number = 0, y: number = 0): void {
            var self = Utils;
            var cos = self.cos;
            var sin = self.sin;
            // 角度转弧度
            var get = function (angle) {
                return angle * Math.PI / 180;
            };
            var graphics = shape.graphics;
            graphics.clear();
            if (startAngle != endAngle) {
                graphics.beginFill(color);
                graphics.moveTo(x, y);
                // 移动到起点
                graphics.lineTo(radiaus * cos(startAngle) + x, radiaus *
                    sin(startAngle) + y);
                // 移动到终点
                graphics.lineTo(radiaus * cos(endAngle) + x, radiaus *
                    sin(endAngle) + y);
                // 画弧
                graphics.drawArc(x, y, radiaus, get(startAngle), get(endAngle));
            }
            else
                graphics.drawCircle(x, y, radiaus);
            graphics.endFill();
        }

        /**
         * 移除List的数据
         * @param list
         * @param isCache 是否是缓存数据，是的话不清除
         */
        public static removeListData(list: eui.List, isCache?: boolean): void {
            var datas = <eui.ArrayCollection>list.dataProvider;
            datas && !isCache && datas.removeAll();
            list.dataProvider = null;
        }

        /**
         * 更新列表数据
         * @param list
         * @param datas 新数据源
         */
        public static updateListData(list: eui.List, datas: any[]): void {
            var array = <eui.ArrayCollection>list.dataProvider;
            if (array)
                array.replaceAll(datas);
            else
                list.dataProvider = new eui.ArrayCollection(datas);
        }

        /**
         * 数值转为RGB
         */
        public static getRGB(value: number = 0): number[] {
            var rgb = [], count = 3;
            do {
                rgb.unshift(value % 256);
                value = value / 256 | 0;
            } while (--count);
            return rgb;
        }

        /**
         * 随机排序数组
         * @param array 数组
         * @param copy 是否复制一份新数组，即true时不改变array原来的顺序
         */
        public static randomSort<T>(array: T[], copy?: boolean): T[] {
            copy && (array = array.concat());
            return array.sort(function () { return Math.random() - 0.5 });
        }

        /**
         * 检测两个对象是否相等
         */
        public static isSame(obj0: any, obj1: any): boolean {
            if (obj0 != obj1) {
                let key0 = Object.keys(obj0);
                let key1 = Object.keys(obj0);
                if (key0.length == key1.length) {
                    for (let i in key0) {
                        let key = key0[i];
                        if (!Utils.isSame(obj0[key], obj1[key]))
                            return false;
                    }
                }
                else
                    return false;
            }
            return true;
        }

        /**
         * 获取正百分比value % range
         * @param value 求余的值
         * @param range 被求余的值
         */
        public static getPstPercent(value: number, range: number): number {
            value %= range;
            value |= 0;
            if (value < 0) {
                value = (value + range) % range;
            }
            return value;
        }

        /**
         * 是不是没有值
         */
        public static isNoValue(obj: any): boolean {
            return obj === void 0 || obj === null ||
                isNaN(obj) || obj === Infinity;
        }

        /**
         * 复制一个对象
         * @param obj 需要复制的对象
         * @param copy 被复制对象，不存在则创建
         * @returns 返回copy
         */
        public static copyObj<T>(obj: T, copy: T = Object.create(null)): T {
            var isArray = TypeUtils.isArray;
            var isObject = TypeUtils.isObject;
            for (let i in obj) {
                let data = obj[i];
                // 数组
                if (isArray(data))
                    copy[i] = (<any>data).concat();
                else if (isObject(data))
                    copy[i] = Utils.copyObj(data);
                else
                    copy[i] = data;
            }
            return copy;
        }

        /**
         * 版本号比较，版本号的格式“xx.xx.xx”，即数字之间用统一的字符隔开
         * @param version0 版本号0
         * @param version1 版本号1
         * @param format 间隔符，默认'.'
         * @returns 1大于，-1小于，0相等
         */
        public static compareVersion(version0: string, version1: string, format: string = '.'): number {
            var array0 = version0.split(format);
            var array1 = version1.split(format);
            for (let i in array0) {
                let v0 = Number(array0[i]) || 0;
                let v1 = Number(array1[i]) || 0;
                if (v0 == v1)
                    continue;
                if (v0 > v1)
                    return 1;
                return -1;
            }
            return 0;
        }

        /**
         * 获取圆形遮罩
         */
        public static getCircleMask(size: number): egret.Shape {
            var shape = new egret.Shape;
            var graphics = shape.graphics;
            var radiaus = size / 2;
            graphics.beginFill(0)
            graphics.drawCircle(radiaus, radiaus, radiaus);
            graphics.endFill();
            return shape;
        }

        /**
         * 特殊版取整，当小数大于ratio时取整
         * @param value 数值
         * @param ratio 取证系数,[0~1]，默认.5，即四舍五入
         */
        public static round(value: number, ratio: number = .5): number {
            return Math.floor(value + 1 - ratio);
        }

        /**
         * 获取数值第几位的二进制值
         * @param value 数值，请保持整数
         * @param index 下标
         */
        public static getBin(value: number, index: number): number {
            var num = Math.pow(2, index);
            return (value & num) == num ? 1 : 0;
        }

        /**
         * 界面画成纹理
         */
        public static drawToTexture(view: egret.DisplayObject): egret.RenderTexture {
            var render = new egret.RenderTexture;
            render.drawToTexture(view);
            return render;
        }

        /**
         * 添加过滤器
         */
        public static addFilter(target: egret.DisplayObject, filter: egret.Filter): void {
            var filters = target.filters || [];
            filters.push(filter);
            target.filters = filters;
        }

        /**
         * 移除过滤器
         * @param target
         * @param clzz 过滤器类
         */
        public static removeFilter(target: egret.DisplayObject, clzz: { new (): egret.Filter }): void {
            var filters = target.filters;
            var length = filters && filters.length;
            for (let i = length - 1; i >= 0; i--)
                if (filters[i] instanceof clzz)
                    filters.splice(i, 1);
        }
    }
}