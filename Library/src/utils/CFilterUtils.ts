module lie {

	var bases = [0.3086, 0.6094, 0.082];	// 通道三基色

	/**
	 * 颜色过滤器工具类
	 */
	export class CFilterUtils {

		/**
		 * 获取基础的颜色矩阵
		 */
		public static getMatrix(): number[] {
			return [
				1, 0, 0, 0, 0,
				0, 1, 0, 0, 0,
				0, 0, 1, 0, 0,
				0, 0, 0, 1, 0
			];
		}

		/**
		 * 获取灰色矩阵
		 * @param offset 偏差，默认0
		 */
		public static getGrayMatrix(offset: number = 0): number[] {
			var array = [];
			for (let i = 0; i < 3; i++) {
				array = array.concat(bases);
				array.push(0, offset);
			}
			array.push(0, 0, 0, 1, 0);	// 透明度基础
			return array;
		}

		/**
		 * 普通过滤器，没任何变化
		 */
		public static getNormal(): egret.ColorMatrixFilter {
			return new egret.ColorMatrixFilter(CFilterUtils.getMatrix());
		}

		/**
		 * 亮度调节过滤器，小于0亮度变低，否之亮度变高
		 * @param offset [-255,255]
		 */
		public static getOffset(offset: number): egret.ColorMatrixFilter {
			var matrix = CFilterUtils.getMatrix();
			for (let i = 1; i < 4; i++)
				matrix[i * 5 - 1] = offset;
			return new egret.ColorMatrixFilter(matrix);
		}

		/**
		 * 颜色取反过滤器，即色值原为x，变色后为255-x
		 */
		public static getInverse(): egret.ColorMatrixFilter {
			return new egret.ColorMatrixFilter([
				-1, 0, 0, 0, 255,
				0, -1, 0, 0, 255,
				0, 0, -1, 0, 255,
				0, 0, 0, 1, 0
			]);
		}

		/**
		 * 灰色过滤器
		 */
		public static getGray(): egret.ColorMatrixFilter {
			return new egret.ColorMatrixFilter(CFilterUtils.getGrayMatrix());
		}

		/**
		 * 获取阈值过滤器，由于颜色的阈值是0和255，即黑和白，所以也可以称为黑白照过滤器
		 * @param n 偏差，取值0-255，值越小则原色变白更多，否之变黑更多
		 */
		public static getThreshold(n: number = 128): egret.ColorMatrixFilter {
			var matrix = CFilterUtils.getGrayMatrix(-256 * n);
			for (let i = 0; i < 3; i++) {
				let index = i * 5;
				for (let j = 0; j < 3; j++)
					matrix[index + j] *= 256;
			}
			return new egret.ColorMatrixFilter(matrix);
		}
	}
}