module lie {

	var instance: ViewTree;

	/**
	 * 界面树
	 */
	export class ViewTree extends eui.Group {

		private gpTree: eui.Group;
		private gpAttr: eui.Group;

		protected constructor() {
			super();
			var rect = new eui.Rect;
			var left = new eui.Scroller;
			var right = new eui.Scroller;
			var splice = new eui.Rect;
			this.gpTree = left.viewport = new eui.Group;
			this.gpAttr = right.viewport = new eui.Group;
			splice.width = 10;
			splice.fillColor = 0x666666;
			right.percentWidth = left.percentWidth = 50;
			splice.percentHeight = right.percentHeight = left.percentHeight =
				rect.percentWidth = rect.percentHeight = 100;
			rect.alpha = .6;
			right.right = 0;
			splice.horizontalCenter = 0;
			right.bounces = left.bounces = false;
			// 添加
			this.addChild(rect);
			this.addChild(left);
			this.addChild(right);
			this.addChild(splice);
			// 单例
			instance = this;
		}

		/**
		 * 刷新
		 */
		public refresh(): void {
			var gpTree = this.gpTree;
			var node = NodeTree.create();
			node.y = 0;
			gpTree.removeChildren();
			gpTree.addChild(node);
			node.setNode(egret.sys.$TempStage);
		}

		/**
		 * 显示对象
		 */
		public showNode(display: egret.DisplayObject): void {
			var gpAttr = this.gpAttr;
			var node = NodeAttr.create();
			node.y = 0;
			gpAttr.removeChildren();
			gpAttr.addChild(node);
			node.setAttr('Root', display);
		}

		/**
		 * 获取单例
		 */
		public static get instance(): ViewTree {
			return instance || (instance = new ViewTree);
		}
	}
}