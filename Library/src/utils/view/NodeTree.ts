module lie {

	var cache: NodeTree[] = [];

	/**
	 * 树节点
	 * 大致显示：存在子节点时显示+/-号，存在名称时红名，显示类名
	 */
	export class NodeTree extends eui.Group {

		private $parser: egret.HtmlTextParser;
		private $node: egret.DisplayObject;
		private $nodeList: egret.DisplayObject[];

		private lblShow: eui.Label;		// 点击显示/隐藏子节点
		private lblName: eui.Label;
		private gpChild: eui.Group;

		protected constructor() {
			super();
			var show = this.lblShow = new eui.Label('+');
			var name = this.lblName = new eui.Label;
			var list = this.gpChild = new eui.Group;
			show.x = 10;
			name.x = 30;
			list.x = 40;
			list.y = 40;
			list.visible = false;
			// 文字初始化
			show.height = name.height = 40;
			show.width = 20;
			show.textAlign = 'center'/*egret.HorizontalAlign.CENTER*/;
			show.verticalAlign = name.verticalAlign = 'middle'/*egret.VerticalAlign.MIDDLE*/;
			show.textColor = name.textColor = 0x9acd32;
			// 列表初始化
			(list.layout = new eui.VerticalLayout).gap = 0;
			// 添加
			this.addChild(show);
			this.addChild(name);
			this.addChild(list);
			// 富文本解析器
			this.$parser = new egret.HtmlTextParser;
			// 监听
			show.addEventListener(egret.TouchEvent.TOUCH_TAP, this.updateList, this);
			name.addEventListener(egret.TouchEvent.TOUCH_TAP, this.showNode, this);
		}

		public $onRemoveFromStage(): void {
			super.$onRemoveFromStage();
			var self = this;
			self.init();
			self.$nodeList = null;
			egret.callLater(cache.push, cache, this);
		}

		/**
		 * 恢复初始化
		 */
		private init(): void {
			var self = this;
			var list = self.gpChild;
			list.visible = false;
			list.removeChildren();
			self.lblShow.text = '+';
			self.lblName.text = '';
		}

		/**
		 * 设置节点
		 */
		public setNode(node: egret.DisplayObject): void {
			var self = this;
			var clzz = (<any>node).__class__;	// 类名
			var name = node.name;
			var child = node.$children;
			var show = child && child.length > 0;
			self.$node = node;
			// 初始化
			self.init();
			// 其他
			name && (name = '<font color="#ff9dc3">' + name + ' : </font>');
			self.lblName.textFlow = self.$parser.parse(name + clzz);
			self.alpha = node.visible ? 1 : .5;
			// 待初始化列表
			self.$nodeList = show && child.concat();
			self.lblShow.visible = show;
		}

		/**
		 * 更改列表状态
		 */
		protected updateList(): void {
			var self = this;
			var list = self.gpChild;
			var isShow = list.visible = !list.visible;
			var oHeight = self.height;
			self.lblShow.text = isShow ? '-' : '+';
			isShow && self.initList();
			self.updateHeight((self.height = list.y + (isShow ? list.height : 0)) - oHeight);
		}

		/**
		 * 刷新树的高度
		 * @param sub 变化的高度值
		 */
		protected updateHeight(sub: number): void {
			var self = this;
			var parent = self.parent;
			while (parent && !(parent instanceof ViewTree)) {
				// parent.height = NaN;	// 自适应
				parent.height += sub;
				parent = parent.parent;
			}
		}

		/**
		 * 初始化列表
		 */
		protected initList(): void {
			var self = this;
			var datas = self.$nodeList;
			if (datas) {
				let list = self.gpChild;
				let length = datas.length;
				let create = NodeTree.create;
				for (let i = 0; i < length; i++) {
					let node = create();
					list.addChild(node);
					node.setNode(datas[i]);
				}
				list.height = length * 40;
				self.$nodeList = null;
			}
		}

		/**
		 * 显示节点属性
		 */
		protected showNode(): void {
			ViewTree.instance.showNode(this.$node);
		}

		/**
		 * 创建节点
		 */
		public static create(): NodeTree {
			return cache.shift() || new NodeTree;
		}
	}
}