module lie {

	var cache: NodeAttr[] = [];

	/**
	 * 属性节点
	 * 大致显示：存在子属性时显示+/-号、属性为仅含get或set时显示标签、属性名、冒号、值/类
	 */
	export class NodeAttr extends eui.Group {

		private $nodeList: any;

		private lblShow: eui.Label;
		private gpLabel: eui.Group;
		private gpChild: eui.Group;

		public constructor() {
			super();
			var top = this.gpLabel = new eui.Group;
			// 布局
			var layout = top.layout = new eui.HorizontalLayout;
			layout.gap = 10;
			layout.paddingLeft = 10;
			// 文本
			for (let i = 0; i < 5; i++) {
				let label = new eui.Label;
				label.height = 40;
				label.verticalAlign = 'middle'/*egret.VerticalAlign.MIDDLE*/;
				label.visible = false;
				top.addChild(label);
			}
			this.addChild(top);
			// 列表
			var list = this.gpChild = new eui.Group;
			list.x = 40;
			list.y = 40;
			list.visible = false;
			(list.layout = new eui.VerticalLayout).gap = 0;
			this.addChild(list);
			// +/-特殊处理
			var show = this.lblShow = <eui.Label>top.getChildAt(0);
			show.text = '+';
			show.width = 20;
			show.textAlign = 'center'/*egret.HorizontalAlign.CENTER*/;
			show.textColor = 0xffffff;
			this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.updateList, this);
		}

		public $onRemoveFromStage(): void {
			super.$onRemoveFromStage();
			var self = this;
			self.init();
			self.$nodeList = null;
			egret.callLater(cache.push, cache, this);
		}

		/**
		 * 恢复初始化
		 */
		private init(): void {
			var self = this;
			var list = self.gpChild;
			list.visible = false;
			list.removeChildren();
			self.lblShow.text = '+';
		}

		/**
		 * 设置属性
		 * @param attr 属性名
		 * @param value 属性值
		 * @param desc 额外描述
		 */
		public setAttr(attr: string, value: any, desc?: string): void {
			var self = this;
			var utils = TypeUtils;
			var curIdx = 0, labels = <eui.Label[]>self.gpLabel.$children;
			var show = false, type;
			if (utils.isArray(value)) {
				show = (<any[]>value).length > 0;
				type = 'Array';
			}
			// 对象，若是egret对象，会带__class__类型，否则使用默认，注意null也是属于特殊对象
			else if (utils.isObject(value) && value) {
				show = true;
				type = value.__class__ || 'Object';
			}
			// 字符串增加双引号	
			else if (utils.isString(value))
				type = '"' + value + '"';
			else if (utils.isFunction(value))
				type = 'Function';
			else
				type = value + '';
			// 显示+/-号
			var lblShow = labels[curIdx++];
			lblShow.visible = lblShow.includeInLayout = show;
			self.$nodeList = show && value;
			// 描述
			if (desc) {
				let lblbDesc = labels[curIdx++];
				lblbDesc.text = desc;
				lblbDesc.textColor = 0x6d7373;
			}
			// 属性
			var lblAttr = labels[curIdx++];
			lblAttr.text = attr;
			lblAttr.textColor = 0xff9dc3;
			// 冒号
			var lblColon = labels[curIdx++];
			lblColon.text = ' : ';
			lblColon.textColor = 0xffffff;
			// 类型/值
			var lblType = labels[curIdx++];
			lblType.text = type;
			lblType.textColor = 0x9acd32;
			// 隐藏多余的，从1开始
			var i = 0;
			while (++i < 5)
				labels[i].visible = i < curIdx;
			// 初始化
			self.init();
		}

		/**
		 * 更改列表状态
		 */
		protected updateList(e: egret.Event): void {
			var self = this;
			var lblShow = self.lblShow;
			e.stopImmediatePropagation();
			if (lblShow.visible) {
				var list = self.gpChild;
				var isShow = list.visible = !list.visible;
				var oHeight = self.height;
				lblShow.text = isShow ? '-' : '+';
				isShow && self.initList();
				self.updateHeight((self.height = list.y + (isShow ? list.height : 0)) - oHeight);
			}
		}

		/**
		 * 刷新树的高度
		 * @param sub 变化的高度值
		 */
		protected updateHeight(sub: number): void {
			var self = this;
			var parent = self.parent;
			while (parent && !(parent instanceof ViewTree)) {
				// parent.height = NaN;	// 自适应
				parent.height += sub;
				parent = parent.parent;
			}
		}

		/**
		 * 初始化列表
		 */
		protected initList(): void {
			var self = this;
			var datas = self.$nodeList;
			if (datas) {
				let clzz = NodeAttr;
				let list = self.gpChild;
				let keys = clzz.getKeys(datas);
				let length = keys.length;
				let create = clzz.create;
				let desc = clzz.getDesc;
				for (let i = 0; i < length; i++) {
					let node = create();
					let attr = keys[i];
					list.addChild(node);
					node.setAttr(attr, datas[attr], desc(datas, attr));
				}
				list.height = length * 40;
				self.$nodeList = null;
			}
		}

		/**
		 * 获取属性值
		 */
		protected static getKeys(obj: any): string[] {
			var array = [];
			for (let i in obj)
				array.push(i);
			array.sort(function (a, b) {
				return a > b ? 1 : -1;
			});
			return array;
		}

		/**
		 * 获取对象属性的描述，若不属于自身，则会去原型链上查找
		 * @param obj 对象
		 * @param attr 属性
		 */
		protected static getDesc(obj: any, attr: string): string {
			var str = '';
			if (obj) {
				let info = Object.getOwnPropertyDescriptor(obj, attr);
				if (info) {
					let get = !!info.get;
					let set = !!info.set;
					if (get)
						set || (str = 'get');
					else
						set && (str = 'set');
				}
				else
					str = NodeAttr.getDesc(obj.__proto__, attr);
			}
			return str;
		}

		/**
		 * 创建节点
		 */
		public static create(): NodeAttr {
			return cache.shift() || new NodeAttr;
		}
	}
}