module lie {

	var app: AppViews;	// 存放单例

	/**
	 * 层级容器
	 */
	class Container extends egret.DisplayObjectContainer {

		/**
		 * 移除子控件触发
		 */
		public $doRemoveChild(index: number, notifyListeners?: boolean): egret.DisplayObject {
			var target = super.$doRemoveChild(index, notifyListeners);
			if (target) {
				// 当前层级最顶层
				if (!(<UIComponent><any>this).unNest) {
					let last = AppViews.getTopComponent(this);
					last && last.onShow();
				}
			}
			return target;
		}
	}

	/**
	 * 应用的视图管理类
	 */
	export class AppViews {

		private $panelLevel: Container;		// 面板层
		private $dialogLevel: Container;	// 对话框层
		private $topLevel: Container;		// 顶层

		protected constructor() {

		}

		/**
		 * 初始化
		 */
		private init(): void {
			var self = this;
			var clzz = AppViews;
			var stage = clzz.stage;
			stage.removeChildren();
			// 层级部署
			self.$panelLevel = self.addLevel('panel');
			self.$dialogLevel = self.addLevel('dialog');
			self.$topLevel = self.addLevel('top');
		}

		/**
		 * 获取面板层
		 */
		public get panelLevel(): Container {
			return this.$panelLevel;
		}

		/**
		 * 获取对话框层
		 */
		public get dialogLevel(): Container {
			return this.$dialogLevel;
		}

		/**
		 * 获取最顶层，注：顶层的东西随便加，记得清理
		 */
		public get topLevel(): Container {
			return this.$topLevel;
		}

		/**
		 * 添加层
		 * @param name 名称
		 */
		protected addLevel(name?: string): Container {
			var stage = AppViews.stage;
			var container = new Container;
			container.name = name;
			container.width = stage.stageWidth;
			container.height = stage.stageHeight;
			container.touchEnabled = false;
			stage.addChild(container);
			return container;
		}

		/**
		 * 获取层
		 */
		public getLevel(name?: string): Container {
			return <Container>AppViews.stage.getChildByName(name);
		}

		/**
		 * 获取最顶层
		 */
		public get curPanel(): UIComponent {
			var panel = this.panelLevel;
			return <UIComponent>panel.$children[panel.numChildren - 1];
		}

		/**
		 * 获取最顶对话框
		 */
		public get curDialog(): Dialog {
			var panel = this.dialogLevel;
			return <Dialog>panel.$children[panel.numChildren - 1];
		}

		// 静态

		/**
		 * 是否初始化界面
		 */
		public static get isInit(): boolean {
			return !!app;
		}

		/**
		 * 获取视图管理类单例
		 */
		public static app(): AppViews {
			if (!app) {
				app = new AppViews;
				app.init();
			}
			return app;
		}

		/**
		 * 获取舞台
		 */
		public static get stage(): egret.Stage {
			return egret.sys.$TempStage;	// egret.MainContext.instance.stage
		}

		/**
		 * 获取舞台宽度
		 */
		public static get stageWidth(): number {
			return AppViews.stage.stageWidth;
		}

		/**
		 * 获取舞台高度
		 */
		public static get stageHeight(): number {
			return AppViews.stage.stageHeight;
		}

		// 重点，层及控制管理体现

		/**
		 * 获取当前面板的最顶元素（该面板没有则取下一面板）
		 */
		public static getTopComponent(cont: Container): UIComponent {
			if (cont.numChildren == 0) {
				let parent = cont.parent;
				let index = parent.getChildIndex(cont);
				cont = null;
				for (let i = index - 1; i >= 0; i--) {
					cont = <any>parent.getChildAt(i);
					if (cont.numChildren)
						break;
				}
				if (!cont)
					return null;
			}
			return <UIComponent>cont.$children[cont.numChildren - 1];
		}

		/**
		 * 获取最顶的控件——对话框之下
		 */
		public static getTopCommont(): lie.UIComponent {
			return AppViews.getTopComponent(AppViews.app().dialogLevel);
		}

		/**
		 * 获取当前面板
		 */
		public static get curPanel(): UIComponent {
			return app && app.curPanel;
		}

		/**
		 * 获取当前对话框
		 */
		public static get curDialog(): Dialog {
			return app && app.curDialog;
		}

		/**
		 * 插入一个控件，并居中
		 * @param attr AppViews对象的属性名
		 * @param clzz 需要添加的对象类
		 */
		private static pushChild<T extends UIComponent, Z>(attr: string, clzz: { new (data?: Z): T }, data?: Z): T {
			var self = AppViews;
			var child = new clzz(data);
			var container = <Container>self.app()[attr];
			// 隐藏
			if (!child.unNest) {
				let last = self.getTopComponent(container);	// 当前层级最顶层
				last && last.onHide();
			}
			container.addChild(child);
			self.setInCenter(child);
			return child;
		}

		/**
		 * 移除层里的控件
		 * @param attr AppViews对象的属性名
		 * @param clzz 需要移除的对象类
		 */
		private static removeChild(attr: string, clzz?: { new (data?: any): UIComponent }): void {
			var panel = <Container>AppViews.app()[attr];
			var num = panel.numChildren;
			if (num > 0) {
				if (clzz) {

					let top = panel.getChildAt(num - 1);
					for (let i = 0; i < num; i++) {
						let child = panel.getChildAt(i);
						if (child instanceof clzz) {
							panel.removeChildAt(i);
							i--;
							num--;
						}
					}
				}
				else
					panel.removeChildren();
			}
		}

		/**
		 * 移除clzz其上面的面板，clzz不传参时则返回上一级
		 */
		private static backChild(attr: string, clzz?: any): void {
			var self = AppViews;
			var panel = <Container>self.app()[attr];
			var num = panel.numChildren;
			if (!clzz) {
				panel.removeChildAt(--num);
			}
			else {
				for (let i = 0; i < num; i++) {
					let child = <UIComponent>panel.getChildAt(i);
					if (child instanceof clzz) {
						i++;
						for (let j = i; j < num; j++)
							panel.removeChildAt(i);
						break;
					}
				}
			}
		}

		/**
		 * 检测层级是否含有该类控件，包含继承该类的子类
		 */
		private static hasChild(attr: string, clzz: any): boolean {
			if (app) {
				let panel = <Container>app[attr];
				for (let i = 0, num = panel.numChildren; i < num; i++)
					if (panel.getChildAt(i) instanceof clzz)
						return true;
			}
			return false;
		}

		/**
		 * 将子控件设置在父控件中心点
		 */
		public static setInCenter(child: egret.DisplayObject): void {
			var stage = AppViews.stage;
			child.x = (stage.stageWidth - child.width) / 2;
			child.y = (stage.stageHeight - child.height) / 2;
		}

		/**
		 * 新建一个面板并放入
		 */
		public static pushPanel<T extends UIComponent, Z>(clzz: { new (data?: Z): T }, data?: Z): T {
			return AppViews.pushChild('panelLevel', clzz, data);
		}

		/**
		 * 新建一个对话框并放入
		 */
		public static pushDialog<T extends Dialog, Z>(clzz: { new (data?: Z): T }, data?: Z): T {
			return AppViews.pushChild('dialogLevel', clzz, data);
		}

		/**
		 * 新建一个顶层控件并放入
		 */
		public static pushTop<T extends UIComponent, Z>(clzz: { new (data?: Z): T }, data?: Z): T {
			return AppViews.pushChild('topLevel', clzz, data);
		}

		/**
		 * 移除面板
		 * @param clzz 面板类，不传参数时则全部移除
		 */
		public static removePanel(clzz?: { new (data?: any): UIComponent }): void {
			AppViews.removeChild('panelLevel', clzz);
		}

		/**
		 * 移除对话框
		 * @param clzz 对话框类，不传参数时则全部移除
		 */
		public static removeDialog(clzz?: { new (data?: any): Dialog }): void {
			AppViews.removeChild('dialogLevel', clzz);
		}

		/**
		 * 移除顶层控件
		 * @param clzz 顶层控件类，不传参数时则全部移除
		 */
		public static removeTop(clzz?: { new (data?: any): Dialog }): void {
			AppViews.removeChild('topLevel', clzz);
		}

		/**
		 * 移除clzz其上面的面板，clzz不传参时则返回上一面板
		 */
		public static backPanel(clzz?: any): void {
			AppViews.backChild('panelLevel', clzz);
		}

		/**
		 * 检测是否包含该类（包含继承该类的子类）的面板
		 */
		public static hasPanel(clzz?: any): boolean {
			return AppViews.hasChild('panelLevel', clzz);
		}

		/**
		 * 检测是否包含该类（包含继承该类的子类）的对话框
		 */
		public static hasDialog(clzz?: any): boolean {
			return AppViews.hasChild('dialogLevel', clzz);
		}
	}
}