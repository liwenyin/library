module lie {

	/**
	 * 字符串操作工具类
	 */
	export class StrUtils {

        /**
         * 切割字符串
         */
		public static splitStr(str: string, format: string): string[] {
			return str ? str.split(format) : [];
		}

		/**
         * 切割成数字数组
         * @param str 数组
         * @param format 切割符号
         */
		public static splitNumberArray(str: string, format: string): number[] {
			return str ? str.split(format).map(function (v) { return Number(v) }) : [];
		}

        /**
         * 切割成数字Map
         * @param str 数组
         * @param format 切割符号
         */
		public static splitNumberMap(str: string, format: string): { [key: string]: number } {
			var map = <{ [key: string]: number }>{}
			str && str.split(format).map(function (v) {
				map[v] = 1;
			});
			return map;
		}

        /**
         * 指定位置改为大写，默认首字母大写
         * @param str 字符串
         * @param index 改为大写的位置，默认0
         */
		public static toUpperIndex(str: string, index: number = 0): string {
			var char = str[index];
			char && (char = char.toUpperCase());
			return str.substr(0, index) + char + str.substr(index + 1);
		}
	}
}