module lie {

	/**
	 * 动画
	 */
	export class LAnimation extends eui.sys.Animation {

		/**
		 * 清理，一经清理不可再用
		 */
		public clear(): void {
			var self = this;
			self.stop();
			self.easerFunction = self.endFunction = self.updateFunction = null;
		}
	}
}