module lie {
	/**
	 * 监听对象
	 */
	interface TargetEvent {
		type: string;
		listener: Function;
		thisObject: any;
		useCapture: boolean;
		priority: number;
	}

	/**
	 * 缩放对象
	 */
	interface ScaleTarget extends egret.DisplayObject {
		$hashBegin?: boolean;// 是否按下
		$bgScaleX?: number;	// 初始缩放值
		$bgScaleY?: number;
		$evtScale?: number;	// 缩放倍数
	}

	/**
	 * 颜色加深对象
	 */
	interface IShadeTarget extends egret.DisplayObject {
		$hashBegin?: boolean;			// 是否按下
		$shade?: number;				// 深度系数
		$shadeAlpha?: Float32Array;		// 颜色偏差
	}

    /**
     * 时间控件
     */
	export interface ITFStepDisplay extends egret.DisplayObject {
		timer: number;
	}

	/**
	 * 控件监听工具类
	 */
	export class EventUtils {

		protected static $scaleTime: number = 100;	// 缩放动画时间
		protected static $shadeTime: number = 100;	// 加深动画时间

		/**
		 * 是否有该监听
		 * @param type 监听类型
		 * @param call 空时只检测类型
		 * @param thisObj 回调所属对象
		 */
		public static hasEventListener(target: egret.EventDispatcher, type: string, call?: Function, thisObj?: any): boolean {
			var value = target.$EventDispatcher;
			var list = [].concat(value[1] || [], value[2] || []);
			var datas = <TargetEvent[]>list[type];
			if (datas && call) {
				for (let i in datas) {
					let event = datas[i];
					if (event.listener == call && event.thisObject == thisObj)
						return true;
				}
			}
			// datas存在的情况下，call为空返回真
			return !!datas && !call;
		}

		/**
		 * 移除控件上的所有监听，该方法也适用于没有通过addEventListener来添加的控件
		 */
		public static removeEventListener(target: egret.EventDispatcher): void {
			var value = target.$EventDispatcher;
			var list = [].concat(value[1] || [], value[2] || []);
			for (let i in list) {
				let item = list[i];
				for (let j in item) {
					let datas = <TargetEvent[]>item[j];
					for (let k in datas) {
						let event = datas[k];
						target.removeEventListener(event.type, event.listener, event.thisObject, event.useCapture);
					}
				}
			}
		}

		/**
		 * 移除控件上的所有监听，该方法也适用于没有通过addEventListener来添加的控件
		 */
		public static removeEventByType(target: egret.EventDispatcher, type: string): void {
			var value = target.$EventDispatcher;
			var list = [].concat(value[1] || [], value[2] || []);
			for (let i in list) {
				let item = list[i];
				for (let j in item) {
					let datas = <TargetEvent[]>item[j];
					for (let k in datas) {
						let event = datas[k];
						if (event.type == type)
							target.removeEventListener(type, event.listener, event.thisObject, event.useCapture);
					}
				}
			}
		}

		/**
		 * 移除root往下所有的点击事件
		 */
		public static removeEventListeners(root: egret.EventDispatcher): void {
			var self = EventUtils;
			if (root instanceof egret.DisplayObjectContainer)
				for (let i = 0, num = root.numChildren; i < num; i++)
					self.removeEventListeners(root.getChildAt(i));
			else
				self.removeEventListener(root);
		}

		/**
		 * 添加缩放监听，记得用removeEventListener来移除这个监听
		 */
		public static addScaleListener(target: ScaleTarget, scale: number = 0.95): void {
			var self = EventUtils;
			target.$evtScale = scale;
			target.$bgScaleX = target.scaleX;
			target.$bgScaleY = target.scaleY;
			self.addTouchBeginListener(target, self.onScaleBegin, self);
			self.addTouchFinishListener(target, self.onScaleEnd, self);
		}

		/**
		 * 缩放开始
		 */
		protected static onScaleBegin(event: egret.TouchEvent): void {
			var target = <ScaleTarget>event.currentTarget;
			var tween = egret.Tween;
			var scale = target.$evtScale;
			var scaleX = target.scaleX * scale;
			var scaleY = target.scaleY * scale;
			target.$hashBegin = true;
			tween.removeTweens(target);
			tween.get(target).to({ scaleX: scaleX, scaleY: scaleY }, EventUtils.$scaleTime);
		}

		/**
		 * 缩放结束
		 */
		protected static onScaleEnd(event: egret.TouchEvent): void {
			var target = <ScaleTarget>event.currentTarget;
			var time = EventUtils.$scaleTime;
			var tween = egret.Tween;
			var scaleX = target.$bgScaleX;
			var scaleY = target.$bgScaleY;
			var bScaleX = scaleX * 1.1;
			var bScaleY = scaleY * 1.1;
			tween.removeTweens(target);
			target.$hashBegin && tween.get(target).to({ scaleX: bScaleX, scaleY: bScaleY }, time).to({ scaleX: scaleX, scaleY: scaleY }, time);
			target.$hashBegin = void 0;
		}

		/**
		 * 添加颜色加深监听
		 * @param target 目标
		 * @param shade 加深层度，值越小，颜色越深，反之越浅，取值范围[-255,255]
		 */
		public static addShadeListener(target: IShadeTarget, shade: number = -20): void {
			var self = EventUtils;
			var filter = CFilterUtils.getNormal();
			target.$shade = shade;
			target.$shadeAlpha = filter.$uniforms.colorAdd;
			Utils.addFilter(target, filter);
			self.addTouchBeginListener(target, self.onShadeBegin, self);
			self.addTouchFinishListener(target, self.onShadeEnd, self);
			// 离开舞台则移除
			Utils.rewriteFunc(target, '$onRemoveFromStage', self.clearShade, self);
		}

		/**
		 * 加深开始
		 */
		protected static onShadeBegin(event: egret.Event): void {
			var target = <IShadeTarget>event.currentTarget;
			var shade = target.$shade / 255;	// 跟Laya不一样
			target.$hashBegin = true;
			egret.Tween.get(target.$shadeAlpha).to({ x: shade, y: shade, z: shade }, EventUtils.$shadeTime);
		}

		/**
		 * 缩放结束
		 */
		protected static onShadeEnd(event: egret.Event): void {
			var target = <IShadeTarget>event.currentTarget;
			if (target.$hashBegin) {
				let time = EventUtils.$shadeTime;
				egret.Tween.get(target.$shadeAlpha).to({
					x: 0, y: 0, z: 0
				}, time);
				target.$hashBegin = void 0;
			}
		}

		/**
		 * 清除shade
		 * @param target 
		 */
		protected static clearShade(target: IShadeTarget): void {
			delete target.$shadeAlpha;
			Utils.removeFilter(target, egret.ColorMatrixFilter);
		}

		// 常用的监听类型归类

		/**
		 * 添加点击按下监听
		 */
		public static addTouchBeginListener(target: egret.EventDispatcher, call: Function, thisObj?: any): void {
			target.addEventListener(egret.TouchEvent.TOUCH_BEGIN, call, thisObj);
		}

		/**
		 * 添加点击移动监听
		 */
		public static addTouchMoveListener(target: egret.EventDispatcher, call: Function, thisObj?: any): void {
			target.addEventListener(egret.TouchEvent.TOUCH_MOVE, call, thisObj);
		}

		/**
		 * 添加点击谈起监听
		 */
		public static addTouchEndListener(target: egret.EventDispatcher, call: Function, thisObj?: any): void {
			target.addEventListener(egret.TouchEvent.TOUCH_END, call, thisObj);
		}

		/**
		 * 添加TouchTap监听
		 */
		public static addTouchTapListener(target: egret.DisplayObject, call: Function, thisObj?: any, useCapture?: boolean): void {
			EventUtils.addClickEffect(target);
			target.addEventListener(egret.TouchEvent.TOUCH_TAP, call, thisObj, useCapture);
		}

		/**
		 * 在TouchTap的基础上进行缩放
		 */
		public static addTouchTapScaleListener(target: ScaleTarget, call: Function, thisObj?: any, scale?: number, useCapture?: boolean): void {
			var self = EventUtils;
			self.addScaleListener(target, scale);
			self.addTouchTapListener(target, call, thisObj, useCapture);
		}

		/**
		 * 添加监听结束监听
		 */
		public static addTouchFinishListener(target: egret.EventDispatcher, finish: Function, thisObj?: any): void {
			var event = egret.TouchEvent;
			target.addEventListener(event.TOUCH_END, finish, thisObj);
			target.addEventListener(event.TOUCH_CANCEL, finish, thisObj);
			target.addEventListener(event.TOUCH_RELEASE_OUTSIDE, finish, thisObj);
		}

		/**
		 * 添加按住监听
		 * @param target
		 * @param begin 按住时的回调
		 * @param end 松手时的回调，会调用多次，请自己在end里判断
		 */
		public static addTouchingListener(target: egret.EventDispatcher, begin: Function, end: Function, thisObj?: any): void {
			var self = EventUtils;
			self.addTouchBeginListener(target, begin, thisObj);
			self.addTouchFinishListener(target, end, thisObj);
		}

		/**
		 * 添加移动控件监听
		 * @param target 监听控件
		 * @param call 移动时的回调，回调参数有俩，横坐标的移动值和纵坐标的移动值
		 * @param thisObj 回调的所属对象
		 */
		public static addMoveingListener(target: egret.DisplayObject, call: (disX: number, disY: number) => void, thisObj?: any): void {
			var self = EventUtils;
			var event = egret.TouchEvent;
			var touchX, touchY;
			self.addTouchBeginListener(target, function (e) {
				e.stopImmediatePropagation();
				touchX = e.stageX;
				touchY = e.stageY;
			});
			self.addTouchMoveListener(target, function (e) {
				e.stopImmediatePropagation();
				let newX = e.stageX, newY = e.stageY;
				call.call(thisObj, newX - touchX, newY - touchY);
				touchX = newX;
				touchY = newY;
			});
		}

		// 音效相关

		/**
		 * 播放音效
		 */
		public static $playBtnEffect(): void {
			pfUtils.playEffect('button');
		}

		/**
		 * 添加点击音效
		 */
		public static addClickEffect(target: egret.DisplayObject): void {
			var self = EventUtils;
			self.addTouchBeginListener(target, self.$playBtnEffect, self);
		}
	}
}