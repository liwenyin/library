module lie {

	/**
	 * 时效性签名
	 */
	export class TimeSign<T> {

		private datas: any[];
		private updateTime: number;
		private resetTime: number;

		constructor(defData: T, secData: T, resetTime?: number) {
			// 格式化时间
			if (resetTime < 0 || isNaN(resetTime))
				resetTime = 0;
			else
				resetTime %= 86400000;
			this.datas = [defData, secData];
			this.resetTime = resetTime;
		}

		/**
		 * 获取数据
		 */
		public getData(): T {
			var self = this, update = self.updateTime,
				resetTime = self.resetTime, getPerTime = TimeUtils.getPerTimeMil;
			var perTime0 = update ? getPerTime(update) : -1;
			var index = Number(perTime0 >= resetTime ||
				getPerTime(egret.getTimer()) < resetTime)
			return self.datas[index];
		}

		/**
		 * 更新调用时间，标记使用了数据次数
		 */
		public update(): void {
			this.updateTime = egret.getTimer();
		}
	}

	/**
	 * 时间工具类
	 */
	export class TimeUtils {

		// private static cache: any = {};

		// /**
		//  * 计时开始
		//  * @param key 标志
		//  */
		// public static begin(key: string): void {
		// 	TimeUtils.cache[key] = Date.now();
		// }

		// /**
		//  * 计时结束
		//  * @param key 标志
		//  */
		// public static end(key: string): void {
		// 	var cache = TimeUtils.cache;
		// 	var time = cache[key];
		// 	if (time) {
		// 		delete cache[key];
		// 		console.log(key + ' used time:' + (Date.now() - time));
		// 	}
		// }

		/**
		 * 获取秒数位于一天中的时间
		 */
		public static getPerTime(second: number): number {
			return (second + 8 * 3600) % 86400;
		}

		/**
		 * 获取秒数位于一天中的时间
		 */
		public static getPerTimeMil(milsecond: number): number {
			return (milsecond + 8 * 3600000) % 86400000;
		}

        /**
         * 获取天数
         * @param second
         */
		public static getDay(second: number): number {
			return (second + 8 * 3600) / 86400 | 0;
		}

		/**
		 * 获取时间点的零点时间（单位：秒）
		 */
		public static getZero(second: number): number {
			return second - TimeUtils.getPerTime(second);
		}

        /**
         * 检测两个秒数是否同一天
         */
		public static isSameDay(second0: number, second1: number): boolean {
			var get = TimeUtils.getDay;
			return get(second0) == get(second1);
		}
	}
}