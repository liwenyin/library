module lie {

	var utils = {};

	/**
	 * 缓存工具类
	 */
	export class CacheUtils<T> {

		private key: string;		// 属性名
		private caches: T[];		// 属性值
		// 通用创建
		private $createFunc: (any) => T;	// 创建缓存对象的方法
		private $createThis: any;			// 方法所属对

		/**
		 * 为obj创建
		 */
		public constructor(key: string) {
			this.key = key;
			this.caches = utils[key] = [];
		}

		/**
		 * 初始化创建缓存方法
		 */
		public initCreate(func: () => T, thisObj?: any): void {
			this.$createFunc = func;
			this.$createThis = thisObj;
		}

		/**
		 * 如果缓存不足，则通过创建获取缓存
		 */
		public getByCreate(): T {
			var self = this;
			var cache = self.getCache();
			if (!cache)
				cache = self.$createFunc.call(self.$createThis);
			return cache;
		}

		/**
		 * 单个的添加缓存
		 */
		public addCache(cache: T): void {
			this.caches.push(cache);
		}

		/**
		 * 将datas全存入缓存
		 */
		public pushCache(datas: T[]): void {
			var caches = this.caches;
			for (let i in datas)
				caches.push(datas[i]);
		}

		/**
		 * 获取首个缓存
		 */
		public getCache(): T {
			return this.caches.shift();
		}

		/**
		 * 移除固定下标缓存
		 */
		public removeCache(index: number): void {
			this.caches.splice(index, 1);
		}

		/**
		 * 一经调用，则该工具就没用了
		 */
		public clear(): void {
			var self = this;
			delete utils[self.key];
			self.key = self.caches = null;
		}

		/**
		 * 创建一个缓存工具
		 */
		public static create<T>(key: string): CacheUtils<T> {
			if (key && !utils[key])
				return new CacheUtils<T>(key);
		}
	}
}