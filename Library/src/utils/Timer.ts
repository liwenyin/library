module lie {

	/**
	 * 定时器，在原有基础上增加“运行时间”，同时方便定时器的清理
	 */
	export class Timer extends egret.Timer {

		private $runTime: number = 0;	// 已运行时间
		private $lastTime: number;		// 上一次时间

		/**
		 * @param call 回调
		 * @param thisObj 回调所属对象
		 * @param millisecond 毫秒数，默认0
		 * @param repeate 执行次数，默认循环执行
		 * @param isStop 是否创建但不运行
		 */
		public constructor(call: Function, thisObj?: any, millisecond: number = 0, repeate?: number, isStop?: boolean) {
			super(millisecond, repeate);
			this.addEventListener(egret.TimerEvent.TIMER, call, thisObj);
			!isStop && this.start();
		}

		/**
		 * 重写
		 */
		public start(): void {
			var self = this;
			if (!self.running)
				self.$lastTime = egret.getTimer();
			super.start();
		}

		/**
		 * 重写
		 */
		public stop(): void {
			var self = this;
			if (self.running) {
				let nowT = egret.getTimer();
				self.$runTime += nowT - self.$lastTime;
				self.$lastTime = nowT;
			}
			super.stop();
		}

		/**
		 * 获取运行的时间
		 */
		public get runTime(): number {
			var self = this;
			return self.$runTime + (self.running ?
				egret.getTimer() - self.$lastTime : 0);
		}

		/**
		 * 重置时间，归0
		 */
		public reset(): void {
			super.reset();
			var self = this;
			self.$runTime = 0;
			self.$lastTime = Date.now();
		}

		/**
		 * 清除定时器，一经清除，将不可再用
		 */
		public clear(): void {
			var self = this;
			self.stop();
			EventUtils.removeEventListener(self);
		}
	}
}