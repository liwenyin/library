module lie {

	var getM = egret.HttpMethod.GET;
	var postM = egret.HttpMethod.POST;
	var load: ILoading;

	/**
	 * loading接口
	 */
	interface ILoading {
		showLoading(): void;	// 显示loading
		hideLoading(): void;	// 隐藏loading
	}

	/**
	 * 请求参数
	 */
	interface IHttpParam {
		url: string;			// 请求地址
		param?: any;			// 请求参数，一个对象
		type?: number;			// 请求结果，0二进制文件，1文本，2JSON对象，默认2
		noLoading?: boolean;	// 不显示Loading，默认显示 
	}

	/**
	 * Http请求类
	 * 注：如果请求失败，会返回字符串，请用catch抓取
	 */
	export class Http {

		protected isJson: boolean; 		// 返回结果是否是JSON格式
		protected hasLoding: boolean;	// 是否显示loadng

		private $loading: number;
		private $timeout: number;
		private $request: egret.HttpRequest;
		private $comCall: (any) => void;
		private $errorCall: (any) => void;
		private $thisObj: any;

		protected constructor(url: string, method: string, param?: any, type?: number, noLoading?: boolean) {
			var request = this.$request = new egret.HttpRequest();
			var isBuffer = type == 0;
			this.isJson = !(isBuffer || type == 1);
			this.hasLoding = !noLoading;
			if (method == postM)
				request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			request.responseType = isBuffer ? 'arraybuffer' : 'text';
			request.open(url, method);
			request.send(Http.getUrlParam(param));
		}

		/**
		 * 添加返回请求
		 */
		protected addCall(call: (any) => void, error: (any) => void, thisObj?: any): void {
			var self = this;
			var request = self.$request;
			self.$comCall = call;
			self.$errorCall = error;
			self.$thisObj = thisObj;
			request.addEventListener(egret.Event.COMPLETE, self.onGetComplete, self);
			request.addEventListener(egret.IOErrorEvent.IO_ERROR, self.onGetIOError, self);
			// request.addEventListener(egret.ProgressEvent.PROGRESS, self.onGetProgress, self);	
			self.$loading = egret.setTimeout(self.showLoading, self, 400);
		}

		/**
		 * 请求成功
		 */
		private onGetComplete(event: egret.Event): void {
			var self = this;
			var request = <egret.HttpRequest>event.currentTarget;
			var response = request.response;
			try {
				let data = self.isJson ? JSON.parse(response) : response;
				self.excuteCall(data);
			} catch (e) {
				self.excuteCall('非JSON格式：' + response, true);
			}
		}

		/**
		 * 请求失败
		 */
		private onGetIOError(): void {
			this.excuteCall('连接服务器失败', true);
		}

		/**
		 * 执行回调
		 */
		protected excuteCall(data: any, isError?: boolean): void {
			var self = this;
			var call = isError ? self.$errorCall : self.$comCall;
			call && call.call(self.$thisObj, data);
			self.clear();
			// 关闭
			if (self.$loading) {
				egret.clearTimeout(self.$loading);
				self.$loading = null;
			}
			else if (self.$timeout) {
				egret.clearTimeout(self.$timeout);
				self.$timeout = null;
				self.hasLoding && load && load.hideLoading();
			}
		}

		/**
		 * 显示loading
		 */
		protected showLoading(): void {
			var self = this;
			self.$loading = null;
			self.hasLoding && load && load.showLoading();
			// 超时检测
			self.$timeout = egret.setTimeout(self.onGetIOError, self, 10000);
		}

		/**
		 * 清除请求
		 */
		public clear(): void {
			var self = this;
			EventUtils.removeEventListener(self.$request);
			self.$request = self.$comCall = self.$errorCall = self.$thisObj = null;
		}

		//// 静态方法 ////

		/**
		 * 发起请求
		 */
		private static $request(data: IHttpParam, method: string): Promise<any> {
			var http = new Http(data.url, method, data.param, data.type, data.noLoading);
			return new Promise<any>(function (resolve, reject) {
				http.addCall(resolve, reject);
			});
		}

		/**
		 * 设置请求中loading的工具
		 * @param newLoad 新的loading工具
		 */
		public static setLoading(newLoad: ILoading): void {
			load = newLoad;
		}

		/**
		 * 将对象转为伴随这url的参数
		 * @param param 参数对象
		 * @param sortFunc 对象key值的排序
		 */
		public static getUrlParam(param: any, sortFunc?: (a: string, b: string) => number): string {
			if (param) {
				let isO = TypeUtils.isObject;
				let str = JSON.stringify;
				let attr = [];
				for (let i in param) {
					let data = param[i];
					if (isO(data)) {
						data = str(data);
					}
					attr.push(i + '=' + data);
				}
				sortFunc && attr.sort(sortFunc);
				return attr.join('&');
			}
			return '';
		}

        /**
         * 获取网页url上的参数
         */
		public static getQueryString(name: string): string {
			var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
			var ret = window.location.search.substr(1).match(reg);
			return ret ? decodeURIComponent(ret[2]) : '';
		}

		/**
		 * 发起post请求
		 * @param data 参数
		 */
		public static post(data: IHttpParam): Promise<any> {
			return Http.$request(data, postM);
		}

		/**
		 * 发起get请求
		 * @param data 参数
		 */
		public static get(data: IHttpParam): Promise<any> {
			return Http.$request(data, getM);
		}
	}
}