module lie {

	/**
	 * 转盘group
	 * 注：需先设置好自身宽高再设置子项等数据，自身最好是方形，宽高一致
	 */
	export class DialGroup<T> extends eui.Group {

		private $cache: any[] = [];		// 缓存：子项类、数据、半径、是否初始化、回调、回调所属对象
		// 动画数据
		private $frameTotal: number;	// 当前经过的帧数
		private $frameAngle: number;	// 每一帧的旋转角度
		private $framCycle: number;		// 速度减慢周期
		private $timer: Timer;			// 转盘动画定时器
		private $prizeIndex: number;	// 奖励下标
		private $prizeAngle: number;	// 定位角度

		public constructor() {
			super();
			this.$timer = new Timer(this.onAnimation, this, 0, -1, true);
		}

		public $onRemoveFromStage(): void {
			super.$onRemoveFromStage();
			var self = this;
			self.$timer.clear();
			self.$cache = self.$timer = null;
		}

		/**
		 * 初始化缓存
		 */
		private $initCache(index: number, value: any): void {
			var self = this;
			var cache = self.$cache;
			if (cache[index] != value) {
				cache[3] = false;	// 防止如果initDial报错，则无法开启转盘
				cache[index] = value;
				self.initDial();
			}
		}

		/**
		 * 设置渲染子项
		 */
		public set itemRender(clzz: { new (data: T): eui.Component }) {
			this.$initCache(0, clzz);
		}

		/**
		 * 设置渲染数据，按顺时针排布
		 */
		public set datas(datas: T[]) {
			this.$initCache(1, datas);
		}

		/**
		 * 设置半径，子视图的锚点到转盘中心的半径
		 */
		public set radiaus(value: number) {
			value > 0 && this.$initCache(2, value);
		}

		/**
		 * 居中
		 */
		private center(item: eui.Component): void {
			item.anchorOffsetX = item.width / 2;
			item.anchorOffsetY = item.height / 2;
		}

		/**
		 * 居中监听
		 */
		private onCenter(e: egret.Event): void {
			this.center(e.currentTarget);
		}

		/**
		 * 初始化转盘
		 */
		private initDial(): void {
			type TCom = { new (data: T): eui.Component };
			var self = this;
			var cache = self.$cache;
			var clazz: TCom = cache[0];
			var datas = cache[1];
			var radiaus = cache[2];
			if (clazz && datas && radiaus > 0) {
				// self.removeChildren();
				// 添加
				let number = datas.length;
				let size = self.width / 2;
				let angle = 360 / number, sAngle = 0;
				let clzz = Utils, cos = clzz.cos, sin = clzz.sin;
				for (let i in datas) {
					let item = new clazz(datas[i]);
					let rAngle = 90 - sAngle;
					item.addEventListener(egret.Event.RESIZE, self.onCenter, self);
					self.center(item);
					self.addChild(item);
					// 设置位置和角度
					item.rotation = sAngle;
					item.x = cos(rAngle) * radiaus + size;
					item.y = size - sin(rAngle) * radiaus + 3;// 3为图片不标准偏差
					sAngle += angle;
				}
				self.$cache[3] = true;
			}
		}

		/**
		 * 设置操作
		 */
		protected setOperation(enable: boolean): void {
			AppViews.stage.touchChildren = enable;
		}

		/**
		 * 动画定时器回调
		 */
		protected onAnimation(): void {
			var self = this;
			var angle = self.$frameAngle;
			self.rotation += angle;
			// 先消耗偏移角度
			if (self.$prizeAngle >= angle) {
				self.$prizeAngle -= angle;
			}
			// 速度周期性减慢
			else if (++self.$frameTotal % self.$framCycle == 0) {
				// 如果没有收到奖品数据，速度保留在一定程度不减少
				if (self.$prizeIndex >= 0 || angle > 16) {
					// 停留
					if (--self.$frameAngle <= 0) {
						self.stopTimer();
					}
				}
			}
		}

		/**
		 * 停止定时器
		 */
		protected stopTimer(): void {
			var self = this;
			self.$timer.stop();
			self.setOperation(true);
			// 触发回调
			var cache = self.$cache;
			var call = cache[4];
			call && call.call(cache[5], self.$prizeIndex);
		}

		/**
		 * 初始化奖品信息
		 */
		protected initPrize(index: number): void {
			var self = this;
			var frameAngle = self.$frameAngle;
			var framCycle = self.$framCycle;
			var angle = 360 - self.getChildAt(index + 1).rotation;
			var oldAngle = ((1 + frameAngle) * framCycle / 2 -
				self.$frameTotal % framCycle) * frameAngle + self.rotation;
			// 重点数据
			self.$prizeIndex = index;
			self.$prizeAngle = Utils.getPstPercent(angle - oldAngle, 360);
		}

		/**
		 * 开始转动，需初始化完毕
		 */
		public start(): void {
			var self = this;
			if (!self.$timer.running && self.$cache[3]) {
				self.setOperation(false);
				self.$prizeIndex = void 0;
				self.$frameTotal = 0;
				self.$frameAngle = 24;
				self.$framCycle = 10;
				self.$timer.start();
			}
		}

		/**
		 * 停止转动
		 * @param index 停留的下标
		 */
		public stop(index: number): void {
			var self = this;
			if (self.$prizeIndex == void 0) {
				self.initPrize(index);
			}
		}

		/**
		 * 添加停止回调
		 */
		public addStopCall(call: (index: number) => void, thisObj?: any): void {
			var cache = this.$cache;
			cache[4] = call;
			cache[5] = thisObj;
		}
	}
}