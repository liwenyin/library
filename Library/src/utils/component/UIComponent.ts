module lie {
	/**
	 * 自带清理方法的控件
	 */
	export class UIComponent extends eui.Component {

		protected isDestroy: boolean;

		public unNest: boolean;		// 不需要层级管理，设为true则不触发其他面板的onShow和onHide

		protected childrenCreated(): void {
			this.onCreate();
		}

		public $onRemoveFromStage(): void {
			var self = this;
			self.isDestroy = true;
			EventUtils.removeEventListeners(self);
			self.onDestroy();
			super.$onRemoveFromStage();
		}

		/**
		 * 控件进入场景时回调
		 */
		protected onCreate(): void {

		}

		/**
		 * 控件离开场景时回调——onRemoveFromStage实际意义上应该是私有函数，如果没有
		 * 写上super.XXX，它没办法有效移除，为避免出错，才有该函数存在
		 */
		protected onDestroy(): void {

		}

		/**
		 * 从父控件移除
		 */
		public removeFromParent(): void {
			var self = this;
			var parent = self.parent;
			parent && parent.removeChild(self);
		}

		/**
		 * 层级变化——被覆盖，AppViews
		 */
		public onHide(): void {

		}

		/**
		 * 层级变化——显示，AppViews
		 */
		public onShow(): void {

		}
	}

	/**
	 * 锚点在中心的图片
	 */
	export class CenImage extends eui.Image {

		public $setTexture(texture: egret.Texture): boolean {
			var width = 0, height = 0;
			if (texture) {
				width = texture.textureWidth;
				height = texture.textureHeight;
			}
			this.anchorOffsetX = width / 2;
			this.anchorOffsetY = height / 2;
			return super.$setTexture(texture);
		}
	}

	/**
	 * App的视图
	 */
	export class AppComponent extends UIComponent {

		public constructor(skinName?: string) {
			super();
			skinName && (this.skinName = AppConfig.getSkin(skinName));
			this.onResize();
		}

		/**
		 * 界面尺寸重置
		 */
		protected onResize(): void {

		}

		//// 广告相关 ////

		/**
		 * 设置广告，该方法伴随AppConfig配置
		 * @param key 广告标志
		 * @param value 配置下标，默认0
		 */
		public set banner(value: number) {
			var self = this;
			var bannerId = AppConfig.getBannerId(value);
			if (bannerId) {
				let banner = pfUtils.showBannerAds(bannerId);
				if (banner) {
					let rew = Utils.rewriteFunc;
					rew(self, 'onHide', function () {
						banner.hide();
					});
					rew(self, 'onShow', function () {
						banner.show();
					});
					rew(self, 'onDestroy', function () {
						banner.hide();
						banner = null;
					});
				}
			}
		}

		/**
		 * 请重写该方法来调整界面
		 */
		protected onBanner(hasBanner: boolean): void {

		}
	}

	/**
	 * 模仿AppComponent的构造，其余模仿UIComponent
	 */
	export class AppRenderer extends eui.ItemRenderer {

		protected isDestroy: boolean;

		public constructor(skinName?: string) {
			super();
			skinName && (this.skinName = AppConfig.getSkin(skinName));
		}

		protected childrenCreated(): void {
			this.onCreate();
		}

		public $onRemoveFromStage(): void {
			var self = this;
			self.isDestroy = true;
			EventUtils.removeEventListeners(self);
			self.onDestroy();
			super.$onRemoveFromStage();
		}

		/**
		 * 控件进入场景时回调
		 */
		protected onCreate(): void {

		}

		/**
		 * 控件离开场景时回调——onRemoveFromStage实际意义上应该是私有函数，如果没有
		 * 写上super.XXX，它没办法有效移除，为避免出错，才有该函数存在
		 */
		protected onDestroy(): void {

		}
	}
}