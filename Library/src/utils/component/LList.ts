module lie {

	/**
	 * 自动定位的列表
	 */
	export class LList extends eui.List {

		private $hAnimat: LAnimation;
		private $vAnimat: LAnimation;

		private $itemH: number;
		private $itemV: number;
		private $moveH: boolean;
		private $moveV: boolean;
		private $curDisH: number;	// 当前滑动距离
		private $curDisV: number;	// 当前滑动距离

		public slideSpeed: number = .4;			// 滑动系数，即滑1像素需要的时间（单位毫秒）
		public slideRange: number = 10;			// 滑动范围，滑动的距离小于该值时会回到原处

		public static E_END: string = 'LEnd';	// 结束监听

		protected createChildren(): void {
			super.createChildren();
			var self = this;
			var layout = self.layout;
			var moveH = false, moveV = false;
			var egretIs = egret.is;
			if (egretIs(layout, 'eui.HorizontalLayout'))
				moveH = true;
			else if (egretIs(layout, 'eui.VerticalLayout'))
				moveV = true;
			else if (egretIs(layout, 'eui.TileLayout')) {
				moveH = true;
				moveV = true;
			}
			self.$moveH = moveH;
			self.$moveV = moveV;
			// 可添加
			if (moveH || moveV) {
				self.scrollEnabled = true;
				self.$curDisH = self.$curDisV = 0;
				// 监听
				EventUtils.addMoveingListener(self, self.onMove, self);
				EventUtils.addTouchFinishListener(self, self.onEnd, self);
				// 动画
				let hAnimat = self.$hAnimat = new LAnimation(function (animation) {
					self.scrollH = animation.currentValue;
				}, null);
				let vAnimat = self.$vAnimat = new LAnimation(function (animation) {
					self.scrollV = animation.currentValue;
				}, null);
				hAnimat.easerFunction = vAnimat.easerFunction = null;
				hAnimat.endFunction = vAnimat.endFunction = function () {
					self.enabled = true;
					self.dispatchEventWith(LList.E_END);
				};
			}
		}

		/**
		 * 重写退出场景
		 */
		public $onRemoveFromStage(): void {
			super.$onRemoveFromStage();
			var self = this;
			var hAnimat = self.$hAnimat;
			var vAnimat = self.$vAnimat;
			if (hAnimat) {
				hAnimat.clear();
				self.$hAnimat = null;
			}
			if (vAnimat) {
				vAnimat.clear();
				self.$vAnimat = null;
			}
		}

		/**
		 * 移动
		 */
		private onMove(disX: number, disY: number): void {
			var self = this;
			if (self.$moveH)
				self.scrollH -= disX;
			if (self.$moveV)
				self.scrollV -= disY;
		}

		/**
		 * 松手
		 */
		private onEnd(): void {
			var self = this;
			var hAnimat = self.$hAnimat;
			var vAnimat = self.$vAnimat;
			// 水平检测
			if (self.$moveH) {
				let width = self.itemH;
				let oldSH = self.scrollH;
				let intSH = self.$curDisH;
				let disSH = oldSH - intSH;
				let newSH = intSH;
				if (Math.abs(disSH) >= self.slideRange)
					newSH += width * (disSH > 0 ? 1 : -1);
				if (newSH < 0)
					newSH = 0;
				else {
					let maxH = self.contentWidth - self.width;
					if (newSH > maxH)
						newSH = maxH;
				}
				self.$curDisH = newSH;
				if (newSH != oldSH) {
					hAnimat.from = oldSH;
					hAnimat.to = newSH;
					hAnimat.duration = Math.abs(newSH - oldSH) / 2/* * self.slideSpeed*/;
					self.enabled = false;
					hAnimat.play();
				}
			}
			// 垂直检测
			if (self.$moveV) {
				let height = self.itemV;
				let oldSV = self.scrollV;
				let intSV = self.$curDisV;
				let disSV = oldSV - intSV;
				let newSV = intSV;
				if (Math.abs(disSV) >= self.slideRange)
					newSV += height * (disSV > 0 ? 1 : -1);
				if (newSV < 0)
					newSV = 0;
				else {
					let maxV = self.contentHeight - self.height;
					if (newSV > maxV)
						newSV = maxV;
				}
				if (newSV != oldSV) {
					vAnimat.from = oldSV;
					vAnimat.to = newSV;
					vAnimat.duration = Math.abs(newSV - oldSV) / 2/* * self.slideSpeed*/;
					self.enabled = false;
					vAnimat.play();
				}
			}
		}

		/**
		 * 获取子项水平间隔
		 */
		public get itemH(): number {
			var self = this;
			var width = self.$itemH;
			if (width == void 0) {
				let children = self.$children;
				if (children && children.length > 0) {
					let layout = <any>self.layout;
					let latGap = 0, egretIs = egret.is;
					if (egretIs(layout, 'eui.HorizontalLayout'))
						latGap = layout.gap;
					else if (egretIs(layout, 'eui.TileLayout'))
						latGap = layout.horizontalGap;
					width = children[0].width + latGap;
				}
				self.$itemH = width;
			}
			return width || 0;
		}

		/**
		 * 获取子项垂直间隔
		 */
		public get itemV(): number {
			var self = this;
			var height = self.$itemV;
			if (height == void 0) {
				let children = self.$children;
				if (children && children.length > 0) {
					let layout = <any>self.layout;
					let latGap = 0, egretIs = egret.is;
					if (egretIs(layout, 'eui.VerticalLayout'))
						latGap = layout.gap;
					else if (egretIs(layout, 'eui.TileLayout'))
						latGap = layout.verticalGap;
					height = children[0].height + latGap;
				}
				self.$itemV = height;
			}
			return height || 0;
		}

		/**
		 * 是否可点击
		 */
		public set enabled(value: boolean) {
			this.touchEnabled = this.touchChildren = value;
		}

		/**
		 * 水平滑动，若已滑动中则无效
		 * @param isLeft 是否向左滑动
		 */
		public slideH(isLeft?: boolean): void {
			var self = this;
			var itemH = self.itemH;
			if (itemH > 0) {
				let hAnimat = self.$hAnimat;
				if (!hAnimat.isPlaying) {
					let curh = self.scrollH;
					let next = self.scrollH + (isLeft ? -1 : 1) * itemH;
					if (next >= 0 && next <= self.contentWidth - self.width) {
						hAnimat.from = curh;
						hAnimat.to = next;
						hAnimat.duration = Math.abs(next - curh) * self.slideSpeed;
						hAnimat.play();
					}
				}
			}
		}

		/**
		 * 垂直滑动，若已滑动中则无效
		 * @param isUp 是否向上滑动
		 */
		public slideV(isUp?: boolean): void {
			var self = this;
			var itemV = self.itemV;
			if (itemV > 0) {
				let vAnimat = self.$vAnimat;
				if (!vAnimat.isPlaying) {
					let curv = self.scrollV;
					let next = self.scrollH + (isUp ? -1 : 1) * itemV;
					if (next >= 0 && next <= self.contentHeight - self.height) {
						vAnimat.from = curv;
						vAnimat.to = next;
						vAnimat.duration = Math.abs(next - curv) * self.slideSpeed;
						vAnimat.play();
					}
				}
			}
		}
	}
}