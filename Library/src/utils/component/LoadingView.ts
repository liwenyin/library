module lie {

	var inInit = false;
	var flag: { FIX_DUPLICATE_LOAD: number } = (<any>RES).FEATURE_FLAG;
	/**
	 * 重写，该代码需要伴随引擎源码，如果不能用，请联系我
	 */
	var init = function () {
		if (!inInit) {
			inInit = true;
			flag.FIX_DUPLICATE_LOAD = 0;	// 允许重复加载，针对合图、位图等
			// 重写，注意不需要该文件请不要加入工程
			let prototype = <any>RES.ResourceLoader.prototype;
			prototype.getECount = function (groupName) {
				var dir = this.loadItemErrorDic[groupName];
				return dir ? dir.length : 0;
			};
			prototype.setGroupProgress = function (groupName, r) {
				var reporter = this.reporterDic[groupName];
				this.numLoadedDic[groupName]++;
				var current = this.numLoadedDic[groupName];
				var total = this.groupTotalDic[groupName];
				if (reporter && reporter.onProgress) {
					// 新增，减去错误资源数量
					reporter.onProgress(current - this.getECount(groupName), total, r);
				}
				return current == total;
			};
		}
	};

	/**
	 * 游戏loading页面，使用该类的原因是egret加载存在错误资源但是进度是正常的，即它的进度表示资源加载（不论成败）的次数，
	 * 若想进度只显示成功资源、失败能重载可以使用该类
	 */
	export class LoadingView extends egret.DisplayObjectContainer {

		private $groupName: string;         // 预加载的组名
		private $urlResource: string;		// 网络资源路径
		private $timeout: number;           // 定时器
		private $errorKeys: string[];       // 加载错误的key
		private $errorNType: NetworkType;   // 错误加载项时的网络状态
		private $isFinish: boolean;         // 是否结束
		private $pTimeout: number;			// 卡进度条

		protected eGroupName: string = '$errorGroup';   // 重加载的错误组名，可重写
		protected eTimeOut: number = 5000;              // 超时检测时间
		protected isLoading: boolean;					// 是否加载中，不能重写

		/**
		 * @param groupName 预加载组名
		 */
		public constructor(groupName: string, url?: string) {
			super();
			var load = this;
			this.$urlResource = url;
			this.$groupName = groupName;
			// 重写，注意不需要该文件请不要加入工程
			init();
		}

		/**
		 * 加载配置——重置了RES的loadConfig原理
		 */
		public loadConfig(resJson: string, resDir?: string): Promise<void> {
			var self = this;
			var url = self.$urlResource;
			var load = function () {
				return RES.loadConfig(resJson, resDir);
			};
			if (url) {
				type ResConfig = { groups: any[], resources: any[] };
				let urlConfig: ResConfig;
				// 自定义1，初始化配置
				let initConfig = function (data: ResConfig) {
					let root = url.substring(0, url.lastIndexOf('/') + 1);
					let resources = data && data.resources;
					for (let i in resources) {
						let res = resources[i];
						res.url = res.url;
						res.root = root;
					}
					urlConfig = data;
				};
				// 重写2
				var splitStr = StrUtils.splitStr;
				RES.processor['LegacyResourceConfigProcessor'].onLoadStart = function (host, resource) {
					return host.load(resource, 'json').then(function (data) {
						var resConfigData = (<any>RES).config.config;
						var root = resource.root;
						var fileSystem = resConfigData.fileSystem;
						if (!fileSystem) {
							fileSystem = {
								fsData: {},
								getFile: function (filename) {
									return fsData[filename];
								},
								addFile: function (data) {
									if (!data.type)
										data.type = "";
									if (root == undefined) {
										data.root = "";
									}
									fsData[data.name] = data;
								},
								profile: function () {
									console.log(fsData);
								},
								removeFile: function (filename) {
									delete fsData[filename];
								}
							};
							resConfigData.fileSystem = fileSystem;
						}
						var groups = resConfigData.groups;
						for (var _i = 0, _a = data.groups; _i < _a.length; _i++) {
							var g = _a[_i];
							groups[g.name] = splitStr(g.keys, ',');
						}
						// 修改1，添加url组名
						var uGroups = urlConfig.groups;
						for (let i in uGroups) {
							let g = uGroups[i];
							let n = g.name;
							let a = splitStr(g.keys, ',');
							// 存在则扩充
							groups[n] = (groups[n] || []).concat(a);
						}
						// 添加结束
						var alias = resConfigData.alias;
						var fsData = fileSystem['fsData'];
						var _loop_1 = function (resource_1) {
							let name = resource_1.name;
							fsData[name] = resource_1;
							// 修改3：根路径保留http
							resource_1.root || (fsData[name].root = root);
							if (resource_1.subkeys) {
								resource_1.subkeys.split(",").forEach(function (subkey) {
									alias[subkey] = name + "#" + subkey;
									alias[name + "." + subkey] = name + "#" + subkey;
								});
								// ResourceConfig.
							}
						};
						// 修改2，添加图片到配置
						var res0 = data.resources;
						var res1 = urlConfig && urlConfig.resources;
						for (let i in res1) {
							res0.push(res1[i]);
						}
						// 添加结束
						for (var _b = 0, len = res0.length; _b < len; _b++) {
							var resource_1 = res0[_b];
							_loop_1(resource_1);
						}
						host.save(resource, data);
						return data;
					});
				};
				// 获取网上资源
				return new Promise<void>(function (resolve) {
					Http.get({
						url: url
					}).then(function (config) {
						initConfig(config);
						load().then(resolve);
					}).catch(function () {
						self.showResUrlError();
					});
				});
			}
			return load();
		}

		/**
		 * 开始加载
		 */
		protected onLoadStart(): void {
			var self = this;
			if (!self.isLoading) {
				let event = RES.ResourceEvent.ITEM_LOAD_ERROR;
				let gName = self.$groupName;
				let remove = function () {
					self.isLoading = false;
					gName == self.eGroupName &&
						delete (<any>RES).config.config.groups[gName]; // 删除组名
					RES.removeEventListener(event, self.onItemError, self);
				};
				self.isLoading = true;
				self.$errorKeys = [];
				RES.loadGroup(gName, 0, <any>self).then(function () {
					remove();
					flag.FIX_DUPLICATE_LOAD = 1;
					self.onSuccess();
				}).catch(function () {
					remove();
					self.onReload();
				});
				RES.addEventListener(event, self.onItemError, self);
			}
		}

		/**
		 * 加载错误的key值
		 */
		protected onItemError(event: RES.ResourceEvent): void {
			this.$errorKeys.push(event.resItem.name);
		}

		/**
		 * 移除监听
		 */
		private $removeListener(): void {
			var self = this;
			egret.clearTimeout(self.$timeout);
			NetworkUtils.removeNetworkStatusChange(self.onStatusChange, self);
		}

		/**
		 * 重新加载判断
		 */
		protected onReload(): void {
			var self = this;
			var utils = NetworkUtils;
			utils.getNetworkType().then(function (type) {
				let eGroupName = self.eGroupName;
				let createGroup = eGroupName && RES.createGroup(eGroupName, self.$errorKeys);
				self.$groupName = eGroupName;   // 修改错误组
				// 创建新组成功
				if (createGroup) {
					// 当前网络状态已是最佳状态
					if (utils.isBestNetworkType(type))
						self.onError(1);
					else {
						self.$errorNType = type;
						// 超时检测
						self.$timeout = egret.setTimeout(self.onError, self, self.eTimeOut, 0);
						// 状态变化
						utils.addNetworkStatusChange(self.onStatusChange, self);
					}
				}
				else {
					self.onError(-1);
				}
			});
		}

		/**
		 * 监听网络状态变化回调
		 */
		protected onStatusChange(type: NetworkType): void {
			var self = this;
			// 网络状态升高，自动重新加载
			if (NetworkUtils.networkUpgrade(self.$errorNType, type)) {
				self.$removeListener();
				self.onLoadStart();
			}
		}

		//// 以下方法供子类修改，用"on"开头的记得加上super，上面的方法看不懂则请勿乱改 ////

		/**
		 * 进度更新
		 */
		protected onProgress(current: number, total: number): void {
			var self = this;
			// 进度条卡XX%情况处理
			if (!self.$pTimeout && current / total >= self.getEndPro()) {
				self.$pTimeout = egret.setTimeout(function () {
					self.$pTimeout = null;
					!self.$isFinish && self.onSuccess();
				}, null, 1000);
			}
		}

		/**
		 * 加载结束
		 */
		protected onSuccess(): void {
			this.$isFinish = true;
			this.$removeListener();
		}

		/**
		 * 加载错误
		 * @param errorCode 错误编码：-1组名错误，请前端同志修改eGroupName，0正常错误，1资源路径错误
		 */
		protected onError(errorCode: number): void {
			this.$removeListener();
		}

		/**
		 * 获取结束的进度，即认为加载到多少时可认为不需要再加载了
		 */
		protected getEndPro(): number {
			return 0.95;
		}

		/**
		 * 服务器资源加载失败时调用
		 */
		protected showResUrlError(): void {
			pfUtils.showToast('加载网络资源失败，请联系管理员');
		}
	}
}