module lie {

	/**
	 * 微信数据开放域控件
	 * 使用：设置好控件大小、坐标及类型即可，其它的无需管理
	 * 注：该控件跟开放域的代码是配套的，不同开放域代码请勿使用该类
	 */
	export class WXBitmap extends eui.Image {

		private $viewName: string;
		private $timeout: number;
		private $timer: lie.Timer;

		public auto: boolean = true;	// 仅在未加入场景前修改有效，兼容ui
		public loop: boolean = false;	// 是否循环刷新界面，默认false，即5秒后会自动停止

		constructor() {
			super();
			this.once(egret.Event.ADDED_TO_STAGE, this.onCreate, this);
		}

		public $onRemoveFromStage(): void {
			super.$onRemoveFromStage();
			this.onDestroy();
		}

		/**
		 * 加入场景
		 */
		protected onCreate(): void {
			var self = this;
			// 纹理
			self.texture = pfUtils.getShareCanvas();
			self.visible = false;
			// 0.1秒刷一次界面
			self.$timer = new Timer(self.reloadCanvas, self, 100, void 0, true);
			self.auto && self.refresh();
		}

		/**
		 * 离开场景
		 */
		protected onDestroy(): void {
			var self = this;
			var post = pfUtils.postMessage;
			self.clear();
			post('exit', { view: self.$viewName });
			// post('pause');
		}

		/**
		 * 设置界面名称
		 */
		public set viewName(value: string) {
			this.$viewName = value;
		}

		/**
		 * 刷新开放域界面
		 * @param param 携带参数通知开放域
		 */
		public refresh(param?: any): void {
			var self = this;
			// 检测
			var texture = self.$texture;
			if (texture) {
				let width = self.width;
				let height = self.height;
				let data = {
					view: self.$viewName,
					width: width,
					height: height
				};
				let timer = self.$timer;
				// 赋值属性
				for (let i in param)
					data[i] = param[i];
				self.visible = true;
				// 通知+延迟刷新界面
				let post = pfUtils.postMessage;
				// post('resume');
				post('enter', data);
				// 5秒后停止绘画
				self.clearTimeout();
				self.reloadCanvas();	// 先刷新一次
				timer.start();
				self.loop || (self.$timeout = egret.setTimeout(timer.stop, timer, 5000));
			}
		}

		/**
		 * 重新渲染cavans界面
		 */
		protected reloadCanvas(): void {
			var bitmapdata = this.texture.$bitmapData;
			egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
			bitmapdata.webGLTexture = null;
		}

		/**
		 * 清除延迟
		 */
		protected clearTimeout(): void {
			var self = this;
			egret.clearTimeout(self.$timeout);
			self.$timeout = null;
		}

		/**
		 * 清除界面
		 */
		protected clear(): void {
			var self = this;
			var texture = self.$texture;
			var timer = self.$timer;
			if (timer) {
				timer.clear();
				self.$timer = null;
			}
			if (texture) {
				texture.dispose();
				self.$texture = null;
			}
			self.clearTimeout();
		}
	}
}