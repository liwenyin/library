module lie {

	/**
	 * 对话框，注意onDestroy加了点东西
	 */
	export class Dialog extends AppComponent {

		protected obscure: egret.DisplayObject;		// 背景朦层

		private m_pCall: Function;
		private m_pThis: any;

		protected childrenCreated(): void {
			this.initObscure();
			this.onCreate();
		}

		public $onRemoveFromStage(): void {
			super.$onRemoveFromStage();
			var self = this;
			var call = self.m_pCall;
			if (call) {
				call(self.m_pThis);
				self.m_pCall = self.m_pThis = null;
			}
		}

		/**
		 * 初始化朦层
		 */
		private initObscure(): void {
			var self = this;
			var stage = self.stage;
			var rect = self.obscure = new eui.Rect;
			rect.width = stage.stageWidth;
			rect.height = stage.stageHeight;
			rect.horizontalCenter = rect.verticalCenter = 0;
			self.addChildAt(rect, 0);
			self.setObscureAlpha(0.5);
		}

		/**
		 * 设置朦层的透明度
		 */
		public setObscureAlpha(alpha: number): void {
			this.obscure.alpha = alpha;
		}

		/**
		 * 添加朦层监听，点击关闭
		 */
		public addOCloseEvent(): void {
			var self = this;
			EventUtils.addTouchTapListener(self.obscure, self.onClose, self);
		}

		/**
		 * 添加关闭回调
		 */
		public addCloseCall(call: Function, thisObj?: any): void {
			var self = this;
			self.m_pCall = call;
			self.m_pThis = thisObj;
		}

		/**
		 * 关闭窗口
		 */
		protected onClose(event: egret.Event): void {
			event.stopImmediatePropagation();
			this.removeFromParent();
		}
	}
}