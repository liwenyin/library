/**
 * 应用配置类，该类下标*的需要重写
 */
class AppConfig {

	// 缓存变量
	private static localMusic = 'music';
	// private static localEffect = 'effect';
	private static localShort = 'shortcut';
	private static $musicOpen: boolean;
	// private static $effectOpen: boolean;
	private static $hasShortcut: boolean;		// 是否从我的小程序启动
	private static $firstShortcut: boolean;		// 是否首次从我的小程序启动
	// 工具类参数
	private static $query: any;					// 入口参数
	private static $caches: any[] = [];			// 回调缓存

	/**
	 * 本地数据初始化
	 */
	public static init(): void {
		var self = AppConfig;
		var geto = LocalData.getObject;
		self.$musicOpen = geto(self.localMusic) !== false;
		// self.$effectOpen = geto(self.localEffect) !== false;
		self.$hasShortcut = !!geto(self.localShort);
	}

	//// 音乐相关 ////

	/**
	 * 是否打开音乐
	 */
	public static isOpenMusic(): boolean {
		return AppConfig.$musicOpen;
	}

	/**
	 * 是否打开了音效
	 */
	public static isOpenEffect(): boolean {
		return AppConfig.$musicOpen/*AppConfig.$effectOpen*/;
	}

	/**
	 * 设置音乐状态
	 */
	public static setMusic(isOpen: boolean): void {
		var self = AppConfig;
		isOpen = !!isOpen;
		if (self.$musicOpen != isOpen) {
			LocalData.setObject(self.localMusic, isOpen);
			self.$musicOpen = isOpen;
			isOpen ? pfUtils.playBgMusic() : pfUtils.pauseBgMusic();
		}
	}

	/**
	 * *根据别名获取音乐文件在项目的相对路径
	 */
	public static getMusicSrc(resName: string): string {
		return 'resource/sound/' + resName + '.mp3';
	}

	// /**
	//  * 设置音效状态
	//  */
	// public static setEffect(isOpen: boolean): void {
	// 	var self = AppConfig;
	// 	isOpen = !!isOpen;
	// 	if (self.$effectOpen != isOpen) {
	// 		LocalData.setObject(self.localEffect, isOpen);
	// 		self.$effectOpen = isOpen;
	// 	}
	// }

	/**
	 * 改变音乐状态
	 */
	public static changeMusic(): void {
		var self = AppConfig;
		self.setMusic(!self.isOpenMusic());
	}

	/**
	 * *获取skins下的皮肤
	 */
	public static getSkin(name: string): string {
		return 'resource/skins/' + name + '.exml';
	}

	//// 工具类使用方法 ////

	/**
	 * 设置分享链接参数
	 * @param query 参数
	 * @param ticket 分享票据
	 */
	public static setQuery(query: any, ticket?: string): void {
		var self = AppConfig;
		if (self.$query != query) {
			self.$query = query;
			// *其他处理
		}
	}

	/**
	 * 通过我的小程序启动
	 */
	public static startWithShortcut(): void {
		var self = AppConfig;
		// 本地缓存
		self.$hasShortcut = true;
		LocalData.setObject(self.localShort, true);
		// 刷新
		self.addInitCall(function () {
			// 首次添加
			if (!self.$firstShortcut) {
				self.$firstShortcut = true;
			}
			// *其他处理
		});
	}

	/**
	 * 根据类型获取广告ID
	 */
	public static getBannerId(type?: number): string {
		return '';
	}

	//// 初始化完毕 ////

	/**
	 * *是否可以初始化
	 * 注：需加上是否已登陆上服务器的判定
	 */
	protected static canInit(): boolean {
		return AppViews.isInit /*&& Server.isLogin()*/;
	}

	/**
	 * 添加界面初始化完毕且玩家已经登陆的操作
	 * @param call 回调函数
	 * @param thisObj 回调所属对象
	 * @param params 回调参数
	 */
	public static addInitCall(call: Function, thisObj?: any, ...params: any[]): void {
		var self = AppConfig;
		if (self.canInit()) {
			call.apply(thisObj, params);
		}
		else {
			self.$caches.push([call, thisObj, params]);
		}
	}

	/**
	 * 执行缓存回调
	 * 注：请分别在主页显示和服务器登录成功里调用
	 */
	public static excuteCalls(): void {
		var self = AppConfig;
		if (self.canInit()) {
			let calls = self.$caches;
			for (let i in calls) {
				let call = calls[i];
				call[0].apply(call[1], call[2]);
			}
			self.$caches = [];
		}
	}
}