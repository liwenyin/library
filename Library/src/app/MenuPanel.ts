/**
 * Test 菜单页，游戏首页
 */
class MenuPanel extends lie.AppComponent {

	public constructor() {
		super('MenuPanelSkin');
	}

	/**
	 * 重写进入舞台
	 */
	protected onCreate(): void {

	}

	/**
	 * 重写离开舞台
	 */
	protected onDestroy(): void {

	}

	/**
	 * 重写页面显示
	 */
	public onShow(): void {

	}

	/**
	 * 重写页面被覆盖
	 */
	public onHide(): void {

	}
}