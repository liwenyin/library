/**
 * 加密工具
 * 注：这里只包含了hmac sha通用，和sha256专用
 * 包含文件：core、hmac、sha256、hmac-sha256
 */
declare module CryptoJS {

    interface ITFinit {
        sigBytes: number;
        words: number[];
    }

    interface ITFHasher {

    }

    function SHA256(message: string, hash: ITFHasher): ITFinit;

    /**
     * HmacSHA256加密
     * @param message 需要加密的字符串
     * @param key 加密秘钥
     */
    function HmacSHA256(message: string, key: string): ITFinit;

}