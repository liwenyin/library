declare namespace dcodeIO {

    export class ProtoBuf {
        
        public static loadProto(text: string);
    }
}