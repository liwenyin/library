var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var lie;
(function (lie) {
    /**
     * 自带清理方法的控件
     */
    var UIComponent = (function (_super) {
        __extends(UIComponent, _super);
        function UIComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        UIComponent.prototype.childrenCreated = function () {
            this.onCreate();
        };
        UIComponent.prototype.$onRemoveFromStage = function () {
            this.isDestroy = true;
            this.onDestroy();
            _super.prototype.$onRemoveFromStage.call(this);
        };
        /**
         * 控件进入场景时回调
         */
        UIComponent.prototype.onCreate = function () {
        };
        /**
         * 控件离开场景时回调——onRemoveFromStage实际意义上应该是私有函数，如果没有
         * 写上super.XXX，它没办法有效移除，为避免出错，才有该函数存在
         */
        UIComponent.prototype.onDestroy = function () {
        };
        /**
         * 从父控件移除
         */
        UIComponent.prototype.removeFromParent = function () {
            var self = this;
            var parent = self.parent;
            parent && parent.removeChild(self);
        };
        return UIComponent;
    }(eui.Component));
    lie.UIComponent = UIComponent;
    __reflect(UIComponent.prototype, "lie.UIComponent");
    /**
     * 锚点在中心的图片
     */
    var CenImage = (function (_super) {
        __extends(CenImage, _super);
        function CenImage() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CenImage.prototype.$setTexture = function (texture) {
            var width = 0, height = 0;
            if (texture) {
                width = texture.textureWidth;
                height = texture.textureHeight;
            }
            this.anchorOffsetX = width / 2;
            this.anchorOffsetY = height / 2;
            return _super.prototype.$setTexture.call(this, texture);
        };
        return CenImage;
    }(eui.Image));
    lie.CenImage = CenImage;
    __reflect(CenImage.prototype, "lie.CenImage");
    /**
     * App的视图
     */
    var AppComponent = (function (_super) {
        __extends(AppComponent, _super);
        function AppComponent(skinName) {
            var _this = _super.call(this) || this;
            _this.skinName = AppConfig.getSkin(skinName);
            return _this;
        }
        return AppComponent;
    }(UIComponent));
    lie.AppComponent = AppComponent;
    __reflect(AppComponent.prototype, "lie.AppComponent");
})(lie || (lie = {}));
var lie;
(function (lie) {
    var single;
    /**
     * 对话框，注意onDestroy加了点东西
     */
    var Dialog = (function (_super) {
        __extends(Dialog, _super);
        function Dialog() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Dialog.prototype.onCreate = function () {
            this.initObscure();
        };
        /**
         * 初始化朦层
         */
        Dialog.prototype.initObscure = function () {
            var self = this;
            var stage = self.stage;
            var rect = self.obscure = new eui.Rect;
            rect.width = stage.stageWidth;
            rect.height = stage.height;
            rect.horizontalCenter = rect.verticalCenter = 0;
            self.addChildAt(rect, 0);
            self.setObscureAlpha(0.5);
        };
        /**
         * 设置朦层的透明度
         */
        Dialog.prototype.setObscureAlpha = function (alpha) {
            this.obscure.alpha = alpha;
        };
        /**
         * 添加朦层监听，点击关闭
         */
        Dialog.prototype.addOCloseEvent = function () {
            var self = this;
            lie.EventUtils.addTouchTapListener(self.obscure, self.onClose, self);
        };
        /**
         * 关闭窗口
         */
        Dialog.prototype.onClose = function (event) {
            event.stopImmediatePropagation();
            this.removeFromParent();
        };
        Dialog.prototype.onDestroy = function () {
            lie.EventUtils.removeEventListener(this.obscure);
        };
        /**
         * 显示单一的对话框，对话框样式固定
         * @param 内容
         * @param 点击确定的回调
         * @param 回调所属对象
         */
        Dialog.showSingleDialog = function (content, call, thisObj) {
            if (!single) {
                single = lie.AppViews.pushDialog(SignleDialog);
                single.skinName = AppConfig.getSkin('SingleDialogSkin');
            }
            single.setText(content);
            single.addCall(call, thisObj);
            return single;
        };
        /**
         * 隐藏单一对话框
         */
        Dialog.hideSingleDialog = function () {
            if (single) {
                single.visible = false;
                single.clearCall();
            }
        };
        /**
         * 移除单一对话框
         */
        Dialog.removeSingDialog = function () {
            if (single) {
                single.removeFromParent();
                single = null;
            }
        };
        return Dialog;
    }(lie.UIComponent));
    lie.Dialog = Dialog;
    __reflect(Dialog.prototype, "lie.Dialog");
    /**
     * 只允许弹出一个的对话框，也可以称为通用对话框
     */
    var SignleDialog = (function (_super) {
        __extends(SignleDialog, _super);
        function SignleDialog() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        SignleDialog.prototype.onCreate = function () {
            _super.prototype.onCreate.call(this);
            var self = this;
            lie.EventUtils.addTouchTapScaleListener(self.m_btnOk, self.removeFromParent, self);
        };
        SignleDialog.prototype.onDestroy = function () {
            _super.prototype.onDestroy.call(this);
            var self = this;
            lie.EventUtils.removeEventListener(self.m_btnOk);
            // 执行回调
            var call = self.$call;
            if (call) {
                call.call(self.$thisObje);
                self.clearCall();
            }
        };
        /**
         * 设置文本
         */
        SignleDialog.prototype.setText = function (text) {
            this.m_lblText.text = text;
        };
        /**
         * 添加监听
         */
        SignleDialog.prototype.addCall = function (call, thisObje) {
            var self = this;
            self.$call = call;
            self.$thisObje = thisObje;
        };
        /**
         * 清除回调
         */
        SignleDialog.prototype.clearCall = function () {
            var self = this;
            self.$call = self.$thisObje = null;
        };
        return SignleDialog;
    }(Dialog));
    __reflect(SignleDialog.prototype, "SignleDialog");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 控件监听工具类
     */
    var EventUtils = (function () {
        function EventUtils() {
        }
        /**
         * 移除控件上的所有监听，该方法也适用于没有通过addEventListener来添加的控件
         */
        EventUtils.removeEventListener = function (target) {
            var value = target.$EventDispatcher;
            var list = [].concat(value[1] || [], value[2] || []);
            var events = [];
            for (var i in list) {
                var item = list[i];
                for (var j in item) {
                    var datas = item[j];
                    for (var k in datas) {
                        var event_1 = datas[k];
                        console.log(event_1);
                        target.removeEventListener(event_1.type, event_1.listener, event_1.thisObject, event_1.useCapture);
                    }
                }
            }
        };
        /**
         * 移除root往下所有的点击事件
         */
        EventUtils.removeEventListeners = function (root) {
            var self = EventUtils;
            if (root instanceof egret.DisplayObjectContainer)
                for (var i = 0, num = root.numChildren; i < num; i++)
                    self.removeEventListeners(root.getChildAt(i));
            else
                self.removeEventListener(root);
        };
        /**
         * 添加缩放监听，记得用removeEventListener来移除这个监听
         */
        EventUtils.addScaleListener = function (target, scale) {
            if (scale === void 0) { scale = 0.95; }
            var addE = function (type, call, thisObj) {
                target.addEventListener(type, call, thisObj);
            };
            var self = EventUtils;
            var clzz = egret.TouchEvent;
            target.$evtScale = scale;
            target.$bgScaleX = target.scaleX;
            target.$bgScaleY = target.scaleY;
            addE(clzz.TOUCH_BEGIN, self.onScleBegin, self);
            addE(clzz.TOUCH_END, self.onScaleEnd, self);
            addE(clzz.TOUCH_CANCEL, self.onScaleEnd, self);
            addE(clzz.TOUCH_RELEASE_OUTSIDE, self.onScaleEnd, self);
        };
        /**
         * 缩放开始
         */
        EventUtils.onScleBegin = function (event) {
            var target = event.currentTarget;
            var tween = egret.Tween;
            var scale = target.$evtScale;
            var scaleX = target.scaleX * scale;
            var scaleY = target.scaleY * scale;
            tween.removeTweens(target);
            tween.get(target).to({ scaleX: scaleX, scaleY: scaleY }, EventUtils.$scaleTime);
        };
        /**
         * 缩放结束
         */
        EventUtils.onScaleEnd = function (event) {
            var target = event.currentTarget;
            var time = EventUtils.$scaleTime;
            var tween = egret.Tween;
            var scaleX = target.$bgScaleX;
            var scaleY = target.$bgScaleY;
            var bScaleX = scaleX * 1.1;
            var bScaleY = scaleY * 1.1;
            tween.removeTweens(target);
            tween.get(target).to({ scaleX: bScaleX, scaleY: bScaleY }, time).to({ scaleX: scaleX, scaleY: scaleY }, time);
        };
        // 常用的监听类型归类
        /**
         * 添加TouchTap监听
         */
        EventUtils.addTouchTapListener = function (target, call, thisObj, useCapture) {
            target.addEventListener(egret.TouchEvent.TOUCH_TAP, call, thisObj, useCapture);
        };
        /**
         * 在TouchTap的基础上进行缩放
         */
        EventUtils.addTouchTapScaleListener = function (target, call, thisObj, scale, useCapture) {
            var self = EventUtils;
            self.addScaleListener(target, scale);
            self.addTouchTapListener(target, call, thisObj, useCapture);
        };
        /**
         * 添加按住监听
         * @param target
         * @param begin 按住时的回调
         * @param end 松手时的回调，会调用多次，请自己在end里判断
         */
        EventUtils.addTouchingListener = function (target, begin, end, thisObj) {
            var event = egret.TouchEvent;
            target.addEventListener(event.TOUCH_BEGIN, begin, thisObj);
            target.addEventListener(event.TOUCH_END, end, thisObj);
            target.addEventListener(event.TOUCH_CANCEL, end, thisObj);
            target.addEventListener(event.TOUCH_RELEASE_OUTSIDE, end, thisObj);
        };
        /**
         * 添加移动监听
         */
        EventUtils.addTouchMoveListener = function (target) {
            var event = egret.TouchEvent;
            var touchX, touchY;
            target.addEventListener(event.TOUCH_BEGIN, function (e) {
                e.stopImmediatePropagation();
                touchX = e.stageX;
                touchY = e.stageY;
            }, null);
            target.addEventListener(event.TOUCH_MOVE, function (e) {
                e.stopImmediatePropagation();
                var newX = e.stageX, newY = e.stageY;
                target.x += newX - touchX;
                target.y += newY - touchY;
                touchX = newX;
                touchY = newY;
            }, null);
        };
        /**
         * 添加按住时每隔一段时间就触发回调的监听
         * @returns 返回监听函数，清理使用
         */
        EventUtils.addTouchStepListener = function (target, delay, step, thisObj) {
            var time;
            var begin = function (e) {
                step.call(thisObj, e);
                time = egret.setTimeout(begin, null, delay);
            };
            var end = function (e) {
                if (time) {
                    egret.clearTimeout(time);
                    time = null;
                }
            };
            EventUtils.addTouchingListener(target, begin, end, thisObj);
            return [begin, end];
        };
        EventUtils.$scaleTime = 100; // 缩放动画时间
        return EventUtils;
    }());
    lie.EventUtils = EventUtils;
    __reflect(EventUtils.prototype, "lie.EventUtils");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 游戏生命周期管理类
     * 注：调试模式不要开启
     */
    var LifeCycle = (function () {
        function LifeCycle() {
        }
        /**
         * 游戏生命周期初始化
         */
        LifeCycle.init = function () {
            var self = LifeCycle;
            var lifecycle = egret.lifecycle;
            lifecycle.addLifecycleListener(function (context) {
                context.onUpdate = self.update;
            });
            lifecycle.onPause = self.pause;
            lifecycle.onResume = self.resume;
        };
        /**
         * 全局游戏更新函数
         */
        LifeCycle.update = function () {
        };
        /**
         * 全局游戏暂停函数
         */
        LifeCycle.pause = function () {
            egret.ticker.pause();
        };
        /**
         * 全局游戏恢复函数
         */
        LifeCycle.resume = function () {
            egret.ticker.resume();
        };
        return LifeCycle;
    }());
    lie.LifeCycle = LifeCycle;
    __reflect(LifeCycle.prototype, "lie.LifeCycle");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 存放和获取一些本地变量名，属于应用级别上的存储
     */
    var LocalData = (function () {
        function LocalData() {
        }
        /**
         * 设置Item
         */
        LocalData.setItem = function (key, value) {
            return egret.localStorage.setItem(key, value);
        };
        /**
         * 获取Item的值
         */
        LocalData.getItem = function (key) {
            return egret.localStorage.getItem(key);
        };
        /**
         * 注意value必须是对象，否则会出现奇怪的现象
         */
        LocalData.setObject = function (key, value) {
            return LocalData.setItem(key, JSON.stringify(value));
        };
        /**
         * 获取一个对象
         */
        LocalData.prototype.getObject = function (key) {
            return JSON.parse(LocalData.getItem(key));
        };
        return LocalData;
    }());
    lie.LocalData = LocalData;
    __reflect(LocalData.prototype, "lie.LocalData");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 类型检验工具类
     */
    var TypeUtils = (function () {
        function TypeUtils() {
        }
        /**
         * 检测是否是字符串类型，注意new String检测不通过的
         */
        TypeUtils.isString = function (obj) {
            return typeof obj === 'string' && obj.constructor === String;
        };
        /**
         * 检测是不是数组
         */
        TypeUtils.isArray = function (obj) {
            return Object.prototype.toString.call(obj) === '[object Array]';
        };
        /**
         * 检测是不是数字，注意new Number不算进来
         */
        TypeUtils.isNumber = function (obj) {
            return typeof obj === 'number' && !isNaN(obj); // type NaN === 'number' 所以要去掉
        };
        return TypeUtils;
    }());
    lie.TypeUtils = TypeUtils;
    __reflect(TypeUtils.prototype, "lie.TypeUtils");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 工具类，存放一些常用的且不好归类的方法
     */
    var Utils = (function () {
        function Utils() {
        }
        // 工具私有变量，请勿乱动
        /**
         * 初始化一个长度为length，值全为value的数组
         * 注意，由于数组公用一个value，因此，value适用于基础类型，对于对象类的，请用memset2方法
         */
        Utils.memset = function (length, value) {
            return Array.apply(null, Array(length)).map(function () { return value; });
        };
        /**
         * 初始化一个长度为length，值通过getValue函数获取，注意getValue第一个参数是没用，第二个参数是当前数组的下标，即你返回的数据将存放在数组的该下标
         */
        Utils.memset2 = function (length, getValue) {
            return Array.apply(null, Array(length)).map(getValue);
        };
        /**
         * 创建一个定时器
         * @param listener 定时回调
         * @param thisObject 回调所属对象
         * @param second 回调间隔，单位秒，默认一秒
         * @param repeat 循环次数，默认一直重复
         */
        Utils.createTimer = function (listener, thisObject, second, repeat) {
            if (second === void 0) { second = 1; }
            var timer = new egret.Timer(second * 1000, repeat);
            timer.addEventListener(egret.TimerEvent.TIMER, listener, thisObject);
            timer.start();
            return timer;
        };
        /**
         * 移除一个定时器
         * @param timer 被移除的定时器
         * @param listener 监听函数
         * @param thisObject 函数所属对象
         */
        Utils.removeTimer = function (timer, listener, thisObject) {
            timer.stop();
            timer.removeEventListener(egret.TimerEvent.TIMER, listener, thisObject);
        };
        /**
         * 获取网页参数
         */
        Utils.getQueryString = function (name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var ret = window.location.search.substr(1).match(reg);
            return ret ? decodeURIComponent(ret[2]) : '';
        };
        /**
         * 获取cr1位于cr0的方向
         * 注：0为上，顺时针开始数，共八个方向
         */
        Utils.getDistance = function (col0, row0, col1, row1) {
            var row, col, c = 2, xs = 1;
            if (row0 < row1)
                row = -1;
            else if (row0 > row1)
                row = 1;
            else
                row = 0;
            if (col0 > col1) {
                c = 6;
                xs = -1;
            }
            else if (col0 == col1)
                xs = 2;
            return c - row * xs;
        };
        /**
         * 关闭web版游戏
         */
        Utils.closeWebGame = function () {
            var history = window.history;
            lie.Dispatch.clear();
            lie.CSocket.getInstance().close();
            AppConfig.onClose();
            if (history.length > 1)
                history.back();
            else
                window.close();
        };
        /**
         * 自定义格式化字符串
         * @param reg 正则
         * @param str 字符串
         * @param args 填充参数
         */
        Utils.formatStringReg = function (reg, str, args) {
            for (var i in args) {
                var arg = args[i];
                if (reg.test(str))
                    str = str.replace(reg, args[i]);
                else
                    break;
            }
            return str;
        };
        /**
         * 格式化字符串
         */
        Utils.formatString = function (str) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            str = str.replace(/%%/g, '%'); // 有些识别问题出现两个%号
            return Utils.formatStringReg(/%d|%s/i, str, args); // 忽略大小写
        };
        /**
         * 偏移整型数组
         * @param array 需要偏移的数组
         * @param offset 偏移量，对应array长度，没有值时默认0
         */
        Utils.offsetArray = function (array) {
            var offset = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                offset[_i - 1] = arguments[_i];
            }
            for (var i in array)
                array[i] += (offset[i] || 0);
            return array;
        };
        return Utils;
    }());
    lie.Utils = Utils;
    __reflect(Utils.prototype, "lie.Utils");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 事件分发工具类，异步操作用
     */
    var Dispatch = (function () {
        function Dispatch() {
        }
        /**
         * 注册对象
         * @param key 事件的唯一标志
         * @param obj 注册事件
         * @param replace 是否替换掉之前的回调
         */
        Dispatch.$register = function (key, obj, replace) {
            var self = Dispatch;
            var target = self.$targets[key];
            var isReg = !target || replace;
            isReg && (self.$targets[key] = obj);
            return isReg;
        };
        /**
         * 注册事件
         * @param key 事件的唯一标志
         * @param call 回调函数
         * @param thisObj 回调所属对象
         * @param replace 是否替换掉之前的回调
         */
        Dispatch.register = function (key, call, thisObj, replace) {
            return Dispatch.$register(key, [call, thisObj], replace);
        };
        /**
         * 通知，触发回调
         * @param key 事件标志
         * @param param 回调参数
         */
        Dispatch.notice = function (key) {
            var params = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                params[_i - 1] = arguments[_i];
            }
            var target = Dispatch.$targets[key];
            target && target[0].apply(target[1], params);
        };
        /**
         * 移除事件
         */
        Dispatch.remove = function (key) {
            delete Dispatch.$targets[key];
        };
        /**
         * 是否注册了某个事件
         */
        Dispatch.hasRegister = function (key) {
            return !!Dispatch.$targets[key];
        };
        /**
         * 注册多个事件，统一回调，参数功能看register
         */
        Dispatch.registers = function (keys, call, thisObj, replace) {
            var obj = [call, thisObj];
            for (var i in keys) {
                var key = keys[i];
                Dispatch.$register(key, obj, replace);
            }
        };
        /**
         * 清除所有事件
         */
        Dispatch.clear = function () {
            Dispatch.$targets = {};
        };
        Dispatch.$targets = {}; // 存放所有回调函数
        return Dispatch;
    }());
    lie.Dispatch = Dispatch;
    __reflect(Dispatch.prototype, "lie.Dispatch");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 影片剪辑，实现可于皮肤创建
     */
    var MovieClip = (function (_super) {
        __extends(MovieClip, _super);
        function MovieClip() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        MovieClip.prototype.childrenCreated = function () {
            var self = this;
            var getr = RES.getResAsync;
            // 这里就不检测资源的正确性了，调用的时候注意即可
            getr(self.image, function (texture) {
                if (!self.isDestroy) {
                    getr(self.frame, function () {
                        if (!self.isDestroy)
                            self.initMovieClip();
                    }, null);
                }
            }, null);
        };
        MovieClip.prototype.onDestroy = function () {
            this.clearMovieCall();
        };
        /**
         * 初始化数据
         */
        MovieClip.prototype.initMovieClip = function () {
            var self = this;
            var getr = RES.getRes;
            self.$factory = new egret.MovieClipDataFactory(getr(self.frame), getr(self.image));
            self.addChild(self.$movieClip = new egret.MovieClip);
            self.carryMovieCall();
        };
        /**
         * 执行回调
         */
        MovieClip.prototype.carryMovieCall = function () {
            var self = this;
            var call = self.m_pClipCall;
            if (call) {
                var clip = self.$movieClip;
                // 初始化数据
                clip.movieClipData = self.$factory.generateMovieClipData(self.action);
                call.call(self.m_pThisObj, clip);
                self.clearMovieCall(); // 执行完销毁
            }
        };
        /**
         * 清空回调
         */
        MovieClip.prototype.clearMovieCall = function () {
            var self = this;
            self.m_pClipCall = self.m_pThisObj = null;
        };
        Object.defineProperty(MovieClip.prototype, "res", {
            /**
             * 若命名一致，可取其前缀
             */
            set: function (value) {
                this.image = value + '_png';
                this.frame = value + '_json';
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MovieClip.prototype, "image", {
            /**
             * 获取帧动画合图资源名
             */
            get: function () {
                return this.$image;
            },
            /**
             * 设置帧动画合图资源名
             */
            set: function (value) {
                this.$image = String(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MovieClip.prototype, "frame", {
            /**
             * 获取帧动画数据资源名
             */
            get: function () {
                return this.$frame;
            },
            /**
             * 设置帧动画数据资源名
             */
            set: function (value) {
                this.$frame = String(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MovieClip.prototype, "action", {
            /**
             * 获取当前动画名称
             */
            get: function () {
                return this.$action;
            },
            /**
             * 设置当前动画名称
             */
            set: function (value) {
                this.$action = String(value);
            },
            enumerable: true,
            configurable: true
        });
        // 供外部使用的方法
        /**
         * 只能通过该方法来获取MovieClip对象
         */
        MovieClip.prototype.addMovieCall = function (call, thisObj) {
            var self = this;
            self.m_pClipCall = call;
            self.m_pThisObj = thisObj;
            self.$factory && self.carryMovieCall(); // 存在则立马执行
        };
        /**
         * 播放
         */
        MovieClip.prototype.play = function (playTimes) {
            this.addMovieCall(function (clip) {
                clip.play(playTimes);
            }, null);
        };
        return MovieClip;
    }(lie.UIComponent));
    lie.MovieClip = MovieClip;
    __reflect(MovieClip.prototype, "lie.MovieClip");
})(lie || (lie = {}));
var lie;
(function (lie) {
    var app; // 存放单例
    /**
     * 应用的视图管理类
     */
    var AppViews = (function () {
        function AppViews() {
        }
        /**
         * 初始化
         */
        AppViews.prototype.init = function () {
            var self = this;
            self.stage.removeChildren();
            self.$panelLevel = self.addLevel('panel');
            self.$dialogLevel = self.addLevel('dialog');
        };
        Object.defineProperty(AppViews.prototype, "stage", {
            /**
             * 获取舞台
             */
            get: function () {
                return egret.sys.$TempStage; // egret.MainContext.instance.stage
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AppViews.prototype, "panelLevel", {
            /**
             * 获取面板层
             */
            get: function () {
                return this.$panelLevel;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AppViews.prototype, "dialogLevel", {
            /**
             * 获取对话框层
             */
            get: function () {
                return this.$dialogLevel;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 添加层
         * @param name 名称
         */
        AppViews.prototype.addLevel = function (name) {
            var container = new egret.DisplayObjectContainer;
            container.name = name;
            this.stage.addChild(container);
            return container;
        };
        /**
         * 获取层
         */
        AppViews.prototype.getLevel = function (name) {
            return this.stage.getChildByName(name);
        };
        /**
         * 获取视图管理类单例
         */
        AppViews.app = function () {
            if (!app) {
                app = new AppViews;
                app.init();
            }
            return app;
        };
        // 重点，层及控制管理体现
        /**
         * 插入一个控件，并居中
         * @param attr AppViews对象的属性名
         * @param clzz 需要添加的对象类
         */
        AppViews.pushChild = function (attr, clzz) {
            var self = AppViews;
            var child = new clzz;
            self.app()[attr].addChild(child);
            self.setInCenter(child);
            return child;
        };
        /**
         * 移除层里的控件
         * @param attr AppViews对象的属性名
         * @param clzz 需要移除的对象类
         */
        AppViews.removeChild = function (attr, clzz) {
            var panel = AppViews.app()[attr];
            for (var i = 0, num = panel.numChildren; i < num; i++) {
                var child = panel.getChildAt(i);
                if (child instanceof clzz)
                    panel.removeChildAt(i);
            }
        };
        /**
         * 将子控件设置在父控件中心点
         */
        AppViews.setInCenter = function (child) {
            var stage = AppViews.app().stage;
            child.x = (stage.stageWidth - child.width) / 2;
            child.y = (stage.stageHeight - child.height) / 2;
        };
        /**
         * 新建一个面板并放入
         */
        AppViews.pushPanel = function (clzz) {
            return AppViews.pushChild('panelLevel', clzz);
        };
        /**
         * 新建一个对话框并放入
         */
        AppViews.pushDialog = function (clzz) {
            return AppViews.pushChild('dialogLevel', clzz);
        };
        /**
         * 移除面板
         */
        AppViews.removePanel = function (clzz) {
            AppViews.removeChild('panelLevel', clzz);
        };
        /**
         * 移除对话框
         */
        AppViews.removeDialog = function (clzz) {
            AppViews.removeChild('dialogLevel', clzz);
        };
        return AppViews;
    }());
    lie.AppViews = AppViews;
    __reflect(AppViews.prototype, "lie.AppViews");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 回调对象
     */
    var CSocketCallback = (function () {
        function CSocketCallback(callback, target, param) {
            this.m_pTarget = target;
            this.m_pCallback = callback;
            this.m_pParam = param;
            this.m_iTime = lie.Server.getTime();
        }
        /**
         * 是否过期
         */
        CSocketCallback.prototype.isExpire = function () {
            return lie.Server.getTime() - this.m_iTime > 10;
        };
        /**
         * 执行回调
         */
        CSocketCallback.prototype.call = function (object, msgId) {
            var self = this;
            self.m_pCallback.call(self.m_pTarget, object, msgId, self.m_pParam);
        };
        /**
         * 清除
         */
        CSocketCallback.prototype.clear = function () {
            var self = this;
            self.m_pTarget = null;
            self.m_pCallback = null;
            self.m_pParam = null;
        };
        return CSocketCallback;
    }());
    __reflect(CSocketCallback.prototype, "CSocketCallback");
    /**
     * Socket通讯
     */
    var CSocket = (function () {
        function CSocket() {
            this.m_pRequestCalls = {}; // 存放通信回调函数
            var socket = this.m_pSocket = new egret.WebSocket();
            socket.type = egret.WebSocket.TYPE_STRING;
            socket.addEventListener(egret.ProgressEvent.SOCKET_DATA, this.onReceive, this);
            socket.addEventListener(egret.Event.CONNECT, this.onConnected, this);
            socket.addEventListener(egret.Event.CLOSE, this.onClose, this);
            socket.addEventListener(egret.IOErrorEvent.IO_ERROR, this.onError, this);
        }
        /**
         * 接收到消息
         */
        CSocket.prototype.onReceive = function () {
            var self = this;
            var msg = self.m_pSocket.readUTF();
            var obj = JSON.parse(msg);
            var code = obj.errorCode;
            var msgId = obj.msgId;
            console.log('onReceive', obj);
            if (code)
                CSocket.error(code, msgId);
            else {
                var info = obj.info;
                var proArr = self.m_pRequestCalls[msgId];
                if (proArr && proArr.length > 0) {
                    var socketCall = proArr.pop();
                    socketCall.call(info, msgId);
                    socketCall.clear();
                }
                else
                    CSocket.nofify(msgId, info);
            }
        };
        /**
         * 连接开始
         */
        CSocket.prototype.onConnected = function () {
            var self = this;
            self.m_pIsConnected = true;
            self.startHeartbeat();
            lie.Server.startHeartBeat();
            AppConfig.onConnetScoket();
        };
        /**
         * 连接报错
         */
        CSocket.prototype.onError = function () {
            AppConfig.onSocketError();
        };
        /**
         * 关闭连接
         * @param isReconned 是否重连
         */
        CSocket.prototype.close = function (isReconned) {
            this.onClose(null, isReconned);
        };
        /**
         * 连接中断
         */
        CSocket.prototype.onClose = function (e, isReconned) {
            var self = this;
            if (self.m_pIsConnected) {
                self.m_pIsConnected = false;
                lie.Server.stopHeartBeat();
                self.stopHeartbeat();
                isReconned ? self.reconnect() : self.onClear();
                AppConfig.onSocketBreak();
            }
        };
        /**
         * 清理Socket
         */
        CSocket.prototype.onClear = function () {
            var self = this;
            var socket = self.m_pSocket;
            if (socket) {
                lie.EventUtils.removeEventListener(socket);
                self.m_pSocket = null;
                CSocket.$instance = null;
            }
        };
        // 其他操作
        /**
         * 检查时间，清理没必要的事件
         */
        CSocket.prototype.onCheckTimer = function () {
            var calls = this.m_pRequestCalls;
            for (var i in calls) {
                var proArr = calls[i];
                for (var j = 0, len = proArr.length; j < len; j++) {
                    var call = proArr[j];
                    if (proArr[j].isExpire()) {
                        proArr.splice(j, 1);
                        j--;
                        len--;
                    }
                }
            }
        };
        /**
         * 启动心跳
         */
        CSocket.prototype.startHeartbeat = function () {
            var self = this;
            var timer = self.m_pCheckTimer;
            if (timer)
                timer.start();
            else
                timer = self.m_pCheckTimer = lie.Utils.createTimer(self.onCheckTimer, self, 5);
        };
        /**
         * 心跳停止
         */
        CSocket.prototype.stopHeartbeat = function () {
            var self = this;
            self.m_pCheckTimer.stop();
            self.m_pRequestCalls = {};
        };
        /**
         * 清除定时器
         */
        CSocket.prototype.clearTimer = function () {
            var self = this;
            egret.clearTimeout(self.m_pTimer);
            self.m_pTimer = null;
        };
        // 主要供外部使用方法
        /**
         * 连接
         */
        CSocket.prototype.connect = function () {
            var clzz = lie.Server;
            // this.m_pSocket.connect(clzz.HOST, clzz.POST);
            this.m_pSocket.connectByUrl('ws://' + clzz.HOST + ':' + clzz.POST + '?sid=' + clzz.sid);
        };
        /**
         * 重新连接，500后发起重连
         */
        CSocket.prototype.reconnect = function () {
            var self = this;
            self.clearTimer();
            self.m_pTimer = egret.setTimeout(self.connect, self, 500);
        };
        /**
         * 发送消息
         */
        CSocket.prototype.sendAction = function (msg) {
            console.log('sendAction', JSON.stringify(msg));
            lie.MsgUtils.hasMsg(msg.msgId) && this.m_pSocket.writeUTF(JSON.stringify(msg));
        };
        /**
         * 发送没有内容的消息且具有返回回调的消息
         */
        CSocket.prototype.sendWithMsgId = function (msgId, call, thisObj, selfP) {
            this.sendWitfCallback(lie.MsgUtils.newMsg(msgId), call, thisObj, selfP);
        };
        /**
         * 发送具有返回回调的消息
         */
        CSocket.prototype.sendWitfCallback = function (msg, call, thisObj, selfP) {
            var self = this;
            var calls = self.m_pRequestCalls;
            var msgId = msg.msgId;
            if (call) {
                var sock = new CSocketCallback(call, thisObj, selfP);
                var array = calls[msgId] || (calls[msgId] = []);
                array.push(sock);
            }
            self.sendAction(msg);
        };
        /**
         * 获取单利
         */
        CSocket.getInstance = function () {
            var self = CSocket;
            var inc = self.$instance;
            if (!inc)
                inc = self.$instance = new CSocket;
            return inc;
        };
        /**
         * 发送没有内容的消息且具有返回回调的消息
         */
        CSocket.sendWithMsgId = function (msgId, call, thisObj, selfP) {
            CSocket.sendWitfCallback(lie.MsgUtils.newMsg(msgId), call, thisObj, selfP);
        };
        /**
         * 发送具有返回回调的消息
         */
        CSocket.sendWitfCallback = function (msg, call, thisObj, selfP) {
            CSocket.getInstance().sendWitfCallback(msg, call, thisObj, selfP);
        };
        /**
         * 服务端主动推送的消息
         */
        CSocket.nofify = function (msgId, info) {
            var aClzz = lie.GeneralMsg;
            var clzz = AppConfig;
            switch (msgId) {
                case aClzz.PLAYING:
                    clzz.onPlayGame(info);
                    break;
                case aClzz.FINISH:
                    clzz.onFinishGame(info);
                    break;
            }
        };
        /**
         * 错误编码处理
         */
        CSocket.error = function (errorCode, msgId) {
            console.log('error code', errorCode, msgId);
        };
        // 静态相关
        CSocket.TIMER = 30;
        return CSocket;
    }());
    lie.CSocket = CSocket;
    __reflect(CSocket.prototype, "lie.CSocket");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 常用通讯消息
     */
    var GeneralMsg;
    (function (GeneralMsg) {
        GeneralMsg[GeneralMsg["TIME"] = 0] = "TIME";
        GeneralMsg[GeneralMsg["ENTER"] = 1] = "ENTER";
        GeneralMsg[GeneralMsg["MATCH"] = 2] = "MATCH";
        GeneralMsg[GeneralMsg["PLAYING"] = 3] = "PLAYING";
        GeneralMsg[GeneralMsg["FINISH"] = 4] = "FINISH";
    })(GeneralMsg = lie.GeneralMsg || (lie.GeneralMsg = {}));
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 消息工具类
     */
    var MsgUtils = (function () {
        function MsgUtils() {
        }
        /**
         * 新建一个消息
         */
        MsgUtils.newMsg = function (msgId, newInfo) {
            var msg = Object.create(null);
            msg.msgId = msgId;
            newInfo && (msg.info = Object.create(null));
            return msg;
        };
        /**
         * 获取消息ID
         * @param tag 前后端标志，目前是11和12
         */
        MsgUtils.getMsgId = function (tag, msgId) {
            var base = 10000;
            return tag * base + msgId % base;
        };
        /**
         * 转化为请求的消息ID
         */
        MsgUtils.getResMsgId = function (msgId) {
            return MsgUtils.getMsgId(11, msgId);
        };
        /**
         * 转化为推送的消息ID
         */
        MsgUtils.getReqMsgId = function (msgId) {
            return MsgUtils.getMsgId(12, msgId);
        };
        /**
         * 初始化消息，注册
         */
        MsgUtils.init = function (clzz) {
            var keys = Object.keys(clzz);
            for (var i in keys) {
                var key = keys[i];
                var val = clzz[key];
                if (!isNaN(val))
                    MsgUtils.m_pMsgMap[val] = 1;
            }
        };
        /**
         * 是否注册过某个消息
         */
        MsgUtils.hasMsg = function (msgId) {
            return !!MsgUtils.m_pMsgMap[msgId];
        };
        /**
         * 清除数据
         */
        MsgUtils.clear = function () {
            MsgUtils.m_pMsgMap = {};
        };
        MsgUtils.m_pMsgMap = {};
        return MsgUtils;
    }());
    lie.MsgUtils = MsgUtils;
    __reflect(MsgUtils.prototype, "lie.MsgUtils");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 服务器属性类
     */
    var Server = (function () {
        function Server() {
        }
        /**
         * 获取当前服务器时间，单位毫秒
         */
        Server.getTime = function () {
            var self = Server;
            return self.m_pServerTime + (Date.now() - self.m_pLocalTime);
        };
        /**
         * 启动心跳
         */
        Server.startHeartBeat = function () {
            var self = Server;
            var time = self.m_pTimer;
            if (!time) {
                var action = self.m_pTAction = lie.MsgUtils.newMsg(lie.GeneralMsg.TIME);
                time = self.m_pTimer = lie.Utils.createTimer(self.onHeartBeat, self, 30);
            }
            time.start();
            self.onHeartBeat();
        };
        /**
         * 停止心跳
         */
        Server.stopHeartBeat = function () {
            var time = Server.m_pTimer;
            time && time.stop();
        };
        /**
         * 心跳回调
         */
        Server.onHeartBeat = function () {
            var self = Server;
            // CSocket.sendWithMsgId(GeneralAction.TIME, self.updateTime, self);
            lie.CSocket.sendWitfCallback(self.m_pTAction, self.updateTime, self);
        };
        /**
         * 更新时间
         */
        Server.updateTime = function (info) {
            var self = Server;
            self.m_pServerTime = info.time * 1000;
            self.m_pLocalTime = Date.now();
        };
        /**
         * 初始化进入游戏
         */
        Server.startGame = function () {
            var self = Server;
            var query = lie.Utils.getQueryString;
            var sid = self.sid = query('sid');
            var roomId = self.roomId = Number(query('room_id'));
            if (!sid || !roomId)
                AppConfig.onEnterError();
            else
                lie.CSocket.getInstance().connect();
        };
        /**
         * 服务器地址
         */
        Server.HOST = 'localhost';
        /**
         * 端口
         */
        Server.POST = 8017;
        return Server;
    }());
    lie.Server = Server;
    __reflect(Server.prototype, "lie.Server");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 前后端交互方法
     */
    var Service = (function () {
        function Service() {
        }
        /**
         * 请求游戏内，默认带上sid和room_id
         * @param msgId 消息ID
         * @param respCall 响应回调
         * @param thisObj 回调对象
         * @param setData 设置请求参数函数
         */
        Service.reqGameAction = function (msgId, respCall, thisObj, setData) {
            var self = Service;
            var clzz = lie.Server;
            var msg = lie.MsgUtils.newMsg(msgId, true);
            var info = msg.info;
            info.sid = clzz.sid;
            info.roomId = clzz.roomId;
            setData && setData(info);
            lie.CSocket.sendWitfCallback(msg, respCall, thisObj);
        };
        /**
         * 请求进入游戏
         */
        Service.reqEnterGame = function () {
            var self = Service;
            self.reqGameAction(lie.GeneralMsg.ENTER, self.repEnterGame, self);
        };
        /**
         * 响应进入游戏
         */
        Service.repEnterGame = function (resp) {
            AppConfig.onStartGame(resp);
        };
        /**
         * 游戏过程数据更新
         */
        Service.reqPlayGame = function (info) {
            var self = Service;
            self.reqGameAction(lie.GeneralMsg.PLAYING, self.repPlayGame, self, function (data) {
                data.gameInfo = info;
            });
        };
        /**
         * 响应游戏过程数据更新
         */
        Service.repPlayGame = function (resp) {
            AppConfig.onPlayGame(resp);
        };
        /**
         * 游戏结束
         * @param info 游戏数据
         */
        Service.reqFinishGame = function (info) {
            var self = Service;
            self.reqGameAction(lie.GeneralMsg.FINISH, self.repFinishGame, self, function (data) {
                data.gameInfo = info;
            });
        };
        /**
         * 响应游戏过程数据更新
         */
        Service.repFinishGame = function (resp) {
            alert('游戏结束：' + JSON.stringify(resp));
            egret.MainContext.instance.stage.removeChildren();
        };
        return Service;
    }());
    lie.Service = Service;
    __reflect(Service.prototype, "lie.Service");
})(lie || (lie = {}));
