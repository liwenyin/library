declare module lie {

    var single: SignleDialog;

	/**
	 * 对话框，注意onDestroy加了点东西
	 */
    export class Dialog extends UIComponent {

        private obscure: eui.Rect;		// 背景朦层

		/**
		 * 初始化朦层
		 */
        private initObscure(): void;

		/**
		 * 设置朦层的透明度
		 */
        public setObscureAlpha(alpha: number): void;

		/**
		 * 添加朦层监听，点击关闭
		 */
        public addOCloseEvent(): void;

		/**
		 * 关闭窗口
		 */
        protected onClose(event: egret.Event): void;

		/**
		 * 显示单一的对话框，对话框样式固定
		 * @param 内容
		 * @param 点击确定的回调
		 * @param 回调所属对象
		 */
        public static showSingleDialog(content: string, call: Function, thisObj?: any): SignleDialog;

		/**
		 * 隐藏单一对话框
		 */
        public static hideSingleDialog(): void;

		/**
		 * 移除单一对话框
		 */
        public static removeSingDialog(): void;
    }

	/**
	 * 只允许弹出一个的对话框，也可以称为通用对话框
	 */
    class SignleDialog extends Dialog {

        private m_lblText: eui.Label;
        private m_btnOk: eui.Image;

        private $call: Function;
        private $thisObje: any;

		/**
		 * 设置文本
		 */
        public setText(text: string): void;

		/**
		 * 添加监听
		 */
        public addCall(call: Function, thisObje?: any): void;

		/**
		 * 清除回调
		 */
        public clearCall(): void;
    }

    /**
	 * 影片剪辑，实现可于皮肤创建
	 */
    export class MovieClip extends UIComponent {

        private $factory: egret.MovieClipDataFactory;
        private $movieClip: egret.MovieClip;

        private $image: string;
        private $frame: string;
        private $action: string;

        private m_pClipCall: Function;
        private m_pThisObj: any;

		/**
		 * 初始化数据
		 */
        private initMovieClip(): void;

		/**
		 * 执行回调
		 */
        private carryMovieCall(): void;

		/**
		 * 清空回调
		 */
        private clearMovieCall(): void;

		/**
		 * 设置，若命名一致，可取其前缀
		 */
        public res(value: string);

		/**
		 * 设置获取帧动画合图资源名
		 */
        public image(value: string);

		/**
		 * 设置获取帧动画数据资源名
		 */
        public frame(value: string);

		/**
		 * 设置获取当前动画名称
		 */
        public action(): string;

        // 供外部使用的方法

		/**
		 * 只能通过该方法来获取MovieClip对象
		 */
        public addMovieCall(call: (clip: egret.MovieClip) => void, thisObj?: any): void;

		/**
		 * 播放
		 */
        public play(playTimes?: number): void;
    }

    /**
	 * 自带清理方法的控件
	 */
    export class UIComponent extends eui.Component {

        protected isDestroy: boolean;

		/**
		 * 控件进入场景时回调
		 */
        protected onCreate(): void;

		/**
		 * 控件离开场景时回调——onRemoveFromStage实际意义上应该是私有函数，如果没有
		 * 写上super.XXX，它没办法有效移除，为避免出错，才有该函数存在
		 */
        protected onDestroy(): void;

		/**
		 * 从父控件移除
		 */
        public removeFromParent(): void;
    }

	/**
	 * 锚点在中心的图片
	 */
    export class CenImage extends eui.Image { }

	/**
	 * App的视图
	 */
    export class AppComponent extends UIComponent {

        public constructor(skinName: string);
    }

    /**
     * 回调对象
     */
    class CSocketCallback {

        private m_pCallback: Function;
        private m_pTarget: any;
        private m_pParam: any;
        private m_iTime: any;

        public constructor(callback: Function, target: any, param?: any);

        /**
         * 是否过期
         */
        public isExpire(): boolean;

        /**
         * 执行回调
         */
        public call(object: any, msgId?: number): void;

        /**
         * 清除
         */
        public clear(): void;
    }

    /**
     * Socket通讯
     */
    export class CSocket {

        private m_pSocket: egret.WebSocket;
        private m_pRequestCalls: { [key: string]: CSocketCallback[] };	// 存放通信回调函数
        private m_pIsConnected: boolean;
        // 一些连接通用功能
        private m_pTimer: number;           // 重连延迟
        private m_pCheckTimer: egret.Timer; // 检查时间

        /**
         * 接收到消息
         */
        private onReceive(): void;

        /**
         * 连接开始
         */
        private onConnected(): void;

        /**
         * 连接报错
         */
        private onError(): void;

        /**
         * 关闭连接
         * @param isReconned 是否重连
         */
        public close(isReconned?: boolean): void;

        /**
         * 连接中断
         */
        private onClose(e: egret.Event, isReconned?: boolean): void;

        /**
         * 清理Socket
         */
        private onClear(): void;

        // 其他操作

        /**
         * 检查时间，清理没必要的事件
         */
        private onCheckTimer(): void;

        /**
         * 启动心跳
         */
        private startHeartbeat(): void;

        /**
         * 心跳停止
         */
        private stopHeartbeat(): void;

        /**
         * 清除定时器
         */
        private clearTimer(): void;

        // 主要供外部使用方法

        /**
         * 连接
         */
        public connect(): void;

        /**
         * 重新连接，500后发起重连
         */
        public reconnect(): void;

        /**
         * 发送消息
         */
        public sendAction(msg: ITFMessage): void;

        /**
         * 发送没有内容的消息且具有返回回调的消息
         */
        public sendWithMsgId(msgId: number, call?: Function, thisObj?: any, selfP?: any): void;

        /**
         * 发送具有返回回调的消息
         */
        public sendWitfCallback(msg: ITFMessage, call?: Function, thisObj?: any, selfP?: any): void;

        // 静态相关

        private static TIMER: number;   // 默认30毫秒

        private static $instance: CSocket;

        /**
         * 获取单利
         */
        public static getInstance(): CSocket;

        /**
         * 发送没有内容的消息且具有返回回调的消息
         */
        public static sendWithMsgId(msgId: number, call?: Function, thisObj?: any, selfP?: any): void;

        /**
         * 发送具有返回回调的消息
         */
        public static sendWitfCallback(msg: ITFMessage, call?: Function, thisObj?: any, selfP?: any): void;

        /**
         * 服务端主动推送的消息
         */
        public static nofify(msgId: number, info?: any): void;

        /**
         * 错误编码处理
         */
        public static error(errorCode: number, msgId: number): void;
    }

    /**
	 * 常用通讯消息
	 */
    export enum GeneralMsg {
        TIME = 0,
        ENTER,
        MATCH,
        PLAYING,
        FINISH
    }

    /**
	 * 推送和接受的消息对象
	 */
    export interface ITFMessage {
        msgId: number;  	// 消息ID
        info: any;      	// 消息数据
        errorCode?: number;	// 错误编码，0为没有错误，注意后端推送消息才有该属性
    }

	/**
	 * 消息工具类
	 */
    export class MsgUtils {

        private static m_pMsgMap: { [key: number]: number };

		/**
		 * 新建一个消息
		 */
        public static newMsg(msgId: number, newInfo?: boolean): ITFMessage;

		/**
		 * 获取消息ID
		 * @param tag 前后端标志，目前是11和12
		 */
        private static getMsgId(tag: number, msgId: number): number;

		/**
		 * 转化为请求的消息ID
		 */
        public static getResMsgId(msgId: number): number;

		/**
		 * 转化为推送的消息ID
		 */
        public static getReqMsgId(msgId: number): number;

		/**
		 * 初始化消息，注册
		 */
        public static init(clzz: any): void;

		/**
		 * 是否注册过某个消息
		 */
        public static hasMsg(msgId: number): boolean;

		/**
		 * 清除数据
		 */
        public static clear(): void;
    }

    /**
	 * 服务器属性类
	 */
    export class Server {

		/**
		 * 服务器地址
		 */
        public static HOST: string;
		/**
		 * 端口
		 */
        public static POST: number;

        // 服务器时间相关

        private static m_pTimer: egret.Timer;		// 定时器，定时访问，更新时间
        private static m_pServerTime: number;		// 记录服务器时间
        private static m_pLocalTime: number;		// 记录本地时间
        private static m_pTAction: ITFMessage;		// 时间行动

		/**
		 * 获取当前服务器时间，单位毫秒
		 */
        public static getTime(): number;

		/**
		 * 启动心跳
		 */
        public static startHeartBeat(): void;

		/**
		 * 停止心跳
		 */
        public static stopHeartBeat(): void;

		/**
		 * 心跳回调
		 */
        private static onHeartBeat(): void;

		/**
		 * 更新时间
		 */
        private static updateTime(info: { time: number }): void;

        // 需要设置的参数

        public static sid: string;		// 玩家的sid
        public static roomId: number;	// 房间的id

		/**
		 * 初始化进入游戏
		 */
        public static startGame(): void;
    }

    /**
	 * 前后端交互方法
	 */
    export class Service {

		/**
		 * 请求游戏内，默认带上sid和room_id
		 * @param msgId 消息ID
		 * @param respCall 响应回调
		 * @param thisObj 回调对象
		 * @param setData 设置请求参数函数
		 */
        public static reqGameAction(msgId: number, respCall: Function, thisObj?: any, setData?: (data: any) => void): void;

		/**
		 * 请求进入游戏
		 */
        public static reqEnterGame(): void;

		/**
		 * 响应进入游戏
		 */
        protected static repEnterGame(resp: any): void;

		/**
		 * 游戏过程数据更新
		 */
        public static reqPlayGame(info: ITFGameInfo): void;

		/**
		 * 响应游戏过程数据更新
		 */
        protected static repPlayGame(resp: any): void;

		/**
		 * 游戏结束
		 * @param info 游戏数据
		 */
        public static reqFinishGame(info: ITFGameInfo): void;

		/**
		 * 响应游戏过程数据更新
		 */
        protected static repFinishGame(resp: any): void;
    }

    var app: AppViews;	// 存放单例

	/**
	 * 应用的视图管理类
	 */
    export class AppViews {

        private $panelLevel: egret.DisplayObjectContainer;	// 面板层
        private $dialogLevel: egret.DisplayObjectContainer;	// 对话框层

        protected constructor();

		/**
		 * 初始化
		 */
        private init(): void;

		/**
		 * 获取舞台
		 */
        public stage(): egret.Stage;

		/**
		 * 获取面板层
		 */
        public panelLevel: egret.DisplayObjectContainer;

		/**
		 * 获取对话框层
		 */
        public dialogLevel: egret.DisplayObjectContainer;

		/**
		 * 添加层
		 * @param name 名称
		 */
        public addLevel(name?: string): egret.DisplayObjectContainer;

		/**
		 * 获取层
		 */
        public getLevel(name?: string): egret.DisplayObjectContainer;

		/**
		 * 获取视图管理类单例
		 */
        public static app(): AppViews;

        // 重点，层及控制管理体现

		/**
		 * 插入一个控件，并居中
		 * @param attr AppViews对象的属性名
		 * @param clzz 需要添加的对象类
		 */
        private static pushChild<T extends UIComponent>(attr: string, clzz: { new (...args: string[]): T }): T;

		/**
		 * 移除层里的控件
		 * @param attr AppViews对象的属性名
		 * @param clzz 需要移除的对象类
		 */
        private static removeChild(attr: string, clzz: { new (...args: string[]): UIComponent }): void;

		/**
		 * 将子控件设置在父控件中心点
		 */
        public static setInCenter(child: egret.DisplayObject): void;

		/**
		 * 新建一个面板并放入
		 */
        public static pushPanel<T extends UIComponent>(clzz: { new (...args: string[]): T }): T;

		/**
		 * 新建一个对话框并放入
		 */
        public static pushDialog<T extends Dialog>(clzz: { new (...args: string[]): T }): T;

		/**
		 * 移除面板
		 */
        public static removePanel(clzz: { new (...args: string[]): UIComponent }): void;

		/**
		 * 移除对话框
		 */
        public static removeDialog(clzz: { new (...args: string[]): Dialog }): void;
    }

    /**
	 * 事件分发工具类，异步操作用
	 */
    export class Dispatch {

        private static $targets: any;	// 存放所有回调函数

		/**
		 * 注册对象
		 * @param key 事件的唯一标志
		 * @param obj 注册事件
		 * @param replace 是否替换掉之前的回调
		 */
        private static $register(key: string, obj: [Function, any], replace?: boolean): boolean;

		/**
		 * 注册事件
		 * @param key 事件的唯一标志
		 * @param call 回调函数
		 * @param thisObj 回调所属对象
		 * @param replace 是否替换掉之前的回调
		 */
        public static register(key: string, call: Function, thisObj?: any, replace?: boolean): boolean;

		/**
		 * 通知，触发回调
		 * @param key 事件标志
		 * @param param 回调参数
		 */
        public static notice(key: string, ...params: any[]): void;

		/**
		 * 移除事件
		 */
        public static remove(key: string): void;

		/**
		 * 是否注册了某个事件
		 */
        public static hasRegister(key: string): boolean;

		/**
		 * 注册多个事件，统一回调，参数功能看register
		 */
        public static registers(keys: string[], call: Function, thisObj?: any, replace?: boolean): void;

		/**
		 * 清除所有事件
		 */
        public static clear(): void;
    }

    /**
	 * 监听对象
	 */
    interface TargetEvent {
        type: string;
        listener: Function;
        thisObject: any;
        useCapture: boolean;
        priority: number;
    }

    interface ScaleTarget extends egret.DisplayObject {
        $bgScaleX?: number;	// 初始缩放值
        $bgScaleY?: number;
        $evtScale?: number;	// 缩放倍数
    }

	/**
	 * 控件监听工具类
	 */
    export class EventUtils {

        protected static $scaleTime: number;	// 缩放动画时间，默认100

		/**
		 * 移除控件上的所有监听，该方法也适用于没有通过addEventListener来添加的控件
		 */
        public static removeEventListener(target: egret.EventDispatcher): void;

		/**
		 * 移除root往下所有的点击事件
		 */
        public static removeEventListeners(root: egret.EventDispatcher): void;

		/**
		 * 添加缩放监听，记得用removeEventListener来移除这个监听
         * @param target
         * @param scale 缩放值，默认0.95
		 */
        public static addScaleListener(target: ScaleTarget, scale?: number): void;

		/**
		 * 缩放开始
		 */
        protected static onScleBegin(event: egret.TouchEvent): void;

		/**
		 * 缩放结束
		 */
        protected static onScaleEnd(event: egret.TouchEvent): void;

        // 常用的监听类型归类

		/**
		 * 添加TouchTap监听
		 */
        public static addTouchTapListener(target: egret.EventDispatcher, call: Function, thisObj?: any, useCapture?: boolean): void;

		/**
		 * 在TouchTap的基础上进行缩放
		 */
        public static addTouchTapScaleListener(target: ScaleTarget, call: Function, thisObj?: any, scale?: number, useCapture?: boolean): void;

		/**
		 * 添加按住监听
		 * @param target
		 * @param begin 按住时的回调
		 * @param end 松手时的回调，会调用多次，请自己在end里判断
		 */
        public static addTouchingListener(target: egret.EventDispatcher, begin: Function, end: Function, thisObj?: any): void;

		/**
		 * 添加移动监听
		 */
        public static addTouchMoveListener(target: egret.DisplayObject): void;

		/**
		 * 添加按住时每隔一段时间就触发回调的监听
		 * @returns 返回监听函数，清理使用
		 */
        public static addTouchStepListener(target: egret.EventDispatcher, delay: number, step: Function, thisObj?: any): Function[];
    }

    /**
	 * 游戏生命周期管理类
	 * 注：调试模式不要开启
	 */
    export class LifeCycle {

		/**
		 * 游戏生命周期初始化
		 */
        public static init(): void;

		/**
		 * 全局游戏更新函数
		 */
        public static update(): void;

		/**
		 * 全局游戏暂停函数
		 */
        public static pause(): void;

		/**
		 * 全局游戏恢复函数
		 */
        public static resume(): void;
    }

    /**
	 * 存放和获取一些本地变量名，属于应用级别上的存储
	 */
    export class LocalData {

		/**
		 * 设置Item
		 */
        public static setItem(key: string, value: string): boolean;

		/**
		 * 获取Item的值
		 */
        public static getItem(key: string): string;

		/**
		 * 注意value必须是对象，否则会出现奇怪的现象
		 */
        public static setObject(key: string, value: any): boolean;

		/**
		 * 获取一个对象
		 */
        public getObject(key: string): any;
    }

    /**
	 * 类型检验工具类
	 */
    export class TypeUtils {

		/**
		 * 检测是否是字符串类型，注意new String检测不通过的
		 */
        public static isString(obj: any): boolean;

		/**
		 * 检测是不是数组
		 */
        public static isArray(obj: any): boolean;

		/**
		 * 检测是不是数字，注意new Number不算进来
		 */
        public static isNumber(obj: any): boolean;
    }

    /**
     * 工具类，存放一些常用的且不好归类的方法
     */
    export class Utils {

        // 工具私有变量，请勿乱动

        /**
         * 初始化一个长度为length，值全为value的数组
         * 注意，由于数组公用一个value，因此，value适用于基础类型，对于对象类的，请用memset2方法
         */
        public static memset<T>(length: number, value: T): T[];

        /**
         * 初始化一个长度为length，值通过getValue函数获取，注意getValue第一个参数是没用，第二个参数是当前数组的下标，即你返回的数据将存放在数组的该下标
         */
        public static memset2<T>(length: number, getValue: (value?: T, index?: number) => T): T[];

        /**
         * 创建一个定时器
         * @param listener 定时回调
         * @param thisObject 回调所属对象
         * @param second 回调间隔，单位秒，默认一秒
         * @param repeat 循环次数，默认一直重复
         */
        public static createTimer(listener: Function, thisObject: any, second?: number, repeat?: number): egret.Timer;

        /**
         * 移除一个定时器
         * @param timer 被移除的定时器
         * @param listener 监听函数
         * @param thisObject 函数所属对象
         */
        public static removeTimer(timer: egret.Timer, listener: Function, thisObject?: any): void;

        /**
         * 获取网页参数
         */
        public static getQueryString(name: string): string;

        /**
         * 获取cr1位于cr0的方向
         * 注：0为上，顺时针开始数，共八个方向
         */
        public static getDistance(col0: number, row0: number, col1: number, row1: number): number;

        /**
         * 关闭web版游戏
         */
        public static closeWebGame(): void;

        /**
         * 自定义格式化字符串
         * @param reg 正则
         * @param str 字符串
         * @param args 填充参数
         */
        public static formatStringReg(reg: RegExp, str: string, args: any[]): string;

        /**
         * 格式化字符串
         */
        public static formatString(str: string, ...args: any[]): string;

        /**
         * 偏移整型数组
         * @param array 需要偏移的数组
         * @param offset 偏移量，对应array长度，没有值时默认0
         */
        public static offsetArray(array: number[], ...offset: number[]): number[];
    }
}