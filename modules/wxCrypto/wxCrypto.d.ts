declare namespace wxCrypto {

	interface ITFPkcs {

	}

	interface ITFUtil {
		base64ToBytes: (str: string) => number[];
	}

	interface ITFMode {
		CBC: { new (pcks: ITFPkcs): ITFMode };
	}

	interface ITFAES {
		encrypt: (message: number[], password: number[], options: any) => string;
		decrypt: (ciphertext: number[], password: number[], options: any) => string;
	}

	export var pad: { pkcs7: ITFPkcs };
	export var util: ITFUtil;
	export var mode: ITFMode;
	export var AES: ITFAES;
}